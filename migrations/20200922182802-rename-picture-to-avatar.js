const _ = require('lodash');

/**
 *
 * @param {MongoDb} db
 * @param {MongoClient} client
 */
module.exports = {
  async up(db, client) {
    const users = await db.collection('users').find({}).toArray();
    const operations = users.map((user) => {
      if (!_.isUndefined(user.profile.picture)) {
        return db.collection('users').updateOne(
          { _id: user._id },
          {
            $set: { 'profile.avatar': user.profile.picture },
            $unset: { 'profile.picture': null },
          },
        );
      }

      return user;
    });

    return Promise.all(operations);
  },

  /**
   *
   * @param {MongoDb} db
   * @param {MongoClient} client
   */
  async down(db, client) {
    const users = await db.collection('users').find({}).toArray();
    const operations = users.map((user) => {
      if (!_.isUndefined(user.profile.avatar)) {
        return db.collection('users').updateOne(
          { _id: user._id },
          {
            $set: { 'profile.picture': user.profile.avatar },
            $unset: { 'profile.avatar': null },
          },
        );
      }

      return user;
    });

    return Promise.all(operations);
  },
};
