module.exports = {
  /**
   *
   * @param {MongoDb} db
   * @param {MongoClient} client
   */
  async up(db, client) {
    return db
      .collection('users')
      .updateMany(
        {},
        {
          $unset: {
            'options.skippedNavbarTour': null,
            'options.skippedMeetingsOverviewTour': null,
          },
        },
      );
  },

  /**
   *
   * @param {MongoDb} db
   * @param {MongoClient} client
   */
  async down(db, client) {
    // empty
  },
};
