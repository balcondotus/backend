module.exports = {
  /**
   *
   * @param {MongoDb} db
   * @param {MongoClient} client
   */
  async up(db, client) {
    return db.collection('events').updateMany({}, { $unset: { 'payment._id': null } });
  },

  /**
   *
   * @param {MongoDb} db
   * @param {MongoClient} client
   */
  async down(db, client) {
    // empty
  },
};
