const jwt = require('jsonwebtoken');
const moment = require('moment');
const _ = require('lodash');
const AppConfig = require('config/app');

/**
 * Checks if the provided token is empty.
 *
 * @param {String} token
 */
exports.isTokenEmpty = (token) =>
  !_.isString(token) || _.isEmpty(token) || ['undefined', 'null'].includes(token);

/**
 * Generates JWT token.
 *
 * @param {ObjectId} userId
 * @param {String} userEmail
 */
exports.generateToken = (userId) => {
  // To use Balcon without losing the token when a new email is provided,
  // we excluded email address from the credential.
  return jwt.sign(
    {
      _id: userId,
      exp: moment.utc().add(AppConfig.jwt.expiration, AppConfig.jwt.expirationType).unix(),
    },
    process.env.JWT_SECRET,
    {
      audience: process.env.JWT_AUDIENCE,
      issuer: process.env.JWT_ISSUER,
    },
  );
};
