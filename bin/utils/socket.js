const SOCKET = require('lib/consts/socket');

/**
 * Prepares a private room for the user in the dashboard namespace.
 *
 * @param {ObjectId} userId
 *
 * @returns {SocketIO.Namespace}
 */
exports.getPrivateRoom = (userId) => {
  /**
   * @type {SocketIO.Namespace}
   */
  const nsp = global.io.of(SOCKET.NAMESPACE.DASHBOARD);
  /**
   * @type {SocketIO.Room}
   */
  const room = SOCKET.ROOM.PRIVATE(userId);

  return nsp.volatile.in(room);
};
