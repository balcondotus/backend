const NotificationModel = require('lib/models/notification-model');
const SOCKET = require('lib/consts/socket');
const nodemailer = require('nodemailer');
const AppConfig = require('config/app');
const TwilioUtils = require('./twilio');
const ResponseUtils = require('./response');
const SocketUtils = require('./socket');

/**
 * Saves and sends the given notification.
 *
 * @param {Boolean} shouldSend
 * @param {Object} notificationObj NotificationModel data
 */
async function notifyWithNotification(shouldSend, notificationObj) {
  const notificationDoc = await new NotificationModel(notificationObj).save();

  if (shouldSend) {
    const notification = await NotificationModel.getOneByNotificationId(notificationDoc._id);
    const response = ResponseUtils.respondSuccess(notification);
    SocketUtils.getPrivateRoom(notificationObj.user).emit(SOCKET.EVENT.NOTIFICATIONS, response);
  }
}

/**
 * Notify given user with an email if and only if her email is verified.
 *
 * @param {Document} user
 * @param {String} subject
 * @param {String} body
 * @param {Boolean} forceSend
 */
async function notifyWithEmail(user, subject, body, forceSend = false, priority = 'normal') {
  const shouldSend = user.email.verified || forceSend;

  if (shouldSend) {
    const transport =
      process.env.NODE_ENV === 'production'
        ? AppConfig.transport.sendgrid
        : AppConfig.transport.mailtrap;

    const mail = nodemailer.createTransport(transport);

    return mail.sendMail({
      from: AppConfig.notificationSender.email,
      to: user.email.email,
      subject,
      html: body,
      priority,
    });
  }
}

/**
 * Wraps `smsUtils.send()`
 *
 * @param {Document} user
 * @param {String} body
 */
async function notifyWithSms(user, body) {
  if (user.phone && user.phone.phone && user.phone.verified)
    return TwilioUtils.notifyWithSms(user.phone.phone, body);
}

module.exports = { notifyWithSms, notifyWithEmail, notifyWithNotification };
