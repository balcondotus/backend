const AppConfig = require('config/app');
const FeeUtils = require('bin/utils/fee');
const _ = require('lodash');

/**
 * Calculates event's price breakdown, including total.
 *
 * @param {number} eventRate
 * @param {number} eventDuration
 */
function calculateRateBreakdown(eventRate, eventDuration) {
  if (_.isNaN(eventRate) || _.isNaN(eventDuration)) throw new Error('Invalid Arguments');

  const baseFee = Math.ceil(eventRate);
  const platformFee = Math.ceil(eventRate * AppConfig.fee.platform.percent);
  const serviceFee = eventRate > 0 ? Math.ceil(FeeUtils.calculateServiceFee(eventDuration)) : 0;
  const transactionFee =
    eventRate > 0
      ? Math.ceil(FeeUtils.calculateTransactionFee(baseFee, platformFee, serviceFee))
      : 0;

  return {
    total: baseFee + platformFee + serviceFee + transactionFee,
    breakdown: {
      baseFee,
      platformFee,
      serviceFee,
      transactionFee,
    },
  };
}

module.exports = { calculateRateBreakdown };
