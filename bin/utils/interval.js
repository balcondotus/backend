const moment = require('moment');
const _ = require('lodash');

const intervalPattern = /^(?<hours>[0-9]{2}):(?<minutes>[0-9]{2})$/;

/**
 * Makes a moment() object from given `interval` and given `date`.
 *
 * @param {moment.MomentInput} date
 * @param {String} time
 *
 * @returns {moment.Moment}
 */
function makeMoment(date, time) {
  const { hours, minutes } = time.match(intervalPattern).groups;

  return moment(date).set('hour', Number(hours)).set('minute', Number(minutes));
}

/**
 * Checks if given time is within given date.
 *
 * @param {moment.MomentInput} date
 * @param {String} time
 */
function isTimeWithinDate(date, time) {
  return moment.utc(date).isSame(makeMoment(date, time), 'date');
}

/**
 * Sorts intervals in ascending order.
 *
 * @param {Array} list Array of intervals.
 *
 * @returns {Array} Sorted intervals.
 */
function sortIntervals(list) {
  return _.sortBy(list, (interval) => moment.utc(interval.start, 'HH:mm'));
}

/**
 * Finds overlapping intervals.
 * It sorts the given list natively.
 *
 * @param {Array} list Array of intervals.
 */
function findOverlapingIntervals(list) {
  const sortedList = sortIntervals(list);

  const overlappingIntervals = sortedList.reduce((result, current, idx, arr) => {
    // get the previous range
    if (idx === 0) return result;

    const previous = arr[idx - 1];

    const currentRange = moment.range(
      moment.utc(current.start, 'HH:mm'),
      moment.utc(current.end, 'HH:mm'),
    );
    const previousRange = moment.range(
      moment.utc(previous.start, 'HH:mm'),
      moment.utc(previous.end, 'HH:mm'),
    );
    const overlap = currentRange.overlaps(previousRange);

    if (overlap) {
      if (!result.includes(current)) result.push(current);
      if (!result.includes(previous)) result.push(previous);
    }

    return result;
  }, []);

  return overlappingIntervals;
}

/**
 * Makes `UTC` intervals from `start` to `end` with a `frequency`.
 *
 * @param {moment.MomentInput} start `UTC` start datetime
 * @param {moment.MomentInput} end `UTC` end datetime
 * @param {number} frequency Event frequency
 * @param {number} duration Event duration
 */
function make(start, end, frequency, duration) {
  if (
    !moment(start).isValid() ||
    !moment(end).isValid() ||
    _.isNaN(frequency) ||
    !_.inRange(frequency, 5, 61) ||
    _.isNaN(duration) ||
    !_.inRange(duration, 1, 721)
  )
    throw new Error('Invalid Arguments');

  const output = {};
  // By subtracting duration from the end of the valid range, we make sure
  // the end time of the latest slot doesn't pass the valid range
  const validRange = moment.range(moment.utc(start), moment.utc(end).subtract(duration, 'minute'));
  const startEventMoment = moment.utc(start);

  // While both start and end times are within the valid range
  while (validRange.contains(startEventMoment)) {
    const formattedSpot = startEventMoment.format('YYYY-MM-DD');

    // Initialize spot with an emty array
    if (!_.has(output, formattedSpot)) output[formattedSpot] = [];

    output[formattedSpot].push({
      start: startEventMoment.format('YYYY-MM-DDTHH:mm'),
      end: startEventMoment.add(frequency, 'minute').format('YYYY-MM-DDTHH:mm'),
    });
  }

  return output;
}

/**
 * Remakes intervals in the given time zone.
 *
 * @param {object} intervals
 * @param {string} initTimeZone `UTC`/timezone the given intervals are in.
 * @param {string} timeZone `UTC`/timezone the output should be in.
 */
function tz(intervals, initTimeZone, timeZone) {
  if (
    !_.isPlainObject(intervals) ||
    _.isNull(intervals) ||
    _.isNull(moment.tz.zone(initTimeZone)) ||
    _.isNull(moment.tz.zone(timeZone))
  )
    throw new Error('Invalid Arguments');

  const output = {};

  Object.entries(intervals).forEach(([, localIntervals]) => {
    localIntervals.forEach((localInterval) => {
      const startTimeMoment = moment.tz(localInterval.start, initTimeZone).tz(timeZone);
      const endTimeMoment = moment.tz(localInterval.end, initTimeZone).tz(timeZone);
      const formattedSpot = startTimeMoment.format('YYYY-MM-DD');

      // Initialize spot with an emty array
      if (!_.has(output, formattedSpot)) output[formattedSpot] = [];

      output[formattedSpot].push({
        start: startTimeMoment.format('YYYY-MM-DDTHH:mm'),
        end: endTimeMoment.format('YYYY-MM-DDTHH:mm'),
      });
    });
  });

  return output;
}

/**
 * Sorts by spots then events.
 *
 * @param {object} intervals
 */
function sort(intervals) {
  if (!_.isPlainObject(intervals) || _.isNull(intervals)) throw new Error('Invalid Arguments');

  return _(intervals)
    .toPairs()
    .sortBy(0)
    .fromPairs()
    .mapValues((events) => {
      return _.sortBy(events, 'start');
    })
    .value();
}

/**
 * Filters events by applying notice of schedule.
 *
 * @param {object} intervals
 * @param {number} noticeOfSchedule
 */
function applyNoticeOfSchedule(intervals, noticeOfSchedule) {
  if (
    !_.isPlainObject(intervals) ||
    _.isNull(intervals) ||
    _.isNaN(noticeOfSchedule) ||
    !_.inRange(noticeOfSchedule, 0, 525601)
  )
    throw new Error('Invalid Arguments');

  const now = moment.utc().add(noticeOfSchedule, 'minute');

  return (
    _(intervals)
      .toPairs()
      .map(([spot, events]) => {
        return [
          spot,
          _.filter(events, (event) => {
            const startMoment = moment.utc(event.start);

            return startMoment.isAfter(now);
          }),
        ];
      })
      // Ignore empty spots
      .filter(([, events]) => !_.isEmpty(events))
      .fromPairs()
      .value()
  );
}

/**
 * Filters events by applying buffers and ignoring conflicting events.
 *
 * @param {object} intervals
 * @param {object} meetings
 * @param {number} before Buffer before
 * @param {number} after Buffer after
 */
function applyBuffers(intervals, meetings, before, after) {
  if (
    !_.isPlainObject(intervals) ||
    _.isNull(intervals) ||
    !_.isPlainObject(meetings) ||
    _.isNull(meetings) ||
    _.isNaN(before) ||
    _.isNaN(after) ||
    !_.inRange(after, 0, 181) ||
    !_.inRange(before, 0, 181)
  )
    throw new Error('Invalid Arguments');

  return (
    _(intervals)
      .toPairs()
      .map(([spot, events]) => {
        return [
          spot,
          _.filter(events, (event) => {
            // No meetings has been scheduled for the spot
            if (!_.has(meetings, spot)) return true;

            const eventRange = moment.range(
              moment.utc(event.start).subtract(before, 'minute'),
              moment.utc(event.end).add(after, 'minute'),
            );

            let includeEvent = true;

            // eslint-disable-next-line no-restricted-syntax
            for (const meetingEvent of meetings[spot]) {
              const meetingEventRange = moment.range(
                moment.utc(meetingEvent.start).subtract(before, 'minute'),
                moment.utc(meetingEvent.end).add(after, 'minute'),
              );

              if (eventRange.overlaps(meetingEventRange)) {
                // Overlapping events should be excluded
                includeEvent = false;
                break;
              }
            }

            return includeEvent;
          }),
        ];
      })
      // Ignore empty spots
      .filter(([, events]) => !_.isEmpty(events))
      .fromPairs()
      .value()
  );
}

/**
 * Filters events by applying date range.
 *
 * @param {object} intervals
 * @param {string} type Date range type
 * @param {any} value Date range value
 */
function applyDateRange(intervals, type, value) {
  if (
    !_.isPlainObject(intervals) ||
    _.isNull(intervals) ||
    !_.isString(type) ||
    !['rolling', 'range', 'indefinitely'].includes(type.toLowerCase())
  )
    throw new Error('Invalid Arguments');

  const now = moment.utc();

  return (
    _(intervals)
      .toPairs()
      .map(([spot, events]) => {
        return [
          spot,
          _.filter(events, (event) => {
            const startTimeMoment = moment.utc(event.start);
            const endTimeMoment = moment.utc(event.end);
            let validRange;

            switch (type.toLowerCase()) {
              case 'rolling':
                if (_.isNaN(value) || !_.inRange(value, 0, 366)) throw new Error('Invalid Value');

                validRange = moment.range(now, now.clone().add(value, 'day'));

                // Overlapping is fine here
                return validRange.contains(startTimeMoment) && validRange.contains(endTimeMoment);

              case 'range':
                if (
                  !_.isPlainObject(value) ||
                  _.isNull(value) ||
                  !_.has(value, 'start') ||
                  !_.has(value, 'end') ||
                  !moment(value.start).isValid() ||
                  !moment(value.end).isValid()
                )
                  throw new Error('Invalid Value');

                // Both start and end dates are expanded to cover the whole range
                validRange = moment.range(
                  moment.utc(value.start).startOf('date'),
                  moment.utc(value.end).endOf('date'),
                );

                return startTimeMoment.within(validRange) && endTimeMoment.within(validRange);

              case 'indefinitely':
                return true;

              default:
                throw new Error(`${type} is not implemented`);
            }
          }),
        ];
      })
      // Ignore empty spots
      .filter(([, events]) => !_.isEmpty(events))
      .fromPairs()
      .value()
  );
}

/**
 * FilterS events by applying max meetings per day.
 *
 * @param {object} intervals
 * @param {object} meetings
 * @param {number} max Max meetings per day
 */
function applyMaxPerDay(intervals, meetings, max) {
  if (
    !_.isPlainObject(intervals) ||
    _.isNull(intervals) ||
    !_.isPlainObject(meetings) ||
    _.isNull(meetings) ||
    _.isNaN(max) ||
    !_.inRange(max, 0, 1441)
  )
    throw new Error('Invalid Arguments');

  // Max 0 means unlimited events
  if (max === 0) return intervals;

  return _(intervals)
    .toPairs()
    .filter(([spot]) => !_.has(meetings, spot) || meetings[spot].length !== max)
    .fromPairs()
    .value();
}

/**
 * Flattens intervals with slots.
 *
 * @param {object} intervals
 * @param {string?} format Format events in this format using `moment.format()`. `HH:mm` is default.
 */
function slots(intervals, format = 'HH:mm') {
  if (!_.isPlainObject(intervals) || _.isNull(intervals)) throw new Error('Invalid Arguments');

  return _(intervals)
    .toPairs()
    .map(([spot, events]) => {
      return [
        spot,
        _.map(events, (event) => {
          return moment.utc(event.start).format(format);
        }),
      ];
    })
    .fromPairs()
    .value();
}

module.exports = {
  make,
  applyBuffers,
  slots,
  applyMaxPerDay,
  applyDateRange,
  tz,
  sort,
  applyNoticeOfSchedule,
  findOverlapingIntervals,
  sortIntervals,
  isTimeWithinDate,
  makeMoment,
};
