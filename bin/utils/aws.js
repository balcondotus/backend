const AWS = require('aws-sdk');
const fs = require('fs-extra');
const AppConfig = require('config/app');

function getStorageClient() {
  return new AWS.S3({
    endpoint: AppConfig.aws.s3.endpoint,
    credentials: {
      accessKeyId: process.env.ARVANCLOUD_ACCESS_KEY,
      secretAccessKey: process.env.ARVANCLOUD_SECRET_KEY,
    },
  });
}

/**
 * Uploads an avatar to the cloud storage.
 *
 * @param {String} fileName
 * @param {String} filePath
 */
exports.uploadAvatar = async (fileName, filePath) => {
  const client = getStorageClient();

  const data = fs.readFileSync(filePath);

  return client
    .upload({
      ACL: 'public-read',
      Bucket: AppConfig.aws.s3.bucket.avatar,
      Key: fileName,
      Body: data,
    })
    .promise();
};

/**
 * Deletes an avatar from the cloud storage.
 *
 * @param {String} fileName
 */
exports.deleteAvatar = async (fileName) => {
  const client = getStorageClient();

  return client.deleteObject({ Key: fileName, Bucket: AppConfig.aws.s3.bucket.avatar }).promise();
};
