const IntlMessageFormat = require('intl-messageformat').default;
const memoize = require('fast-memoize');

/**
 * Formats translation using `IntlMessageFormat`.
 *
 * @param {object} resource An object contains desired translations
 * @param {string?} userLocale User's locale, `en` is set as fallback and default
 * @param {object?} formattings
 * @param {string?} userTimeZone User's time zone, `Asia/Tehran` is set default
 *
 * @throws {InternalServerError} if neither given locale nor en nor fa exists in the given resource.
 *
 * @returns {string} Formatted translation
 */
exports.format = (resource, userLocale = 'en', formattings = {}, userTimeZone = 'Asia/Tehran') => {
  const formatters = {
    getDateTimeFormat: memoize(
      (locale, { timeZone, ...opts }) =>
        new Intl.DateTimeFormat(locale, { timeZone: userTimeZone, ...opts }),
    ),
  };

  return new IntlMessageFormat(resource[userLocale] || resource.en, [userLocale, 'en'], undefined, {
    ignoreTag: true,
    formatters,
  }).format(formattings);
};
