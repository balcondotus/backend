const moment = require('moment');
const _ = require('lodash');
const IntervalUtils = require('./interval');

/**
 * Makes intervals from availabilities.
 *
 * @param {object} availabilities
 * @param {moment.MomentInput} start `UTC` datetime for start of the range
 * @param {moment.MomentInput} end `UTC` datetime for end of the range
 * @param {number} frequency Event frequency
 * @param {number} duration Event duration
 * @param {string} initTimeZone Host time zone
 */
function intervals(availabilities, start, end, frequency, duration, initTimeZone) {
  if (
    !_.isObject(availabilities) ||
    !moment(start).isValid() ||
    !moment(end).isValid() ||
    _.isNaN(frequency) ||
    !_.inRange(frequency, 5, 61) ||
    _.isNaN(duration) ||
    !_.inRange(duration, 1, 721) ||
    !_.isString(initTimeZone) ||
    _.isNull(moment.tz.zone(initTimeZone))
  )
    throw new Error('Invalid Arguments');

  const times = {};
  const specifics = {};
  const validRange = moment.range(moment.utc(start), moment.utc(end));
  const dateMoment = moment.utc(start);

  /**
   *
   * @param {string} spot Spot in `YYYY-MM-DD` format
   * @param {object} event
   */
  const makeIntervals = (spot, event) => {
    const startEventMoment = moment.tz(`${spot}T${event.start}`, initTimeZone).utc();
    const endEventMoment = moment.tz(`${spot}T${event.end}`, initTimeZone).utc();

    return IntervalUtils.make(startEventMoment, endEventMoment, frequency, duration);
  };

  // While calculated date is within the range
  while (dateMoment.within(validRange)) {
    /**
     * Used with `_.mergeWith()` for concatenating objects.
     *
     * @param {object?} objValue Object value
     * @param {object} srcValue Source value
     */
    const mergeWithCustomizer = (objValue, srcValue) => {
      if (_.isArray(objValue)) {
        // Prevent duplicate specifics due to over and over traversing availabilities
        // e.g. from Saturday to next 30 days, causes Saturday get traversed multiple times
        // Concat arrays then remove duplicates by start field.
        return _.uniqBy(_.concat(objValue, srcValue), 'start');
      }
    };

    const dateWeekday = dateMoment.format('ddd').toLowerCase();
    const spot = dateMoment.format('YYYY-MM-DD');

    // Calculate `times` if any
    availabilities[dateWeekday].times.forEach((event) => {
      const eventIntervals = makeIntervals(spot, event);

      _.mergeWith(times, eventIntervals, mergeWithCustomizer);
    });

    // Calculate `specifics` if any
    Object.entries(availabilities[dateWeekday].specifics).forEach(([spot, events]) => {
      const spotMoment = moment.tz(spot, initTimeZone).utc(true);

      if (
        spotMoment.isSame(dateMoment, 'date') &&
        spotMoment.isSame(dateMoment, 'month') &&
        spotMoment.isSame(dateMoment, 'year')
      ) {
        // Make intervals for this spot if and only if it is equal to the current calculated date.
        // This improves performance because traversing through weekdays happens
        // e.g. from Saturday to next 30 days, causes Saturday get traversed multiple times
        events.forEach((event) => {
          const eventIntervals = makeIntervals(spot, event);

          _.mergeWith(specifics, eventIntervals, mergeWithCustomizer);
        });
      }
    });

    // Next day
    dateMoment.add(1, 'day');
  }

  // Weekly intervals should be replaced with specifics
  return _.assign(times, specifics);
}

/**
 * Wraps other methods to make slots.
 *
 * @param {object} availabilities
 * @param {object} meetings
 * @param {moment.MomentInput} start `UTC` datetime for start of the range
 * @param {moment.MomentInput} end `UTC` datetime for end of the range
 * @param {string} initTimeZone Host time zone
 * @param {number} frequency Event frequency
 * @param {number} duration Event duration
 * @param {string} timeZone
 * @param {number} noticeOfSchedule
 * @param {number} bufferBefore
 * @param {number} bufferAfter
 * @param {string} dateRangeType
 * @param {any} dateRangeValue
 * @param {number} maxPerDay
 *
 * @returns {object} Both UTC and local versions of slots
 */
function slots(
  availabilities,
  meetings,
  start,
  end,
  initTimeZone,
  frequency,
  duration,
  timeZone,
  noticeOfSchedule,
  bufferBefore,
  bufferAfter,
  dateRangeType,
  dateRangeValue,
  maxPerDay,
) {
  let output = intervals(availabilities, start, end, frequency, duration, initTimeZone);
  output = IntervalUtils.applyNoticeOfSchedule(output, noticeOfSchedule);
  output = IntervalUtils.applyBuffers(output, meetings, bufferBefore, bufferAfter);
  output = IntervalUtils.applyDateRange(output, dateRangeType, dateRangeValue);
  output = IntervalUtils.applyMaxPerDay(output, meetings, maxPerDay);

  let utcOutput = IntervalUtils.slots(output);
  utcOutput = IntervalUtils.sort(utcOutput);
  output = IntervalUtils.tz(output, 'UTC', timeZone);
  let localOutput = IntervalUtils.slots(output);
  localOutput = IntervalUtils.sort(localOutput);

  return { utc: utcOutput, local: localOutput };
}

module.exports = {
  intervals,
  slots,
};
