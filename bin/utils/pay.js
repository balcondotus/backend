const { default: Bugsnag } = require('@bugsnag/js');
const AppConfig = require('config/app');
const fetch = require('node-fetch');

exports.request = async (amount, userPhoneNumber, description) => {
  const fetched = await fetch('https://gateway.zibal.ir/v1/request', {
    method: 'POST',
    body: JSON.stringify({
      merchant: process.env.ZIBAL_MERCHANT_KEY,
      redirectUrl: encodeURIComponent(AppConfig.payRedirectUrl()),
      mobile: userPhoneNumber,
      description,
      amount,
    }),
    headers: { 'Content-Type': 'application/json' },
  });

  if (fetched.ok) {
    const { result: code, trackId } = await fetched.json();

    switch (code) {
      case 102:
      case 103:
      case 104:
        Bugsnag.notify(
          new Error(`Something is wrong with Zibal Merchant (Code: ${fetched.result})`),
        );
        break;
      case 106:
        Bugsnag.notify(new Error('Invalid Zibal Redirect URL'));
        break;
      case 105:
        Bugsnag.notify(
          new Error(`Payment amount must be greater than 1000 rials (Amount: ${amount})`),
        );
        break;
      case 113:
        Bugsnag.notify(
          new Error(`Payment amount must be lower than the limitation (Amount: ${amount})`),
        );
        break;

      default:
    }

    return { code, trackId, gateway: code === 100 ? AppConfig.paymentGateway(trackId) : undefined };
  }

  throw new Error('Something unknown happened');
};

exports.verify = async (trackId) => {
  const fetched = await fetch('https://gateway.zibal.ir/v1/verify', {
    method: 'POST',
    body: JSON.stringify({
      merchant: process.env.ZIBAL_MERCHANT_KEY,
      trackId,
    }),
    headers: { 'Content-Type': 'application/json' },
  });

  if (fetched.ok) {
    const {
      result: code,
      paidAt,
      cardNumber,
      status,
      amount,
      refNumber,
      description,
      orderId,
    } = await fetched.json();

    switch (code) {
      case 102:
      case 103:
      case 104:
        Bugsnag.notify(
          new Error(`Something is wrong with Zibal Merchant (Code: ${fetched.result})`),
        );
        break;

      default:
    }

    return { paidAt, cardNumber, status, amount, code, refNumber, description, orderId };
  }

  throw new Error('Something unknown happened');
};
