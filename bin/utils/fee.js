const AppConfig = require('config/app');

/**
 * Calculates service fee for 1:1 meetings.
 *
 * @param {number} meetingDuration
 *
 * @returns {number} Calculated service fee in Tomans
 */
function calculateServiceFee(meetingDuration) {
  const videoCallFee =
    AppConfig.fee.service.videoCall * AppConfig.dollarExchangeRate * meetingDuration;
  const recordingFee =
    AppConfig.fee.service.recording * AppConfig.dollarExchangeRate * meetingDuration;
  const smsFee = AppConfig.fee.service.sms * AppConfig.dollarExchangeRate;

  return videoCallFee + recordingFee + smsFee;
}

/**
 * Calculates transaction fee for 1:1 meetings.
 *
 * @param {number} baseFee
 * @param {number} platformFee
 * @param {number} serviceFee
 *
 * @returns {number} Maximum transaction fee in Tomans if the calculated fee exceeds, otherwise returns the calculated fee in Tomans.
 */
function calculateTransactionFee(baseFee, platformFee, serviceFee) {
  const transactionFee = (baseFee + platformFee + serviceFee) * AppConfig.fee.transaction.percent;

  if (transactionFee > AppConfig.fee.transaction.max) return AppConfig.fee.transaction.max;
  return transactionFee;
}

module.exports = { calculateServiceFee, calculateTransactionFee };
