const moment = require('moment');
const _ = require('lodash');

/**
 * @param {array} meetings
 */
exports.compactMeetingsDates = (meetings) => {
  const output = {};

  meetings.forEach((meeting) => {
    const UTCStartTimeMoment = moment.utc(meeting.time.start);
    const UTCEndTimeMoment = moment.utc(meeting.time.end);
    const UTCStartDate = UTCStartTimeMoment.format('YYYY-MM-DD');

    if (_.isUndefined(output[UTCStartDate])) output[UTCStartDate] = [];

    output[UTCStartDate].push({
      start: UTCStartTimeMoment.format('YYYY-MM-DDTHH:mm'),
      end: UTCEndTimeMoment.format('YYYY-MM-DDTHH:mm'),
    });
  });

  return output;
};

/**
 * Client sends event answers in a single object.
 * We need them in array.
 *
 * @param {Object} answers
 */
exports.mapAnswersObjToArray = (answers) => {
  return Object.entries(answers).map(([questionId, answer]) => {
    return { question: questionId, answer };
  });
};
