const client = require('@sendgrid/client');
const _ = require('lodash');

client.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * Adds/Updates a contact to given lists
 *
 * @param {string[]} listsIDs
 * @param {{first_name:string,last_name:string,email:string}[]} contacts
 */
exports.addContactToLists = async (listsIDs, contacts) => {
  const request = {
    method: 'PUT',
    url: '/v3/marketing/contacts',
    body: {
      list_ids: listsIDs,
      contacts,
    },
  };

  const [response, body] = await client.request(request);

  if (response.statusCode !== 202) throw new Error(JSON.stringify(body));

  return body;
};

/**
 *
 * @param {string} userEmail
 */
exports.findContactsByEmail = async (userEmail) => {
  const request = {
    method: 'POST',
    url: '/v3/marketing/contacts/search',
    body: {
      query: `email = '${userEmail}'`,
    },
  };

  const [response, body] = await client.request(request);

  if (response.statusCode !== 200) throw new Error(JSON.stringify(body));

  return body;
};

/**
 *
 * @param {string} userEmail
 */
exports.findContactsIdsByEmail = async (userEmail) => {
  const contacts = await this.findContactsByEmail(userEmail);

  /**
   * @type {string[]}
   */
  const ids = contacts.result.reduce((accumulator, currentValue) => {
    accumulator.push(currentValue.id);
    return accumulator;
  }, []);

  return ids;
};

/**
 *
 * @param {string[]} listIDs
 * @param {string[]} contactIDs
 */
exports.removeContactsFromLists = async (listIDs, contactIDs) => {
  /**
   * @type {Promise<[import("@sendgrid/client/src/response").ClientResponse, any]>[]}
   */
  const listsRequests = listIDs.reduce((accumulator, currentListId) => {
    const request = {
      method: 'DELETE',
      url: `/v3/marketing/lists/${currentListId}/contacts`,
      qs: {
        contact_ids: contactIDs.join(','),
      },
    };

    accumulator.push(client.request(request));
    return accumulator;
  }, []);

  const responses = await Promise.all(listsRequests);

  return responses.reduce((accumulator, currentValue) => {
    const [response, body] = currentValue;

    if (response.statusCode !== 202) throw new Error(JSON.stringify(body));

    accumulator.push(body);
    return accumulator;
  }, []);
};

/**
 *
 * @param {string[]} listIDs
 * @param {string} userEmail
 */
exports.removeContactByEmailFromLists = async (listIDs, userEmail) => {
  const contacts = await this.findContactsIdsByEmail(userEmail);

  return this.removeContactsFromLists(listIDs, contacts);
};

/**
 *
 * @param {string[]} contacts
 */
exports.deleteContacts = async (contacts) => {
  const request = {
    method: 'DELETE',
    url: '/v3/marketing/contacts',
    qs: {
      ids: _.join(contacts, ','),
    },
  };

  const [response, body] = await client.request(request);

  if (response.statusCode !== 202) throw new Error(JSON.stringify(body));

  return body;
};

/**
 *
 * @param {string} userEmail
 */
exports.deleteContactByEmail = async (userEmail) => {
  const contacts = await this.findContactsIdsByEmail(userEmail);

  if (_.isEmpty(contacts)) return;

  return this.deleteContacts(contacts);
};
