/**
 * Wraps `error.status` and `error.message` in an object.
 * This is helpful for preparing response object.
 *
 * @param {Error} error
 */
exports.respondError = (error) => ({
  error: {
    status: error.status || 500,
    message: error.message,
  },
});

/**
 * Wraps `status` and `data` in an object.
 * This is helpful for preparing response object.
 * It's possible to pass data in the first argument because status has set `200` by default.
 *
 * @param {any} status
 * @param {any} data
 */
exports.respondSuccess = (status, data) => {
  data = data === undefined ? status : data;
  status = typeof status === 'number' ? status : 200;
  return { code: status, data };
};
