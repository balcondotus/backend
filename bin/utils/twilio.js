const {
  Twilio,
  jwt: { AccessToken },
} = require('twilio');

const client = new Twilio(process.env.TWILIO_API_KEY_SID, process.env.TWILIO_API_KEY_SECRET, {
  accountSid: process.env.TWILIO_ACCOUNT_SID,
});

// TODO: Twilio doesn't work without a VPN, test it with/without VPN

/**
 * Sends an SMS notification to the number.
 *
 * @param {String} to Valid and verified number, including country code
 * @param {String} body
 */
exports.notifyWithSms = (to, body) => {
  const notificationOpts = {
    toBinding: JSON.stringify({
      binding_type: 'sms',
      address: to,
    }),
    body,
  };

  return client.notify
    .services(process.env.TWILIO_NOTIFY_SID)
    .notifications.create(notificationOpts);
};

/**
 * Sends an SMS along with a verification code.
 *
 * @param {String} to Valid number, including country code
 */
exports.sendVerificationCodeWithSms = (to) => {
  return client.verify
    .services(process.env.TWILIO_VERIFY_SID)
    .verifications.create({ channel: 'sms', to });
};

/**
 * Checks verification code.
 *
 * @param {String} to Valid number, including country code
 * @param {String} code User verification input
 *
 * @returns {Boolean} True means verified
 */
exports.verifyCode = async (to, code) => {
  const instance = await client.verify
    .services(process.env.TWILIO_VERIFY_SID)
    .verificationChecks.create({ code, to });

  return instance.status === 'approved';
};

/**
 * Creates an access token for the given user to access the given room.
 *
 * @param {String} userId Used as access token identity.
 * @param {String} meetingId Used as room name.
 */
exports.createVideoRoomAccessToken = (userId, meetingId) => {
  const { VideoGrant } = AccessToken;

  // Create an Access Token
  const accessToken = new AccessToken(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_API_KEY_SID,
    process.env.TWILIO_API_KEY_SECRET,
  );

  // Set the Identity of this token
  accessToken.identity = userId;

  // Grant access to Video
  const grant = new VideoGrant({ room: meetingId });
  accessToken.addGrant(grant);

  // Serialize the token as a JWT
  const jwt = accessToken.toJwt();

  return jwt;
};

/**
 * Creates a room for the given meeting ID.
 *
 * @param {String} meetingId
 */
exports.createSmallRoom = (meetingId) => {
  return client.video.rooms.create({
    recordParticipantsOnConnect: true,
    type: 'group-small',
    maxParticipants: 2,
    uniqueName: meetingId,
    enableTurn: true,
    videoCodecs: 'VP8',
  });
};

/**
 * Creates a grid composition hook.
 */
exports.createGridCompositionHook = () => {
  return client.video.compositionHooks.create({
    friendlyName: 'GridCompositionHook',
    audioSources: '*',
    videoLayout: {
      grid: {
        video_sources: ['*'],
      },
    },
    format: 'webm',
  });
};
