const _ = require('lodash');

function stripAllHtmlTags(text) {
  if (_.isString(text)) return text.replace(/(<([^>]+)>)/gi, '');
  return text;
}

function stripSpaces(text) {
  if (_.isString(text)) return text.replace(/\s+/g, '');
  return text;
}

module.exports = { stripAllHtmlTags, stripSpaces };
