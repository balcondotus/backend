const { DashboardEvent } = require('./events');
const { AuthMiddleware } = require('./middlewares');
const SOCKET = require('./consts/socket');

/**
 * Dashboard namespace.
 *
 * @param {SocketIO.Server} io
 */
const dashboard = (io) => {
  io.of(SOCKET.NAMESPACE.DASHBOARD)
    .use(async (socket, next) => {
      // Authorize for the first incoming packet.
      const { token } = socket.handshake.query;

      try {
        const authUser = await AuthMiddleware.authorize(token);

        // Access authenticated user through the socket object.
        socket.user = authUser;

        return next();
      } catch (e) {
        // Cannot pass customized response through SocketIO
        // Client needs to determine if User is not authorized.
        if (e.name && e.name === 'UnauthorizedError') return next(new Error('Unauthorized'));
        next(e);
      }
    })
    .on('connect', (socket) => {
      // Authorize for every incoming packet.
      socket.use(async (packet, next) => {
        const { token } = socket.request._query;

        try {
          await AuthMiddleware.authorize(token);

          return next();
        } catch (e) {
          // Cannot pass customized response through SocketIO
          // Client needs to determine if User is not authorized.
          if (e.name && e.name === 'UnauthorizedError') return next(new Error('Unauthorized'));
          next(e);
        }
      });

      /**
       * Join the current socket to it's private room
       * for getting realtime notifications.
       */
      socket.join(SOCKET.ROOM.PRIVATE(socket.user._id), (err) => {});

      // Events
      DashboardEvent.markNotificationsRead(socket);
      DashboardEvent.getUnreadNotifications(socket);
      DashboardEvent.getBalance(socket);
      DashboardEvent.getMeetings(socket);
      DashboardEvent.cancelMeetings(socket);
      DashboardEvent.saveNotificationChange(socket);
      DashboardEvent.unlinkOauth(socket);
      DashboardEvent.saveAccountChanges(socket);
      DashboardEvent.deleteAccount(socket);
      DashboardEvent.sendEmailVerification(socket);
      DashboardEvent.sendPhoneVerification(socket);
      DashboardEvent.verifyPhoneVerificationCode(socket);
      DashboardEvent.saveFinanceSettings(socket);
      DashboardEvent.saveProfileSettings(socket);
      DashboardEvent.saveCalendarSettings(socket);
      DashboardEvent.getPayments(socket);
      DashboardEvent.getUserDefaultEvent(socket);
      DashboardEvent.updateUserOptions(socket);
      DashboardEvent.savePassword(socket);
      DashboardEvent.disableNotificationsByMethod(socket);
    });
};

module.exports = { dashboard };
