const passport = require('passport');
const refresh = require('passport-oauth2-refresh');
const { Strategy: JWTStrategy, ExtractJwt } = require('passport-jwt');
const { Strategy: AnonymousStrategy } = require('passport-anonymous');
const { Strategy: GoogleTokenStrategy } = require('passport-google-token3');
const FacebookTokenStrategy = require('passport-facebook-token');
const { NotFound } = require('http-errors');
const { AuthController } = require('lib/controllers');
const { UserModel } = require('lib/models');

module.exports = () => {
  passport.serializeUser((user, done) => {
    done(null, user._id);
  });

  passport.deserializeUser((id, done) => {
    UserModel.findById(id, (err, user) => {
      done(err, user);
    });
  });

  const jwt = new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
      issuer: process.env.JWT_ISSUER,
      audience: process.env.JWT_AUDIENCE,
      passReqToCallback: true,
    },
    (req, jwtPayload, done) =>
      UserModel.findById(jwtPayload._id, (err, user) => {
        if (err) return done(err);
        if (!user) return done(new NotFound('User not found!'));

        return done(null, user);
      }),
  );

  const anonymous = new AnonymousStrategy();

  const googleTokenStrategy = new GoogleTokenStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      passReqToCallback: true,
    },
    (req, accessToken, refreshToken, profile, done) =>
      AuthController.authenticate(req.user, accessToken, refreshToken, req.query, profile, done),
  );

  const facebookTokenStrategy = new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
      passReqToCallback: true,
    },
    (req, accessToken, refreshToken, profile, done) =>
      AuthController.authenticate(req.user, accessToken, refreshToken, req.query, profile, done),
  );

  passport.use('anonymous', anonymous);
  passport.use('jwt', jwt);
  passport.use('google-token', googleTokenStrategy);
  passport.use('facebook-token', facebookTokenStrategy);

  refresh.use('google-token', googleTokenStrategy);
};
