// Socket namespaces
const namespaces = require('lib/namespaces');

/**
 *
 * @param {import("socket.io").Server} io
 */
module.exports = (io) => {
  global.io = io; // Access the server everywhere through global.io

  namespaces.dashboard(io);
};
