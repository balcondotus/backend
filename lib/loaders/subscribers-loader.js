const EventEmitter = require('eventemitter3');
const subscribers = require('lib/subscribers');

module.exports = () => {
  const EE = new EventEmitter();
  global.ee = EE;

  subscribers(EE);
};
