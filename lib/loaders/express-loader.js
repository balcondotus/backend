const helmet = require('helmet');
const path = require('path');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const morgan = require('morgan');
const Bugsnag = require('@bugsnag/js');
const AppConfig = require('config/app');
const ResponseUtils = require('bin/utils/response');
const { InternalServerError } = require('http-errors');
const Logger = require('./logger-loader');

/**
 *
 * @param {import("express").Application} app
 */
module.exports = (app) => {
  app.set('host', process.env.APP_HOST || process.env.OPENSHIFT_NODEJS_IP);
  app.set('port', process.env.APP_PORT || process.env.OPENSHIFT_NODEJS_PORT);
  app.set('views', path.join(__dirname, 'public/views'));
  app.set('view engine', 'pug');

  const bugsnagMiddleware = Bugsnag.getPlugin('express');
  // This must be the first piece of middleware in the stack.
  // It can only capture errors in downstream middleware
  app.use(bugsnagMiddleware.requestHandler);

  const determineCorsOrigin = () => {
    if (process.env.NODE_ENV === 'production') return AppConfig.clientOriginProd;
    if (process.env.NODE_ENV === 'staging') return AppConfig.clientOriginStage;
    if (process.env.NODE_ENV === 'development') return AppConfig.clientOriginDev;

    throw new InternalServerError(`${process.env.NODE_ENV} is not implemented here`);
  };

  app.use(helmet());
  app.use(compression());
  app.use(passport.initialize());
  app.use(
    cors({
      origin: determineCorsOrigin(),
    }),
  );
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // App routes
  app.use('/', require('lib/routes'));

  // Log http requests.
  app.use(morgan('dev'));

  app.use((err, req, res, next) => {
    // Log unhandled exceptions.
    const status = err.status || err.statusCode || 500;

    if (app.get('env') === 'production') {
      if (status >= 500) {
        Logger.error(err.message);
      }
    } else {
      Logger.error(err.message);
    }

    next(err);
  });

  app.use((err, req, res, next) => {
    // Respond unhandled exceptions.
    const errorResponse = ResponseUtils.respondError(err);
    res.status(errorResponse.error.status).json(errorResponse);

    next(err);
  });

  // This must be the end piece of middleware in the stack.
  // This handles any errors that Express catches
  app.use((err, req, res, next) => {
    const status = err.status || err.statusCode || 500;

    if (status >= 500) bugsnagMiddleware.errorHandler(err, req, res, next);
  });

  return app;
};
