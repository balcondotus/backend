const moment = require('moment');
require('moment-timezone');
const { extendMoment } = require('moment-range');
// Load locales.
// Make sure to import supported locales here.
require('moment/locale/fa');

module.exports = () => {
  // Set locale En as default
  moment.locale('en');

  extendMoment(moment);
};
