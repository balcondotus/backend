const { setQueues } = require('bull-board');
const reminderJob = require('lib/jobs/reminder-job');
const payoutJob = require('lib/jobs/payout-job');

module.exports = () => {
  const reminderQueue = reminderJob();
  const payoutQueue = payoutJob();

  // Define all queues for bull-board
  setQueues([reminderQueue, payoutQueue]);
};
