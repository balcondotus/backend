const mongoose = require('mongoose');
const Bugsnag = require('@bugsnag/js').default;
const Logger = require('./logger-loader');

module.exports = () => {
  mongoose.set('useFindAndModify', false);
  mongoose.set('useCreateIndex', true);
  mongoose.set('useNewUrlParser', true);
  mongoose.set('useUnifiedTopology', true);

  const connection = mongoose.connect(process.env.MONGODB_URI);

  mongoose.connection.on('error', (err) => {
    Logger.error(err);
    Bugsnag.notify(err);

    process.exit();
  });

  return connection;
};
