const winston = require('winston');
const { InternalServerError } = require('http-errors');

const transports = [];
if (process.env.NODE_ENV !== 'development') {
  transports.push(new winston.transports.Console());
} else {
  transports.push(
    new winston.transports.Console({
      format: winston.format.combine(winston.format.cli(), winston.format.splat()),
    }),
  );
}

function determineLoggerLevel() {
  if (process.env.NODE_ENV === 'production') return 'warn';
  if (process.env.NODE_ENV === 'staging') return 'http';
  if (process.env.NODE_ENV === 'development') return 'debug';

  throw new InternalServerError(`${process.env.NODE_ENV} is not implemented here`);
}

module.exports = winston.createLogger({
  level: determineLoggerLevel(),
  levels: winston.config.npm.levels,
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DDTHH:mm:ss.sssZ',
    }),
    winston.format.errors({ stack: true }),
    winston.format.splat(),
    winston.format.json(),
  ),
  transports,
});
