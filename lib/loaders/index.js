const bugsnagLoader = require('./bugsnag-loader');
const expressLoader = require('./express-loader');
const momentLoader = require('./moment-loader');
const mongooseLoader = require('./mongoose-loader');
const passportLoader = require('./passport-loader');
const redisLoader = require('./redis-loader');
const twilioLoader = require('./twilio-loader');
const jobsLoader = require('./jobs-loader');
const socketLoader = require('./socket-loader');
const subscribersLoader = require('./subscribers-loader');

/**
 * @param {import("express").Application} app
 * @param {import("socket.io").Server} io
 */
exports.init = async (app, io) => {
  // Order is important
  bugsnagLoader();
  await mongooseLoader();
  redisLoader();
  momentLoader();
  passportLoader();
  twilioLoader();
  expressLoader(app);
  socketLoader(io);
  jobsLoader(io);
  subscribersLoader();
};
