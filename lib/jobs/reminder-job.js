const Queue = require('bull');
const { MeetingModel } = require('lib/models');
const EVENT = require('lib/consts/event');

module.exports = () => {
  const reminderQueue = new Queue(
    'send meetings reminders',
    `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
  );

  reminderQueue.process(async (job, done) => {
    const meetings = await MeetingModel.findRemindNeededMeetings();

    job.progress(0);

    meetings.forEach(async (meeting, index) => {
      global.ee.emit(EVENT.MEETING.REMIND, meeting._id);
      job.progress(((index + 1) / meetings.length) * 100);
    });

    job.progress(100);

    done();
  });

  // Repeat reminder job every minute.
  reminderQueue.add(null, { repeat: { cron: '* * * * *' } });

  return reminderQueue;
};
