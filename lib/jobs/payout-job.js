const Queue = require('bull');
const { MeetingModel } = require('lib/models');
const EVENT = require('lib/consts/event');

module.exports = () => {
  const payoutQueue = new Queue(
    'payout to hosts for successful meetings',
    `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
  );

  payoutQueue.process(async (job, done) => {
    const meetings = await MeetingModel.findSuccessfulMeetings();

    job.progress(0);

    meetings.forEach(async (meeting, index) => {
      await meeting.confirm();

      global.ee.emit(EVENT.MEETING.CONFIRMED, meeting._id);

      job.progress(((index + 1) / meetings.length) * 100);
    });

    job.progress(100);

    done();
  });

  // Repeat payout job every 15 minutes.
  payoutQueue.add(null, { repeat: { cron: '*/15 * * * *' } });

  return payoutQueue;
};
