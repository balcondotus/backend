const AuthService = require('lib/services/oauth-service');
const AccountService = require('lib/services/account-service');

/**
 * Authenticates Facebook and Google accounts.
 */
exports.authenticate = async (reqUser, accessToken, refreshToken, params, profile, done) => {
  try {
    const authenticatedUserDoc = await AuthService.authenticate(
      reqUser,
      accessToken,
      refreshToken,
      params,
      profile,
    );
    const authenticatedUserPlain = await AccountService.getLeanUser(authenticatedUserDoc._id);

    return done(null, authenticatedUserPlain, { isNew: authenticatedUserDoc.isNew });
  } catch (e) {
    done(e);
  }
};
