const ResponseUtils = require('bin/utils/response');
const NotificationService = require('lib/services/notification-service');

/**
 * Mark all unread notifications as read if any for the provided user ID.
 *
 * @param {ObjectId} userId
 */
exports.markNotificationsRead = async (userId) => {
  try {
    await NotificationService.markNotificationsRead(userId);

    return ResponseUtils.respondSuccess('Notifications cleared.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

exports.disableNotificationsByMethod = async (userId, method) => {
  try {
    await NotificationService.disableNotificationsByMethod(userId, method);

    return ResponseUtils.respondSuccess('Notifications updated.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Get all unread notifications along with today received ones.
 *
 * @param {ObjectId} userId
 */
exports.getUnreadNotifications = async (userId) => {
  try {
    const notifications = await NotificationService.getUnreadNotifications(userId);

    return ResponseUtils.respondSuccess(notifications);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Saves a single notification
 *
 * @param {String} notification
 * @param {String} method
 */
exports.saveSettings = async (userId, notification, method) => {
  try {
    await NotificationService.saveSettings(userId, notification, method);

    return ResponseUtils.respondSuccess('Notification change has saved.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
