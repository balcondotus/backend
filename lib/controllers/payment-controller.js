const ResponseUtils = require('bin/utils/response');
const PaymentService = require('lib/services/payment-service');

/**
 * Gets an updated balance of the provided user.
 *
 * @param {ObjectId} userId
 */
exports.getBalance = async (userId) => {
  try {
    const balance = await PaymentService.getBalance(userId);

    return ResponseUtils.respondSuccess(balance);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Saves payment settings.
 *
 * @param {ObjectId} userId
 * @param {null|String} iban
 */
exports.saveSettings = async (userId, iban) => {
  try {
    await PaymentService.saveSettings(userId, iban);

    return ResponseUtils.respondSuccess('Payment settings has saved.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Gets User's payments list.
 *
 * @param {ObjectId} userId
 */
exports.getPayments = async (userId) => {
  try {
    const userPayments = await PaymentService.getPayments(userId);

    return ResponseUtils.respondSuccess(userPayments);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Validates meeting time.
 *
 * @param {ObjectId} userId
 * @param {Number} amount
 */
exports.pay = async (userId, amount) => {
  try {
    const paymentGateway = await PaymentService.pay(userId, amount);

    return ResponseUtils.respondSuccess({ ...paymentGateway });
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

exports.verify = async (userId, payToken) => {
  try {
    const verifyResult = await PaymentService.verify(userId, payToken);

    return ResponseUtils.respondSuccess({
      ...verifyResult,
    });
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
