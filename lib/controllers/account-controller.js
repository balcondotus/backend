const _ = require('lodash');
const ResponseUtils = require('bin/utils/response');
const AccountService = require('lib/services/account-service');

/**
 * Signs up.
 *
 * @param {String} email
 * @param {String} password
 * @param {String} firstName
 * @param {String} lastName
 */
exports.signup = async (email, password, firstName, lastName) => {
  try {
    const authenticatedUser = await AccountService.signup(email, password, firstName, lastName);

    return ResponseUtils.respondSuccess(authenticatedUser);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Signs in.
 *
 * @param {String} email
 * @param {String} password
 */
exports.signin = async (email, password) => {
  try {
    const authenticatedUser = await AccountService.signin(email, password);

    return ResponseUtils.respondSuccess(authenticatedUser);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Unlinks oauth provider safely.
 *
 * @param {ObjectId} userId
 * @param {String} provider One of supported providers
 */
exports.unlinkOauth = async (userId, provider) => {
  try {
    await AccountService.unlinkOauth(userId, provider);

    return ResponseUtils.respondSuccess(`Your account has unlinked from ${_.startCase(provider)}.`);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Saves account changes
 *
 * @param {ObjectId} userId
 * @param {String} phone
 * @param {String} email
 * @param {String} timeZone
 * @param {String} locale
 */
exports.saveAccountChanges = async (userId, phone, email, timeZone, locale) => {
  try {
    await AccountService.saveAccountChanges(userId, phone, email, timeZone, locale);

    return ResponseUtils.respondSuccess('Account changes has saved.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Saves password.
 *
 * @param {ObjectId} userId
 * @param {String} currentPassword
 * @param {String} newPassword
 */
exports.savePassword = async (userId, currentPassword, newPassword) => {
  try {
    await AccountService.savePassword(userId, currentPassword, newPassword);

    return ResponseUtils.respondSuccess('Password has changed.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Deletes the user's account safely.
 *
 * @param {Document} user
 */
exports.deleteAccount = async (user) => {
  try {
    await AccountService.deleteAccount(user);

    return ResponseUtils.respondSuccess('Your account has deleted.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Sends a verification code to User's phone number.
 *
 * @param {ObjectId} userId
 */
exports.sendPhoneVerificationCode = async (userId) => {
  try {
    await AccountService.sendPhoneVerificationCode(userId);

    return ResponseUtils.respondSuccess('Verification code has send.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

exports.verifyPhoneVerificationCode = async (userId, code) => {
  try {
    await AccountService.verifyPhoneVerificationCode(userId, code);

    return ResponseUtils.respondSuccess('Verification has done successfully.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Updates User options.
 *
 * @param {ObjectId} userId
 * @param {String} optionKey
 * @param {any} optionValue
 */
exports.updateUserOption = async (userId, optionKey, optionValue) => {
  try {
    await AccountService.updateUserOption(userId, optionKey, optionValue);

    return ResponseUtils.respondSuccess('User option has changed');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
