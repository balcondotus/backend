const ResponseUtils = require('bin/utils/response');
const ScheduleService = require('lib/services/schedule-service');

/**
 * Gets Host profile.
 *
 * @param {String} userIdentifier User's username or ID
 */
exports.getHostProfile = async (userIdentifier) => {
  try {
    const host = await ScheduleService.getHostProfile(userIdentifier);

    return ResponseUtils.respondSuccess(host);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Gets User's availabilities.
 *
 * @param {String} userId User's ID
 * @param {String} startRange
 * @param {String} endRange
 * @param {String} timeZone
 */
exports.getSpots = async (userId, startRange, endRange, timeZone) => {
  try {
    const spots = await ScheduleService.getSpots(userId, startRange, endRange, timeZone);

    return ResponseUtils.respondSuccess(spots);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Validates meeting time.
 *
 * @param {String} userIdentifier User's username or ID
 * @param {String} spot Supported ISO 8601 strings by moment.js
 * @param {String} slot Valid time interval in the given time zone
 * @param {String} timeZone
 */
exports.validateMeetingTime = async (userIdentifier, spot, slot, timeZone) => {
  try {
    await ScheduleService.validateMeetingTime(userIdentifier, spot, slot, timeZone);

    return ResponseUtils.respondSuccess('Valid');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Handles booking.
 *
 * @param {ObjectId} userId
 */
exports.book = async (userId, userIdentifier, spot, slot, timeZone, answers) => {
  try {
    const meeting = await ScheduleService.book(
      userId,
      userIdentifier,
      spot,
      slot,
      timeZone,
      answers,
    );

    return ResponseUtils.respondSuccess(meeting);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Reschedule.
 *
 * @param {Document} user
 * @param {String} rescheduleId
 * @param {String} spot
 * @param {String} slot
 * @param {String} timeZone
 */
exports.reschedule = async (user, rescheduleId, spot, slot, timeZone, reason) => {
  try {
    const rescheduledMeeting = await ScheduleService.reschedule(
      user,
      rescheduleId,
      spot,
      slot,
      timeZone,
      reason,
    );

    return ResponseUtils.respondSuccess(rescheduledMeeting);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
