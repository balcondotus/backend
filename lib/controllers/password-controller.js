const ResponseUtils = require('bin/utils/response');
const PasswordService = require('lib/services/password-service');

/**
 * Sends a reset password link to the email address if it exists.
 *
 * @param {String} email
 */
exports.forgotPassword = async (email) => {
  try {
    const user = await PasswordService.forgotPassword(email);

    return ResponseUtils.respondSuccess(
      `An e-mail has been sent to ${user.email.email} with further instructions.`,
    );
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Resets the password of the associated account with the token.
 * Sends confirmation email too.
 *
 * @param {String} password
 * @param {String} token
 */
exports.resetPassword = async (password, token) => {
  try {
    await PasswordService.resetPassword(password, token);

    return ResponseUtils.respondSuccess('Success! Your password has been changed.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
