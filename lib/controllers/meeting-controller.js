const moment = require('moment');
const MeetingService = require('lib/services/meeting-service');
const ResponseUtils = require('bin/utils/response');

/**
 * Gets an updated balance of the provided user.
 *
 * @param {ObjectId} userId
 * @param {Date|null} startDate Client sends ISO string
 * @param {Date|null} endDate Client sends ISO string
 */
exports.getMeetings = async (userId, startDate, endDate) => {
  try {
    // It's required because passed Dates from the client are stringified automatically.
    startDate = startDate ? moment(startDate).toDate() : undefined;
    endDate = endDate ? moment(endDate).toDate() : undefined;

    const meetings = await MeetingService.getMeetings(userId, startDate, endDate);

    return ResponseUtils.respondSuccess(meetings);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Cancels meeting.
 *
 * @param {ObjectId} userId
 * @param {String} meetingId
 * @param {String} reason
 */
exports.cancel = async (userId, meetingId, reason) => {
  try {
    const canceledMeeting = await MeetingService.cancel(userId, meetingId, reason);

    return ResponseUtils.respondSuccess(canceledMeeting);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Disputes meeting.
 *
 * @param {Document} user
 * @param {String} meetingId
 * @param {String} reason
 */
exports.dispute = async (user, meetingId, reason) => {
  try {
    const disputedMeeting = await MeetingService.dispute(user, meetingId, reason);

    return ResponseUtils.respondSuccess(disputedMeeting);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Gets meeting details.
 *
 * @param {Document} user
 * @param {any} meetingId
 */
exports.getMeeting = async (user, meetingId) => {
  try {
    const meeting = await MeetingService.getSingleMeeting(user, meetingId);

    return ResponseUtils.respondSuccess(meeting);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Creates an access token to the meeting room for User.
 *
 * @param {Document} user
 * @param {any} meetingId
 */
exports.join = async (user, meetingId) => {
  try {
    const userAccessToken = await MeetingService.join(user, meetingId);

    return ResponseUtils.respondSuccess({ token: userAccessToken });
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
