const ResponseUtils = require('bin/utils/response');
const EmailService = require('lib/services/email-service');

/**
 * Sends a verification to User's email address.
 *
 * @param {ObjectId} userId
 */
exports.sendEmailVerification = async (userId) => {
  try {
    const user = await EmailService.sendEmailVerification(userId);

    return ResponseUtils.respondSuccess(
      `An e-mail has been sent to ${user.email.email} with further instructions.`,
    );
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Verifies User's email by token.
 *
 * @param {String} token
 */
exports.verifyEmail = async (token) => {
  try {
    await EmailService.verifyEmail(token);

    return ResponseUtils.respondSuccess('Email has verified.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
