/**
 * This file exports methods that update people in a namespace/room or specific person.
 */

const SOCKET = require('lib/consts/socket');
const SocketUtils = require('bin/utils/socket');
const ResponseUtils = require('bin/utils/response');
const { PaymentModel, UserModel } = require('lib/models');

/**
 * Sends a balance update to the user if and only if she's connected.
 *
 * @param {ObjectId} userId This user will receive a notification.
 */
exports.updateBalance = async (userId) => {
  try {
    const result = ResponseUtils.respondSuccess(await PaymentModel.getUserBalance(userId));

    SocketUtils.getPrivateRoom(userId).emit(SOCKET.EVENT.BALANCE, result);
  } catch (e) {
    // There's no need to emit an error to the client in update conditions.
    // TODO: Add error to Bugsnag
  }
};

/**
 * Updates user profile subscribers.
 */
exports.updateUser = async (userId) => {
  try {
    const { token, ...user } = ResponseUtils.respondSuccess(await UserModel.getLeanUser(userId));
    SocketUtils.getPrivateRoom(userId).emit(SOCKET.EVENT.USER, user);
  } catch (e) {
    // There's no need to emit an error to the client in update conditions.
    // TODO: Add error to Bugsnag
  }
};
