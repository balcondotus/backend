const ResponseUtils = require('bin/utils/response');
const ProfileService = require('lib/services/profile-service');

/**
 * Saves profile settings.
 *
 * @param {ObjectId} userId
 * @param {null|String} username
 * @param {String} firstName
 * @param {String} lastName
 * @param {null|String} gender
 * @param {Number} rate
 * @param {null|String} tagline
 * @param {null|String} description
 * @param {String} newAvatar
 */
exports.saveSettings = async (
  userId,
  username,
  firstName,
  lastName,
  gender,
  rate,
  tagline,
  description,
  newAvatar,
) => {
  try {
    await ProfileService.saveSettings(
      userId,
      username,
      firstName,
      lastName,
      gender,
      rate,
      tagline,
      description,
      newAvatar,
    );

    return ResponseUtils.respondSuccess('Profile has saved.');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

/**
 * Deletes User's avatar from S3, local temp directory, and DB.
 *
 * @param {ObjectId} userId
 * @param {String} transferId
 */
exports.deleteAvatar = async (userId, transferId) => {
  try {
    await ProfileService.deleteAvatar(userId, transferId);

    return ResponseUtils.respondSuccess('Avatar has deleted');
  } catch (e) {
    // FIXME: AWS throws a NoSuchBucket exception sometimes.
    return ResponseUtils.respondError(e);
  }
};

exports.requestTempFile = (fileLength) => {
  try {
    const tempFilePath = ProfileService.requestTempFile(fileLength);

    return ResponseUtils.respondSuccess(tempFilePath);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

exports.loadRemoteFile = async (userId, remoteFilePath) => {
  try {
    const loadResponse = await ProfileService.loadRemoteFile(userId, remoteFilePath);

    return ResponseUtils.respondSuccess(loadResponse);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
