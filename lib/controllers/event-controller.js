const ResponseUtils = require('bin/utils/response');
const EventService = require('lib/services/event-service');

/**
 * Gets User's default event.
 *
 * @param {ObjectId} userId
 */
exports.getUserDefaultEvent = async (userId) => {
  try {
    const userDefaultEvent = await EventService.getDefaultEventByUserId(userId);

    return ResponseUtils.respondSuccess(userDefaultEvent);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};

exports.getDefaultEventQuestions = async (userIdentifier) => {
  try {
    const userDefaultEvent = await EventService.getDefaultEventByUserIdentifier(userIdentifier);

    return ResponseUtils.respondSuccess(userDefaultEvent.questions);
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
