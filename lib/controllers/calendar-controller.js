const CalendarService = require('lib/services/calendar-service');
const ResponseUtils = require('bin/utils/response');

/**
 * Saves calendar settings.
 *
 * @param {ObjectId} userId
 * @param {Object} availabilities
 * @param {Number} duration
 * @param {Number} frequency
 * @param {Object} buffer
 * @param {Number} meetingsPerDay
 * @param {Number} noticeOfSchedule
 * @param {Object} dateRange
 */
exports.saveSettings = async (
  userId,
  availabilities,
  duration,
  frequency,
  buffer,
  meetingsPerDay,
  noticeOfSchedule,
  dateRange,
) => {
  try {
    await CalendarService.saveSettings(
      userId,
      availabilities,
      duration,
      frequency,
      buffer,
      meetingsPerDay,
      noticeOfSchedule,
      dateRange,
    );

    return ResponseUtils.respondSuccess('Calendar settings has saved');
  } catch (e) {
    return ResponseUtils.respondError(e);
  }
};
