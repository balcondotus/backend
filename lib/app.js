require('app-module-path/cwd');
require('dotenv').config();

const express = require('express');
const chalk = require('chalk');
const http = require('http');
const socketio = require('socket.io');
const loaders = require('./loaders');
const Logger = require('./loaders/logger-loader');

async function startServer() {
  // Express app
  const app = express();
  // HTTP server
  const server = http.createServer(app);

  // Socket server
  const io = socketio(server);

  await loaders.init(app, io);

  // Starts HTTP server
  server.listen(app.get('port'), app.get('host'), () => {
    Logger.info(
      '%s App is running at %s:%d in %s mode',
      chalk.green('✓'),
      app.get('host'),
      app.get('port'),
      app.get('env'),
    );

    Logger.info('  Press CTRL-C to stop\n');
  });
}

startServer();
