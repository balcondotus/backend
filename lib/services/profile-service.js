const _ = require('lodash');
const { BadRequest, NotFound } = require('http-errors');
const fs = require('fs-extra');
const FileType = require('file-type');
const fetch = require('node-fetch').default;
const { UserModel } = require('lib/models');
const AppConfig = require('config/app');
const AwsUtils = require('bin/utils/aws');
const EVENT = require('lib/consts/event');
const tmp = require('tmp');
const path = require('path');
const os = require('os');

/**
 *
 * @param {*} userId
 * @param {*} username
 * @param {*} firstName
 * @param {*} lastName
 * @param {*} gender
 * @param {*} rate
 * @param {*} tagline
 * @param {*} description
 * @param {undefined|string} tempAvatarFilePath If it's non-empty, it can Saved avatar file path in temp directory which can be exists or not because it has been deleted due after saving to AWS
 */
exports.saveSettings = async (
  userId,
  username,
  firstName,
  lastName,
  gender,
  rate,
  tagline,
  description,
  tempAvatarFilePath,
) => {
  if (
    !_.isString(firstName) ||
    !_.isString(lastName) ||
    _.isEmpty(firstName) ||
    _.isEmpty(lastName)
  )
    throw new BadRequest('First name, last name, or both are malformed');

  if (!_.isEmpty(username) && !_.isString(username)) throw new BadRequest('Username is malformed');

  if (!_.isEmpty(gender) && !_.isString(gender)) throw new BadRequest('Gender is malformed');

  if (!_.isEmpty(tagline) && !_.isString(tagline)) throw new BadRequest('Tagline is malformed');

  if (!_.isEmpty(description) && !_.isString(description))
    throw new BadRequest('Description is malformed');

  if (!_.isNumber(rate) || rate < 0)
    throw new BadRequest('Rate is required, and must be greater than 0');
  if (!_.isEmpty(gender) && !AppConfig.allowedGenders.includes(_.toUpper(gender)))
    throw new BadRequest('Gender is incorrect.');

  const user = await UserModel.findById(userId);

  if (username) {
    const userByUsername = await UserModel.findByUsername(username);
    if (userByUsername) {
      if (!userByUsername._id.equals(userId)) throw new BadRequest('Username is already taken');
    } else {
      user.username = username;
    }
  } else {
    user.username = undefined;
  }

  if (!_.isEmpty(tempAvatarFilePath) && _.isString(tempAvatarFilePath)) {
    try {
      // Make sure the file path exists
      fs.accessSync(tempAvatarFilePath, fs.constants.F_OK);

      const tempAvatarFileName = path.basename(tempAvatarFilePath);
      // Upload the avatar if and only if it exists on local
      const result = await AwsUtils.uploadAvatar(tempAvatarFileName, tempAvatarFilePath);
      user.profile.avatar = result.Location;
    } catch (err) {
      // FIXME: AWS throws an InvalidArgument exception sometimes.
    }
  }

  await user.updateUserAndEvent((userDoc, userEvent) => {
    userDoc.profile.firstName = firstName;
    userDoc.profile.lastName = lastName;
    userDoc.profile.gender = gender;

    userEvent.payment.rate = rate;
    userEvent.tagline = tagline;
    userEvent.description = description;
  });

  global.ee.emit(EVENT.USER.SETTINGS.PROFILE_UPDATED, userId);

  // Free the storage after a successful uploading to AWS
  if (!_.isEmpty(tempAvatarFilePath)) {
    fs.access(tempAvatarFilePath, fs.constants.F_OK, (err) => {
      if (!err) {
        // Delete the temp file when it exists.
        fs.removeSync(tempAvatarFilePath);
      }
    });
  }
};

/**
 *
 * @param {*} userId
 * @param {string} transferId Local temp file path or a unique file name
 */
exports.deleteAvatar = async (userId, transferId) => {
  if (_.isEmpty(transferId)) throw new BadRequest('No path given to delete');

  const user = await UserModel.findById(userId).exec();

  if (!user) throw new NotFound('User not found');

  if (user.profile.avatar) {
    const uploadedFileNameOnAWS = path.basename(user.profile.avatar);

    await AwsUtils.deleteAvatar(uploadedFileNameOnAWS);
    user.profile.avatar = undefined;
    await user.save({ validateModifiedOnly: true });

    global.ee.emit(EVENT.USER.AVATAR_DELETED, userId);
  }

  // Delete a local temp file with the given transfer ID
  const savedTempFile = path.isAbsolute(transferId)
    ? transferId
    : path.join(os.tmpdir(), transferId);
  fs.access(savedTempFile, fs.constants.F_OK, (err) => {
    if (!err) fs.removeSync(transferId);
  });
};

exports.requestTempFile = (fileLength) => {
  if (_.isNaN(fileLength) || _.toNumber(fileLength) > AppConfig.maxAvatarFileSize)
    throw new BadRequest(
      `File is not provided or exceeds the limitation of ${AppConfig.maxAvatarFileSize} bytes`,
    );

  return tmp.fileSync({ discardDescriptor: true, prefix: 'ba' });
};

exports.loadRemoteFile = async (userId, remoteFilePath) => {
  const fetchResponse = await fetch(decodeURIComponent(remoteFilePath));
  const loadedFileBuffer = await fetchResponse.buffer();

  // Use the file name in the path to know what to delete from the local temp directory later on
  const loadedFileName = path.basename(remoteFilePath);
  const loadedFileType = await FileType.fromBuffer(loadedFileBuffer);
  const tempLoadedFilePath = path.join(os.tmpdir(), loadedFileName);

  fs.writeFileSync(tempLoadedFilePath, loadedFileBuffer);
  const tempFileReadStream = fs.createReadStream(tempLoadedFilePath);

  return {
    mime: loadedFileType.mime,
    length: loadedFileBuffer.length,
    transferId: loadedFileName,
    readStream: tempFileReadStream,
  };
};
