const { Unauthorized, InternalServerError } = require('http-errors');
const _ = require('lodash');
const moment = require('moment');
const { UserModel } = require('lib/models');
const EVENT = require('lib/consts/event');
const { normalizeEmail, isEmail } = require('validator').default;

/**
 * Authenticate.
 *
 * @param {import('mongoose').Document} reqUser
 * @param {String} accessToken
 * @param {String?} refreshToken
 * @param {*} params
 * @param {Object} profile
 *
 * @returns {Promise<import('mongoose').Document>}
 */
exports.authenticate = async (reqUser, accessToken, refreshToken, params, profile) => {
  const {
    provider,
    id,
    name: { givenName, familyName },
  } = profile;

  if (!_.isUndefined(profile.emails[0].value) && isEmail(profile.emails[0].value)) {
    // Normalize email address
    profile.emails[0].value = normalizeEmail(profile.emails[0].value);
  }

  const existingUser = await UserModel.findByProviderId(provider, id);

  if (existingUser && _.isDate(existingUser.deletedAt))
    throw new Unauthorized(
      `An account associated with ${_.startCase(provider)} exists, but has been deleted`,
    );

  // If already signed in
  if (reqUser) {
    // If a user with the provider ID has found
    if (existingUser) {
      // If the signed in user is the same as the found user
      if (existingUser._id.equals(reqUser._id)) return reqUser;

      throw new Unauthorized(
        `This ${_.startCase(provider)} account has already been using by someone else.`,
      );
    }

    // No user found with the provider ID
    // Link the oauth to the account.
    const user = await UserModel.findById(reqUser._id).exec();

    user.profile.firstName = user.profile.firstName || givenName;
    user.profile.lastName = user.profile.lastName || familyName;

    if (provider === 'facebook') {
      user.oauths.push({
        accountId: id,
        provider,
        accessToken,
      });
    } else if (provider === 'google') {
      if (!user.email.verified && user.email.email === profile.emails[0].value)
        user.email.verified = true;
      user.oauths.push({
        accountId: id,
        accessTokenExpires: moment.utc().add(params.expires_in, 'second').toDate(),
        provider,
        accessToken,
        refreshToken,
      });
    }

    const linkedUserDoc = await user.save({ validateModifiedOnly: true });

    global.ee.emit(EVENT.USER.OAUTH_LINKED, user._id, provider);

    return linkedUserDoc;
  }

  // If the user is NOT signed in

  // If a user with the provider ID exists
  if (existingUser) {
    const shouldSave = await existingUser.assignToken();

    if (shouldSave) {
      const savedUser = await existingUser.save({ validateModifiedOnly: true });

      return savedUser;
    }

    return existingUser;
  }

  // If a user with the provider ID doesn't exist

  const existingUserByEmail = await UserModel.findByEmail(profile.emails[0].value);

  if (existingUserByEmail)
    throw new Unauthorized(
      `The email address associated with this ${_.startCase(
        provider,
      )} account has already been using.`,
    );

  const user = new UserModel({
    email: { email: profile.emails[0].value },
    profile: {
      firstName: givenName,
      lastName: familyName,
    },
  });

  const accessTokenExpires = moment.utc().add(params.expires_in, 'second').toDate();

  if (provider === 'facebook') {
    user.oauths.push({
      accountId: id,
      accessTokenExpires,
      provider,
      accessToken,
    });
  } else if (provider === 'google') {
    user.email.verified = true;

    user.oauths.push({
      accountId: id,
      accessTokenExpires,
      provider,
      accessToken,
      refreshToken,
    });
  }

  const shouldSave = await user.assignToken();

  if (!shouldSave) throw new InternalServerError("Couldn't create a token");

  const savedUser = await user.createUserAndEvent();

  global.ee.emit(EVENT.USER.SIGNED_UP, savedUser._id);

  return savedUser;
};
