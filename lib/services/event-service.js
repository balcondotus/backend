const { BadRequest } = require('http-errors');
const _ = require('lodash');
const { EventModel, UserModel } = require('lib/models');

exports.getDefaultEventByUserId = (userId) => {
  return EventModel.getUserDefaultEvent(userId);
};

exports.getDefaultEventByUserIdentifier = async (userIdentifier) => {
  if (!_.isString(userIdentifier) || _.isEmpty(userIdentifier))
    throw new BadRequest('Invalid user identifier');

  const user = await UserModel.getUserByIdentifier(userIdentifier);
  const userDefaultEvent = await EventModel.getUserDefaultEvent(user._id);

  return userDefaultEvent;
};
