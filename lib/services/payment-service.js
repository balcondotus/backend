const { BadRequest } = require('http-errors');
const _ = require('lodash');
const { PaymentModel, UserModel } = require('lib/models');
const PayUtils = require('bin/utils/pay');
const EVENT = require('lib/consts/event');

exports.getBalance = async (userId) => {
  return PaymentModel.getUserBalance(userId);
};

exports.saveSettings = async (userId, iban) => {
  const user = await UserModel.findById(userId).exec();
  user.iban = iban;

  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.USER.SETTINGS.PAYMENT_UPDATED, userId);
};

exports.getPayments = async (userId) => {
  return PaymentModel.getPaymentsForClient(userId);
};

/**
 *
 * @param {*} userId
 * @param {Number} amount Amount in Tomans
 */
exports.pay = async (userId, amount) => {
  if (!_.isNumber(amount) || amount <= 0) throw new BadRequest('Invalid amount number');

  // Minimum amount at Zibal.ir is 1000 rials.
  amount = amount < 100 ? 1000 : amount * 10;

  const user = await UserModel.findById(userId).exec();

  const userPhoneNumber =
    !_.isUndefined(user.phone) && user.phone.verified ? user.phone : undefined;

  return PayUtils.request(
    amount,
    userPhoneNumber,
    `Increase my Balcon.us balance by ${amount} rials.`,
  );
};

exports.verify = async (userId, trackId) => {
  if (!_.isString(trackId) || _.isEmpty(trackId)) throw new BadRequest('Invalid pay token');

  const foundByTransId = await PaymentModel.findByTransactionCode(trackId);

  if (!_.isNull(foundByTransId))
    throw new BadRequest(`Track ID (${trackId}) has already been used`);

  const paymentVerification = await PayUtils.verify(trackId);

  if (paymentVerification.code === 100) {
    const savedPayment = await PaymentModel.increaseUserBalance(
      userId,
      _.toNumber(paymentVerification.amount) / 10,
      'Increased balance',
      trackId,
    );

    global.ee.emit(EVENT.USER.BALANCE.CHARGED, savedPayment._id);
  }

  return paymentVerification;
};
