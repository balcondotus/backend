const _ = require('lodash');
const { BadRequest, NotFound } = require('http-errors');
const validator = require('validator');
const { promisify } = require('util');
const crypto = require('crypto');
const moment = require('moment');
const { UserModel } = require('lib/models');
const AppConfig = require('config/app');
const EVENT = require('lib/consts/event');

const randomBytesAsync = promisify(crypto.randomBytes);

exports.forgotPassword = async (email) => {
  if (!_.isString(email) || _.isEmpty(email))
    throw new BadRequest('Email is empty, or it is not string.');
  if (!validator.isEmail(email)) throw new BadRequest('Please enter a valid email address.');

  email = validator.normalizeEmail(email);

  const resetToken = await randomBytesAsync(16).then((buf) => buf.toString('hex'));

  const user = await UserModel.findByEmail(email);

  if (!user) throw new NotFound('Account with that email address does not exist.');

  if (_.isDate(user.deletedAt)) throw new NotFound('User has been deleted');

  const resetExpires = moment.utc().add(1, 'hour').toDate();

  // User may reset her password while she was signing in using authentication.
  // So, she might have no password object in her document which causes an exception.
  if (user.password) {
    user.password.resetToken = resetToken;
    user.password.tokenExpiresAt = resetExpires;
  } else {
    user.password = {
      resetToken,
      tokenExpiresAt: resetExpires,
    };
  }

  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.ACCOUNT.FORGOT_PASSWORD, user._id);

  return user;
};

exports.resetPassword = async (password, token) => {
  if (!_.isString(password) || !_.isString(token) || _.isEmpty(password) || _.isEmpty(token))
    throw new BadRequest('Password and token must be string and not empty.');
  if (!validator.isLength(password, { min: AppConfig.minPasswordLength }))
    throw new BadRequest('Password must be at least 6 characters long');
  if (!validator.isHexadecimal(token)) throw new BadRequest('Invalid Token. Please retry.');

  const user = await UserModel.findByPasswordResetToken(token);

  if (!user) throw new NotFound('Password reset token is invalid or has expired.');

  if (_.isDate(user.deletedAt)) throw new NotFound('User has been deleted');

  user.password.password = password;

  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.ACCOUNT.RESET_PASSWORD, user._id);

  return user;
};
