const _ = require('lodash');
const { BadRequest } = require('http-errors');
const { isValidObjectId } = require('mongoose');
const AppConfig = require('config/app');
const EVENT = require('lib/consts/event');
const { NotificationModel, UserModel } = require('lib/models');

exports.markNotificationsRead = async (userId) => {
  await NotificationModel.markAllAsRead(userId);
};

exports.getUnreadNotifications = async (userId) => {
  return NotificationModel.getUnreadNotifications(userId);
};

exports.disableNotificationsByMethod = async (userId, method) => {
  if (!isValidObjectId(userId)) throw new BadRequest('Invalid user ID');
  if (!_.isString(method) || _.isEmpty(method))
    throw new BadRequest('A method is required and must be string.');

  method = _.toUpper(method);

  if (!['BALCON', 'SMS', 'EMAIL'].includes(method))
    throw new BadRequest('Notification method is not supported.');

  Object.entries(AppConfig.notification)
    .filter(([supportedNotification, supportedMethods]) => {
      return supportedMethods.includes(method);
    })
    .forEach(async ([supportedNotification, supportedMethods]) => {
      await UserModel.disableNotification(userId, supportedNotification, method);
      global.ee.emit(
        EVENT.USER.SETTINGS.NOTIFICATION_UPDATED,
        userId,
        supportedNotification,
        method,
      );
    });
};

exports.saveSettings = async (userId, notification, method) => {
  if (!isValidObjectId(userId)) throw new BadRequest('Invalid user ID');
  if (
    !_.isString(notification) ||
    !_.isString(method) ||
    _.isEmpty(notification) ||
    _.isEmpty(method)
  )
    throw new BadRequest('Fields are required and must be string.');

  method = _.toUpper(method);
  notification = _.toLower(notification);

  if (_.isUndefined(AppConfig.notification[notification]))
    throw new BadRequest('Notification type is not supported.');
  if (!AppConfig.notification[notification].includes(method))
    throw new BadRequest('Notfication method is not supported.');

  await UserModel.saveNotification(userId, _.toLower(notification), method);

  global.ee.emit(EVENT.USER.SETTINGS.NOTIFICATION_UPDATED, userId, notification, method);
};
