const _ = require('lodash');
const validator = require('validator');
const {
  BadRequest,
  NotFound,
  InternalServerError,
  Conflict,
  Forbidden,
  ServiceUnavailable,
} = require('http-errors');
const fetch = require('node-fetch');
const moment = require('moment');
const UserModel = require('lib/models/user-model');
const AppConfig = require('config/app');
const TwilioUtils = require('bin/utils/twilio');
const REST = require('lib/consts/rest');
const { MeetingModel, PaymentModel } = require('lib/models');
const EVENT = require('lib/consts/event');
const { isValidNumber } = require('libphonenumber-js');

exports.getLeanUser = (userId) => {
  return UserModel.getLeanUser(userId);
};

exports.signup = async (email, password, firstName, lastName) => {
  if (
    !_.isString(email) ||
    !_.isString(password) ||
    !_.isString(firstName) ||
    !_.isString(lastName) ||
    _.isEmpty(email) ||
    _.isEmpty(password) ||
    _.isEmpty(firstName) ||
    _.isEmpty(lastName)
  )
    throw new BadRequest('Fields are required and must be string');
  if (!validator.isEmail(email)) throw new BadRequest('Please enter a valid email address.');
  if (!validator.isLength(password, { min: AppConfig.minPasswordLength }))
    throw new BadRequest('Password must be at least 6 characters long');
  if (_.isEmpty(firstName)) throw new BadRequest('First name is required.');
  if (_.isEmpty(lastName)) throw new BadRequest('Last name is required.');

  email = validator.normalizeEmail(email);

  const foundUser = await UserModel.findByEmail(email);

  if (!foundUser) {
    const newUser = new UserModel({
      email: { email },
      password: {
        password,
      },
      profile: { firstName, lastName },
    });

    const shouldSave = await newUser.assignToken();

    if (shouldSave) {
      const savedUser = await newUser.createUserAndEvent();
      global.ee.emit(EVENT.USER.SIGNED_UP, savedUser._id);
      return UserModel.getLeanUser(savedUser._id);
    }

    throw new InternalServerError('Something happened with assigning a new token for a new user.');
  }

  throw new Conflict('Email is already in use by another account.');
};

exports.signin = async (email, password) => {
  if (!_.isString(email) || !_.isString(password) || _.isEmpty(email) || _.isEmpty(password))
    throw new BadRequest('Fields are required and must be string.');
  if (!validator.isEmail(email)) throw new BadRequest('Please enter a valid email address.');
  if (!validator.isLength(password, { min: AppConfig.minPasswordLength }))
    throw new BadRequest('Password must be at least 6 characters long');

  email = validator.normalizeEmail(email);

  const foundUser = await UserModel.findByEmail(email);

  if (!foundUser) throw new NotFound('No account has found');

  if (_.isDate(foundUser.deletedAt)) throw new NotFound('User has been deleted');

  if (!foundUser.password)
    throw new Conflict('This account is not using a password for signing in.');

  const isMatch = await foundUser.password.comparePassword(password);

  if (isMatch) {
    const shouldSave = await foundUser.assignToken();

    if (shouldSave) {
      const savedUser = await foundUser.save({ validateModifiedOnly: true });

      return UserModel.getLeanUser(savedUser._id);
    }

    return UserModel.getLeanUser(foundUser._id);
  }

  throw new NotFound('Invalid email or password.');
};

exports.unlinkOauth = async (userId, provider) => {
  if (!_.isString(provider) || _.isEmpty(provider))
    throw new BadRequest('Provider must be string and not empty.');

  provider = _.toLower(provider);

  if (!AppConfig.supportedOauthProviders.includes(provider))
    throw new BadRequest('Provider is not supported.');

  const user = await UserModel.findById(userId, {
    oauths: true,
    'oauths.accessToken': true,
    'oauths.accountId': true,
    'oauths.provider': true,
    password: true,
  }).exec();

  if (!user.oauths)
    throw new BadRequest(`You have not linked your account to ${_.startCase(provider)} yet.`);

  const oauthIndex = user.oauths.findIndex((oauth) => oauth.provider === provider);
  if (oauthIndex === -1)
    throw new BadRequest(`You have not linked your account to ${_.startCase(provider)} yet.`);

  // Make sure the user has another option to sign in.
  if (user.oauths.length === 1 && !user.password)
    throw new Forbidden(
      `The ${_.startCase(
        provider,
      )} account cannot be unlinked without another form of sign in enabled. Please link another account or add a password.`,
    );

  const oauthAccessToken = user.oauths[oauthIndex].accessToken;
  const oauthAccountId = user.oauths[oauthIndex].accountId;

  let revokeResponse;
  switch (provider) {
    case 'google':
      revokeResponse = await fetch(REST.UNLINK_OAUTH.GOOGLE.URI(oauthAccessToken), {
        method: REST.UNLINK_OAUTH.GOOGLE.METHOD,
      });
      break;
    case 'facebook':
      revokeResponse = await fetch(
        REST.UNLINK_OAUTH.FACEBOOK.URI(oauthAccountId, oauthAccessToken),
        {
          method: REST.UNLINK_OAUTH.FACEBOOK.METHOD,
        },
      );
      break;
    default:
      throw new InternalServerError('Provider supported, but not defined yet.');
  }

  const response = await revokeResponse.json();

  // Facebook returns {error:{type:'OAuthException'}}
  // Google returns {error:'invalid_token'}
  // in the case of expired token
  if (revokeResponse.ok || response.error) {
    // Remove the oauth from the oauths array.
    user.oauths.splice(oauthIndex, 1);

    await user.save({ validateModifiedOnly: true });

    global.ee.emit(EVENT.USER.OAUTH_UNLINKED, userId, provider);

    return;
  }

  throw new ServiceUnavailable("The token can't be deleted for any reason.");
};

/**
 * Saves password.
 *
 * @param {ObjectId} userId
 * @param {String} currentPassword
 * @param {String} newPassword
 */
exports.savePassword = async (userId, currentPassword, newPassword) => {
  if (
    !_.isString(currentPassword) ||
    _.isEmpty(currentPassword) ||
    !_.isString(newPassword) ||
    _.isEmpty(newPassword)
  )
    throw new BadRequest('Passwords must be non-empty strings.');

  const user = await UserModel.findById(userId).exec();

  if (_.isUndefined(user.password)) throw new BadRequest('Set a password by reseting once.');
  if (newPassword.length < AppConfig.minPasswordLength)
    throw new BadRequest(
      `New password must be more than ${AppConfig.minPasswordLength} in length.`,
    );
  if (!(await user.password.comparePassword(currentPassword)))
    throw new BadRequest('Entered current password is wrong.');

  user.password.password = newPassword;

  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.ACCOUNT.RESET_PASSWORD, user._id);
};

exports.saveAccountChanges = async (userId, phone, email, timeZone, locale) => {
  if (!_.isString(email) || !_.isString(timeZone) || !_.isString(locale))
    throw new BadRequest('Fields must be strings.');

  const user = await UserModel.findById(userId).exec();

  if (_.isEmpty(email) || !validator.isEmail(email)) throw new BadRequest('Email is incorrect.');
  if (!_.isEmpty(phone) && (!_.isString(phone) || !isValidNumber(phone)))
    throw new BadRequest('Entered phone number is incorrect.');
  if (_.isEmpty(timeZone) || _.isNull(moment.tz.zone(timeZone)))
    throw new BadRequest('Timezone is not supported');
  if (_.isEmpty(locale) || !AppConfig.supportedLocales.includes(locale))
    throw new BadRequest('Locale is not supported');

  if (phone) {
    if (user.phone) {
      user.phone.phone = phone;
    } else {
      user.phone = { phone };
    }
  } else {
    user.phone = undefined;
  }
  if (email && email !== user.email.email) user.email = { verified: false, email };
  if (timeZone) user.timeZone = timeZone;
  if (locale) user.locale = locale;

  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.USER.SETTINGS.ACCOUNT_UPDATED, userId);
};

exports.deleteAccount = async (user) => {
  const userUpcomingMeetingsCount = await MeetingModel.countUpcomingMeetings(user._id);

  if (userUpcomingMeetingsCount !== 0)
    throw new Forbidden('You have upcoming meetings, make/cancel them first.');

  const userBalance = await PaymentModel.getUserBalance(user._id);

  if (userBalance.balance !== 0)
    throw new Forbidden(
      'You have online balance, please wait till we transfer it to your bank account.',
    );

  await UserModel.deleteUser(user._id);

  global.ee.emit(EVENT.USER.DELETED_ACCOUNT, user._id);
};

exports.sendPhoneVerificationCode = async (userId) => {
  const user = await UserModel.findById(userId).exec();

  if (!user.phone) throw new Forbidden('You set no number to receive a verification.');
  if (user.phone.verified) throw new Forbidden('Your phone number has already been verified.');

  await TwilioUtils.sendVerificationCodeWithSms(user.phone.phone);
};

exports.verifyPhoneVerificationCode = async (userId, code) => {
  if (!_.isString(code) || _.isEmpty(code))
    throw new BadRequest("Verification code is empty, or it's invalid");

  if (code.length !== AppConfig.phoneVerificationCodeLength)
    throw new BadRequest('Verification code is incorrect.');

  const user = await UserModel.findById(userId).exec();

  if (!user.phone) throw new Forbidden('You set no number to verify it.');
  if (user.phone.verified) throw new Forbidden('Your phone number has already been verified.');

  const isVerified = await TwilioUtils.verifyCode(user.phone.phone, code);

  if (!isVerified) throw new BadRequest('Verification has failed.');

  user.phone.verified = true;
  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.USER.PHONE_VERIFIED, user._id);
};

exports.updateUserOption = async (userId, optionKey, optionValue) => {
  if (!_.isString(optionKey) || _.isUndefined(optionValue))
    throw new BadRequest('Option key or option value is malformed');

  const user = await UserModel.findById(userId).exec();

  if (_.isUndefined(user.options[optionKey]))
    throw new BadRequest(`Option key ${optionKey} is not supported`);

  user.options[optionKey] = optionValue;

  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.USER.OPTIONS_UPDATED, userId, optionKey, optionValue);
};
