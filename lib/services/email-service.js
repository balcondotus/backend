const moment = require('moment');
const { promisify } = require('util');
const _ = require('lodash');
const crypto = require('crypto');
const { BadRequest, Forbidden, NotFound } = require('http-errors');
const { UserModel } = require('lib/models');
const EVENT = require('lib/consts/event');

const randomBytesAsync = promisify(crypto.randomBytes);

/**
 *
 * @returns {Promise<import('mongoose').Document>}
 */
exports.sendEmailVerification = async (userId) => {
  const user = await UserModel.findById(userId).exec();

  if (user.email.verified) throw new Forbidden('The email address has been verified.');

  const token = await randomBytesAsync(16).then((buf) => buf.toString('hex'));

  user.email.verificationToken = token;
  user.email.tokenExpiresAt = moment.utc().add(1, 'hour').toDate();
  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.ACCOUNT.VERIFY_EMAIL, user._id, token);

  return user;
};

exports.verifyEmail = async (token) => {
  if (!_.isString(token) || _.isEmpty(token))
    throw new BadRequest('Token is required and must be string.');

  const user = await UserModel.findByEmailVerificationToken(token);

  if (!user) throw new Forbidden('Token is invalid, or it has been expired.');

  if (_.isDate(user.deletedAt)) throw new NotFound('User has been deleted');

  user.email.verified = true;
  await user.save({ validateModifiedOnly: true });

  global.ee.emit(EVENT.USER.EMAIL_VERIFIED, user._id);
};
