const _ = require('lodash');
const { BadRequest, NotFound, Forbidden } = require('http-errors');
const moment = require('moment');
const { isValidObjectId } = require('mongoose');
const MeetingUtils = require('bin/utils/meeting');
const AvailabilityUtils = require('bin/utils/availability');
const { UserModel, EventModel, MeetingModel, PaymentModel } = require('lib/models');
const EVENT = require('lib/consts/event');
const EventUtils = require('bin/utils/event');

exports.getHostProfile = async (hostIdentifier) => {
  if (!_.isString(hostIdentifier) || _.isEmpty(hostIdentifier))
    throw new BadRequest('Invalid Host Identifier');

  const host = await UserModel.getUserByIdentifier(hostIdentifier);
  const hostDefaultEvent = await EventModel.findUserDefaultEventLean(host._id);
  const eventBreakdown = EventUtils.calculateRateBreakdown(
    hostDefaultEvent.payment.rate,
    hostDefaultEvent.duration,
  );

  return {
    ...host,
    event: {
      tagline: hostDefaultEvent.tagline,
      description: hostDefaultEvent.description,
      duration: hostDefaultEvent.duration,
      price: eventBreakdown,
    },
  };
};

/**
 *
 * @param {*} userId
 * @param {string} localRangeStartDate UTC date
 * @param {string} timeZone Invitee's time zone
 */
exports.getSpots = async (userId, localRangeStartDate, localRangeEndDate, timeZone) => {
  if (!_.isString(userId) || _.isEmpty(userId) || !isValidObjectId(userId))
    throw new BadRequest('Provide a valid user ID');
  if (
    !_.isString(localRangeStartDate) ||
    _.isEmpty(localRangeStartDate) ||
    !moment(localRangeStartDate).isValid()
  )
    throw new BadRequest('Invalid start date');
  if (!_.isString(timeZone) || _.isEmpty(timeZone) || _.isNull(moment.tz.zone(timeZone)))
    throw new BadRequest('Invalid time zone');

  const host = await UserModel.findById(userId).exec();

  if (_.isNull(host)) throw new NotFound('Host not found');

  const UTCRangeStartDate = moment.utc(localRangeStartDate).startOf('month').toDate();
  const UTCRangeEndDate = moment.utc(localRangeEndDate).endOf('month').toDate();
  const hostDefaultEvent = await EventModel.findUserDefaultEventLean(host._id);
  const hostMeetings = MeetingUtils.compactMeetingsDates(
    await MeetingModel.getMeetings(host._id, UTCRangeStartDate, UTCRangeEndDate, 'CREATED'),
  );

  return AvailabilityUtils.slots(
    hostDefaultEvent.availabilities,
    hostMeetings,
    UTCRangeStartDate,
    UTCRangeEndDate,
    host.timeZone,
    hostDefaultEvent.frequency,
    hostDefaultEvent.duration,
    timeZone,
    hostDefaultEvent.noticeOfSchedule,
    hostDefaultEvent.buffer.before,
    hostDefaultEvent.buffer.after,
    hostDefaultEvent.dateRange.type,
    hostDefaultEvent.dateRange.value,
    hostDefaultEvent.meetingsPerDay,
  ).local;
};

exports.validateMeetingTime = async (userIdentifier, spot, localSlot, timeZone) => {
  if (!_.isString(userIdentifier) || _.isEmpty(userIdentifier))
    throw new BadRequest('Invalid user identifier');
  if (!_.isString(spot) || _.isEmpty(spot) || !moment(spot).isValid())
    throw new BadRequest('Invalid spot');
  if (!_.isString(localSlot) || _.isEmpty(localSlot) || !moment(localSlot, 'HH:mm').isValid())
    throw new BadRequest('Invalid slot');
  if (!_.isString(timeZone) || _.isEmpty(timeZone) || _.isNull(moment.tz.zone(timeZone)))
    throw new BadRequest('Invalid time zone');

  const host = await UserModel.findByIdentifier(userIdentifier, false);

  if (_.isNull(host)) throw new NotFound('Host not found');

  const hostDefaultEvent = await EventModel.findUserDefaultEventLean(host._id);
  const hostMeetings = MeetingUtils.compactMeetingsDates(
    await MeetingModel.getMeetings(
      host._id,
      moment.utc(spot).startOf('month').toDate(),
      moment.utc(spot).endOf('month').toDate(),
      'CREATED',
    ),
  );

  const slots = AvailabilityUtils.slots(
    hostDefaultEvent.availabilities,
    hostMeetings,
    spot,
    spot,
    host.timeZone,
    hostDefaultEvent.frequency,
    hostDefaultEvent.duration,
    timeZone,
    hostDefaultEvent.noticeOfSchedule,
    hostDefaultEvent.buffer.before,
    hostDefaultEvent.buffer.after,
    hostDefaultEvent.dateRange.type,
    hostDefaultEvent.dateRange.value,
    hostDefaultEvent.meetingsPerDay,
  ).local;

  if (_.isUndefined(slots[spot])) throw new BadRequest('Invalid spot');
  if (!slots[spot].includes(localSlot)) throw new BadRequest('Invalid slot');
};

exports.book = async (inviteeId, hostIdentifier, spot, localSlot, timeZone, answers) => {
  if (!_.isString(hostIdentifier) || _.isEmpty(hostIdentifier))
    throw new BadRequest('Invalid user identifier');
  if (!_.isString(spot) || _.isEmpty(spot) || !moment(spot).isValid())
    throw new BadRequest('Invalid spot');
  if (!_.isString(localSlot) || _.isEmpty(localSlot) || !moment(localSlot, 'HH:mm').isValid())
    throw new BadRequest('Invalid slot');
  if (!_.isString(timeZone) || _.isEmpty(timeZone) || _.isNull(moment.tz.zone(timeZone)))
    throw new BadRequest('Invalid time zone');

  const host = await UserModel.findByIdentifier(hostIdentifier, false);

  if (_.isNull(host)) throw new NotFound('Host Not Found');

  const inviteeBalance = await PaymentModel.getUserBalance(inviteeId);
  const hostEvent = await EventModel.findUserDefaultEventLean(host._id);
  const hostEventPriceBreakdown = EventUtils.calculateRateBreakdown(
    hostEvent.payment.rate,
    hostEvent.duration,
  );

  if (inviteeId.equals(host._id)) throw new BadRequest('Cannot book time with yourself');

  if (inviteeBalance.balance < hostEventPriceBreakdown.total)
    throw new BadRequest('Not enough balance');

  const hostMeetings = MeetingUtils.compactMeetingsDates(
    await MeetingModel.getMeetings(
      host._id,
      moment.utc(spot).startOf('month').toDate(),
      moment.utc(spot).endOf('month').toDate(),
      'CREATED',
    ),
  );

  const slots = AvailabilityUtils.slots(
    hostEvent.availabilities,
    hostMeetings,
    spot,
    spot,
    host.timeZone,
    hostEvent.frequency,
    hostEvent.duration,
    timeZone,
    hostEvent.noticeOfSchedule,
    hostEvent.buffer.before,
    hostEvent.buffer.after,
    hostEvent.dateRange.type,
    hostEvent.dateRange.value,
    hostEvent.meetingsPerDay,
  ).local;

  if (_.isUndefined(slots[spot])) throw new BadRequest('Invalid spot');
  if (!slots[spot].includes(localSlot)) throw new BadRequest('Invalid slot');

  let isAllQuestionsAnswered = true;
  hostEvent.questions.forEach((question) => {
    if (
      question.isRequired &&
      (_.isUndefined(answers[question._id]) || _.isEmpty(answers[question._id]))
    )
      isAllQuestionsAnswered = false;
  });

  if (!isAllQuestionsAnswered) throw new BadRequest('Answer required questions');

  const UTCSpotMoment = moment.tz(`${spot}T${localSlot}`, timeZone).utc();
  const meeting = new MeetingModel({
    event: hostEvent._id,
    host: host._id,
    invitee: inviteeId,
    cost: {
      rate: hostEventPriceBreakdown.breakdown.baseFee,
      transaction: hostEventPriceBreakdown.breakdown.transactionFee,
      platform: hostEventPriceBreakdown.breakdown.platformFee,
      services: hostEventPriceBreakdown.breakdown.serviceFee,
    },
    time: {
      start: UTCSpotMoment.clone().toDate(),
      end: UTCSpotMoment.clone().add(hostEvent.duration, 'minute').toDate(),
    },
    answers: MeetingUtils.mapAnswersObjToArray(answers),
    created: { user: inviteeId },
  });

  const savedMeeting = await meeting.createNewMeeting();

  global.ee.emit(EVENT.MEETING.BOOKED, savedMeeting._id);
  global.ee.emit(
    EVENT.USER.BALANCE.DEDUCTED,
    meeting.invitee,
    savedMeeting._id,
    _.last(savedMeeting.payments),
  );

  const gotMeeting = await MeetingModel.getMeeting(savedMeeting._id);

  return gotMeeting;
};

exports.reschedule = async (user, rescheduleId, spot, localSlot, timeZone, reason) => {
  if (!_.isString(rescheduleId) || _.isEmpty(rescheduleId) || !isValidObjectId(rescheduleId))
    throw new BadRequest('Invalid reschedule ID');
  if (!_.isString(reason) || _.isEmpty(reason)) throw new BadRequest('Invalid reason');
  if (!_.isString(spot) || _.isEmpty(spot) || !moment(spot).isValid())
    throw new BadRequest('Invalid spot');
  if (!_.isString(localSlot) || _.isEmpty(localSlot) || !moment(localSlot, 'HH:mm').isValid())
    throw new BadRequest('Invalid slot');
  if (!_.isString(timeZone) || _.isEmpty(timeZone) || _.isNull(moment.tz.zone(timeZone)))
    throw new BadRequest('Invalid time zone');

  const meetingDoc = await MeetingModel.findById(rescheduleId)
    .populate('host')
    .populate('event')
    .exec();

  if (_.isNull(meetingDoc)) throw new NotFound('Meeting not found');
  if (meetingDoc.status !== 'CREATED')
    throw new Forbidden('Meeting has already rescheduled or canceled');
  if (moment.utc().isSameOrAfter(meetingDoc.time.end))
    throw new Forbidden('Past meetings cannot be rescheduled');
  if (!meetingDoc.host._id.equals(user._id) && !meetingDoc.invitee.equals(user._id))
    throw new Forbidden('You have no access to reschedule');

  const hostEvent = await EventModel.findUserDefaultEventLean(meetingDoc.host._id);

  const hostMeetings = MeetingUtils.compactMeetingsDates(
    await MeetingModel.getMeetings(
      meetingDoc.host._id,
      moment.utc(spot).startOf('month').toDate(),
      moment.utc(spot).endOf('month').toDate(),
      'CREATED',
    ),
  );

  const slots = AvailabilityUtils.slots(
    hostEvent.availabilities,
    hostMeetings,
    spot,
    spot,
    meetingDoc.host.timeZone,
    hostEvent.frequency,
    hostEvent.duration,
    timeZone,
    hostEvent.noticeOfSchedule,
    hostEvent.buffer.before,
    hostEvent.buffer.after,
    hostEvent.dateRange.type,
    hostEvent.dateRange.value,
    hostEvent.meetingsPerDay,
  ).local;

  if (_.isUndefined(slots[spot])) throw new BadRequest('Invalid spot');
  if (!slots[spot].includes(localSlot)) throw new BadRequest('Invalid slot');

  const UTCSpotMoment = moment.tz(`${spot}T${localSlot}`, timeZone).utc();
  const rescheduledMeeting = await MeetingModel.reschedule(
    user._id,
    meetingDoc._id,
    reason,
    UTCSpotMoment.clone().toDate(),
    UTCSpotMoment.clone().add(meetingDoc.event.duration, 'minute').toDate(),
  );

  global.ee.emit(EVENT.MEETING.RESCHEDULED, meetingDoc._id);

  const rescheduledMeetingForClient = await MeetingModel.getMeeting(rescheduledMeeting._id);

  return rescheduledMeetingForClient;
};
