const { isDate } = require('util').types;
const _ = require('lodash');
const moment = require('moment');
const { BadRequest, NotFound, Forbidden } = require('http-errors');
const { isValidObjectId } = require('mongoose');
const TwilioUtils = require('bin/utils/twilio');
const { MeetingModel } = require('lib/models');
const AppConfig = require('config/app');
const EVENT = require('lib/consts/event');

exports.getMeetings = async (userId, startDate, endDate) => {
  if (!startDate && !endDate) throw new BadRequest('Both start and end dates cannot be null');
  if (startDate && !isDate(startDate)) throw new BadRequest('Start date must be a valid date.');
  if (endDate && !isDate(endDate)) throw new BadRequest('End date must be a valid date.');

  const ungroupedMeetings = await MeetingModel.getMeetings(userId, startDate, endDate);

  // Group meetings by date
  return ungroupedMeetings.reduce((groups, meeting) => {
    const date = moment.utc(meeting.time.start).format('YYYY-MM-DD');
    if (!groups[date]) {
      groups[date] = [];
    }

    groups[date].push(meeting);
    return groups;
  }, {});
};

exports.cancel = async (userId, meetingId, reason) => {
  if (!isValidObjectId(meetingId)) throw new BadRequest('Invalid meeting ID');
  if (!_.isString(reason) || _.isEmpty(reason)) throw new BadRequest('Invalid reason');

  const meetingDoc = await MeetingModel.findById(meetingId).exec();

  if (_.isNull(meetingDoc)) throw new NotFound('Meeting not found');
  if (meetingDoc.status !== 'CREATED')
    throw new Forbidden('Meeting has been rescheduled or already canceled');
  if (!meetingDoc.host.equals(userId) && !meetingDoc.invitee.equals(userId))
    throw new Forbidden('Host and Invitee are allowed to cancel the meeting');
  if (moment.utc().isSameOrAfter(meetingDoc.time.start))
    throw new Forbidden('Past or started meetings cannot be canceled');

  const canceledMeetingDoc = await MeetingModel.cancel(meetingId, userId, reason);

  global.ee.emit(EVENT.MEETING.CANCELED, meetingId);
  global.ee.emit(
    EVENT.USER.BALANCE.REFUNDED,
    canceledMeetingDoc.invitee,
    canceledMeetingDoc._id,
    _.last(canceledMeetingDoc.payments)._id,
  );

  return MeetingModel.getCanceledMeeting(meetingId);
};

exports.dispute = async (user, meetingId, reason) => {
  if (!isValidObjectId(meetingId)) throw new BadRequest('Invalid meeting ID');
  if (!_.isString(reason) || _.isEmpty(reason)) throw new BadRequest('Invalid reason');

  const meetingDoc = await MeetingModel.findById(meetingId).exec();

  if (_.isNull(meetingDoc)) throw new NotFound('Meeting not found');
  if (meetingDoc.status === 'DISPUTED') throw new Forbidden('Meeting has already been disputed');
  if (meetingDoc.status === 'CONFIRMED')
    throw new Forbidden('Confirmed meetings cannot be disputed');
  if (meetingDoc.status !== 'CREATED')
    throw new Forbidden('Rescheduled or canceled meetings cannot be disputed');
  if (!meetingDoc.host.equals(user._id) && !meetingDoc.invitee.equals(user._id))
    throw new Forbidden('Host and Invitee are allowed to dispute the meeting');
  if (
    !moment
      .utc()
      .within(
        moment.range(
          moment.utc(meetingDoc.time.start),
          moment.utc(meetingDoc.time.start).add(AppConfig.allowedDisputeHours, 'hour'),
        ),
      )
  )
    throw new Forbidden(
      `Cannot dispute; ${AppConfig.allowedDisputeHours} hours past from the start time of the meeting`,
    );

  const disputeDoc = await MeetingModel.dispute(meetingId, user._id, reason);

  global.ee.emit(EVENT.MEETING.DISPUTED, meetingDoc._id, disputeDoc._id);

  return MeetingModel.getDisputedMeeting(meetingId);
};

exports.getSingleMeeting = async (user, meetingId) => {
  if (!isValidObjectId(meetingId)) throw new BadRequest('Invalid Meeting ID');

  const meetingDoc = await MeetingModel.findById(meetingId).exec();

  if (_.isNull(meetingDoc)) throw new NotFound('Meeting Not Found');
  if (!meetingDoc.host.equals(user._id) && !meetingDoc.invitee.equals(user._id))
    throw new Forbidden("You're not allowed to join the meeting");
  if (moment.utc().isSameOrAfter(meetingDoc.time.end))
    throw new Forbidden('Meeting has been ended');

  return MeetingModel.getMeeting(meetingId);
};

exports.join = async (user, meetingId) => {
  if (!isValidObjectId(meetingId)) throw new BadRequest('Invalid Meeting ID');

  const meetingDoc = await MeetingModel.findById(meetingId).exec();

  if (_.isNull(meetingDoc)) throw new NotFound('Meeting Not Found');
  if (!meetingDoc.host.equals(user._id) && !meetingDoc.invitee.equals(user._id))
    throw new Forbidden("You're not allowed to join the meeting");
  if (!moment.utc().isSameOrAfter(meetingDoc.time.start))
    throw new Forbidden(`Join the meeting ${moment.utc(meetingDoc.time.start).fromNow()}`);
  if (moment.utc().isSameOrAfter(meetingDoc.time.end))
    throw new Forbidden('Meeting has been ended');

  // User uses this token to join the right room.
  return TwilioUtils.createVideoRoomAccessToken(_.toString(user._id), _.toString(meetingId));
};
