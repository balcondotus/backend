const _ = require('lodash');
const { BadRequest } = require('http-errors');
const moment = require('moment');
const IntervalUtils = require('bin/utils/interval');
const { EventModel } = require('lib/models');

exports.saveSettings = async (
  userId,
  availabilities,
  duration,
  frequency,
  buffer,
  meetingsPerDay,
  noticeOfSchedule,
  dateRange,
) => {
  if (
    !_.isObject(availabilities) ||
    !_.isNumber(duration) ||
    !_.isNumber(frequency) ||
    !_.isObject(buffer) ||
    !_.isNumber(meetingsPerDay) ||
    !_.isNumber(noticeOfSchedule) ||
    !_.isObject(dateRange)
  )
    throw new BadRequest('One or more of given arguments are incorrect');

  if (
    Object.keys(availabilities).length !== 7 ||
    !_.isObject(availabilities.sun) ||
    !_.isObject(availabilities.mon) ||
    !_.isObject(availabilities.tue) ||
    !_.isObject(availabilities.wed) ||
    !_.isObject(availabilities.thu) ||
    !_.isObject(availabilities.fri) ||
    !_.isObject(availabilities.sat)
  )
    throw new BadRequest('Availabilities object is incorrect');

  Object.entries(availabilities).forEach(([weekDayName, weekDayObject]) => {
    if (!_.isObject(weekDayObject.specifics) || !_.isArray(weekDayObject.times))
      throw new BadRequest('Availabilities object is incorrect');

    Object.entries(weekDayObject.specifics).forEach(([date, intervals]) => {
      if (
        !_.isString(date) ||
        !moment.utc(date).isValid() ||
        moment.utc(date).format('ddd').toLowerCase() !== weekDayName ||
        !_.isArray(intervals)
      )
        throw new BadRequest('Availabilities object is incorrect');

      intervals.forEach((intervalObject) => {
        if (
          !_.isObject(intervalObject) ||
          _.isUndefined(intervalObject.start) ||
          _.isUndefined(intervalObject.end) ||
          !_.isString(intervalObject.start) ||
          !_.isString(intervalObject.end) ||
          !moment.utc(intervalObject.start, 'HH:mm').isValid() ||
          !moment.utc(intervalObject.end, 'HH:mm').isValid() ||
          !IntervalUtils.isTimeWithinDate(moment.utc(date), intervalObject.start) ||
          !IntervalUtils.isTimeWithinDate(moment.utc(date), intervalObject.end) ||
          !IntervalUtils.makeMoment(moment.utc(date), intervalObject.start).isBefore(
            IntervalUtils.makeMoment(moment.utc(date), intervalObject.end),
          )
        )
          throw new BadRequest('Availabilities object is incorrect');
      });

      if (!_.isEmpty(IntervalUtils.findOverlapingIntervals(intervals)))
        throw new BadRequest('Availabilities object is incorrect');
    });

    weekDayObject.times.forEach((intervalObject) => {
      if (
        !_.isObject(intervalObject) ||
        _.isUndefined(intervalObject.start) ||
        _.isUndefined(intervalObject.end) ||
        !_.isString(intervalObject.start) ||
        !_.isString(intervalObject.end) ||
        !moment.utc(intervalObject.start, 'HH:mm').isValid() ||
        !moment.utc(intervalObject.end, 'HH:mm').isValid()
      )
        throw new BadRequest('Availabilities object is incorrect');
    });

    if (!_.isEmpty(IntervalUtils.findOverlapingIntervals(weekDayObject.times)))
      throw new BadRequest('Availabilities object is incorrect');
  });

  const userDefaultEvent = await EventModel.findUserDefaultEvent(userId);
  userDefaultEvent.availabilities = availabilities;
  userDefaultEvent.duration = duration;
  userDefaultEvent.frequency = frequency;
  userDefaultEvent.buffer = buffer;
  userDefaultEvent.meetingsPerDay = meetingsPerDay;
  userDefaultEvent.noticeOfSchedule = noticeOfSchedule;
  userDefaultEvent.dateRange = dateRange;

  await userDefaultEvent.save({ validateModifiedOnly: true });
};
