const EVENT = require('lib/consts/event');
const NotificationUtils = require('bin/utils/notification');
const TranslationUtils = require('bin/utils/translation');
const TRANSLATION = require('lib/consts/translation');
const UserModel = require('lib/models/user-model');
const AppConfig = require('config/app');

/**
 *
 * @param {import('eventemitter3')} EE
 */
module.exports = (EE) => {
  EE.on(EVENT.ACCOUNT.FORGOT_PASSWORD, async function forgotPassword(userId) {
    try {
      const userDoc = await UserModel.findById(userId, '+password.resetToken').exec();

      NotificationUtils.notifyWithEmail(
        userDoc,
        TranslationUtils.format(
          TRANSLATION.RESET_PASSWORD_EMAIL_SUBJECT,
          userDoc.locale,
          {},
          userDoc.timeZone,
        ),
        TranslationUtils.format(
          TRANSLATION.RESET_PASSWORD_EMAIL_TEXT,
          userDoc.locale,
          {
            userFirstName: userDoc.profile.firstName,
            resetPasswordLink: AppConfig.resetPasswordUrl(userDoc.password.resetToken),
          },
          userDoc.timeZone,
        ),
      );
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.ACCOUNT.RESET_PASSWORD, async function resetPassword(userId) {
    try {
      const userDoc = await UserModel.findById(userId).exec();

      NotificationUtils.notifyWithEmail(
        userDoc,
        TranslationUtils.format(
          TRANSLATION.PASSWORD_HAS_CHANGED_EMAIL_SUBJECT,
          userDoc.locale,
          {},
          userDoc.timeZone,
        ),
        TranslationUtils.format(
          TRANSLATION.PASSWORD_HAS_CHANGED_EMAIL_TEXT,
          userDoc.locale,
          {
            userFirstName: userDoc.profile.firstName,
          },
          userDoc.timeZone,
        ),
        false,
        'high',
      );
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.ACCOUNT.VERIFY_EMAIL, async function verifyEmail(userId, token) {
    try {
      const userDoc = await UserModel.findById(userId).exec();

      NotificationUtils.notifyWithEmail(
        userDoc,
        TranslationUtils.format(
          TRANSLATION.VERIFY_EMAIL_EMAIL_SUBJECT,
          userDoc.locale,
          {},
          userDoc.timeZone,
        ),
        TranslationUtils.format(
          TRANSLATION.VERIFY_EMAIL_EMAIL_TEXT,
          userDoc.locale,
          {
            userFirstName: userDoc.profile.firstName,
            verifyEmailLink: AppConfig.verifyEmailUrl(token),
          },
          userDoc.timeZone,
        ),
        true,
      );
    } catch (e) {
      EE.emit('error', e);
    }
  });
};
