const _ = require('lodash');
const moment = require('moment');
const EVENT = require('lib/consts/event');
const SendGridUtils = require('bin/utils/sendgrid');
const NotificationUtils = require('bin/utils/notification');
const TranslationUtils = require('bin/utils/translation');
const TRANSLATION = require('lib/consts/translation');
const UserModel = require('lib/models/user-model');
const MeetingModel = require('lib/models/meeting-model');
const UpdateController = require('lib/controllers/update-controller');
const PaymentModel = require('lib/models/payment-model');
const AppConfig = require('config/app');

/**
 *
 * @param {import('eventemitter3')} EE
 */
module.exports = (EE) => {
  EE.on(EVENT.USER.SIGNED_UP, async function userSignedUp(userId) {
    try {
      const signedUpUser = await UserModel.findById(userId).exec();
      // Add given user to our marketing lists on SendGrid.
      await SendGridUtils.addContactToLists(
        [
          AppConfig.sendgrid.list.productListId,
          AppConfig.sendgrid.list.offerListId,
          AppConfig.sendgrid.list.surveyListId,
          AppConfig.sendgrid.list.newsletterListId,
        ],
        [
          {
            email: signedUpUser.email.email,
            first_name: signedUpUser.profile.firstName,
            last_name: signedUpUser.profile.lastName,
          },
        ],
      );
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.DELETED_ACCOUNT, async function userSignedUp(userId) {
    try {
      const deletedUser = await UserModel.findById(userId).exec();
      // Delete given user's saved contact from our SendGrid.
      await SendGridUtils.deleteContactByEmail(deletedUser.email.email);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.BALANCE.CHARGED, async function userBalanceUpdated(paymentId) {
    try {
      const payment = await PaymentModel.findById(paymentId).populate('user').exec();
      const { user } = payment;
      const { payment: paymentNotificationMethods } = user.notifications;

      // Send notifications

      NotificationUtils.notifyWithNotification(paymentNotificationMethods.includes('BALCON'), {
        user: user._id,
        type: 'PAYMENT_INCREASED',
        content: {
          refId: payment._id,
          morph: 'Payment',
        },
      });

      // Send emails
      if (paymentNotificationMethods.includes('EMAIL')) {
        NotificationUtils.notifyWithEmail(
          user,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_INCREASED_EMAIL_SUBJECT,
            user.locale,
            {},
            user.timeZone,
          ),
          TranslationUtils.format(
            TRANSLATION.PAYMENT_INCREASED_EMAIL_TEXT,
            user.locale,
            {
              userFirstName: user.profile.firstName,
            },
            user.timeZone,
          ),
          false,
          'low',
        );
      }

      // Send SMSs

      if (paymentNotificationMethods.includes('SMS')) {
        NotificationUtils.notifyWithSms(
          user,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_INCREASED_SMS,
            user.locale,
            {},
            user.timeZone,
          ),
        );
      }

      UpdateController.updateBalance(user._id);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.SETTINGS.NOTIFICATION_UPDATED, async function userNotificationSettingsUpdated(
    userId,
    notificationType,
    methodType,
  ) {
    try {
      const user = await UserModel.findById(userId).exec();

      /**
       *
       * @param {string} notification
       * @param {string} method
       * @param {string[]} listsIds
       */
      const addOrRemoveContactFromSendGrid = (notification, method, listsIds) => {
        method = _.toUpper(method);
        notification = _.toLower(notification);

        if (method !== 'EMAIL') return;

        const isEnabled = user.notifications[notification].includes(method);

        if (isEnabled) {
          SendGridUtils.addContactToLists(listsIds, [
            {
              email: user.email.email,
              first_name: user.profile.firstName,
              last_name: user.profile.lastName,
            },
          ]);
        } else {
          SendGridUtils.removeContactByEmailFromLists(listsIds, user.email.email);
        }
      };

      switch (_.toLower(notificationType)) {
        case 'product':
          addOrRemoveContactFromSendGrid(notificationType, methodType, [
            AppConfig.sendgrid.list.productListId,
          ]);
          break;
        case 'newsletter':
          addOrRemoveContactFromSendGrid(notificationType, methodType, [
            AppConfig.sendgrid.list.newsletterListId,
          ]);
          break;
        case 'survey':
          addOrRemoveContactFromSendGrid(notificationType, methodType, [
            AppConfig.sendgrid.list.surveyListId,
          ]);
          break;
        case 'offer':
          addOrRemoveContactFromSendGrid(notificationType, methodType, [
            AppConfig.sendgrid.list.offerListId,
          ]);
          break;
        default:
      }

      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.OAUTH_LINKED, async function userOauthLinked(userId, provider) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.OAUTH_UNLINKED, async function userOauthUnlinked(userId, provider) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.SETTINGS.ACCOUNT_UPDATED, async function userAccountUpdated(userId) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.PHONE_VERIFIED, async function userPhoneVerified(userId) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.OPTION_UPDATED, async function userOptionsUpdated(
    userId,
    optionKey,
    optionValue,
  ) {
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.EMAIL_VERIFIED, async function userEmailVerified(userId) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.SETTINGS.PAYMENT_UPDATED, async function userPaymentSettingsUpdated(userId) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.SETTINGS.PROFILE_UPDATED, async function userProfileSettingsUpdated(userId) {
    // TODO: Send notifications
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.AVATAR_DELETED, async function userAvatarDeleted(userId) {
    try {
      UpdateController.updateUser(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.BALANCE.DEDUCTED, async function userBalanceDeducted(
    userId,
    meetingId,
    paymentId,
  ) {
    try {
      const userDoc = await UserModel.findById(userId).exec();
      const { payment: paymentNotificationMethods } = userDoc.notifications;
      const meetingDoc = await MeetingModel.findById(meetingId).populate('host').exec();

      NotificationUtils.notifyWithNotification(paymentNotificationMethods.includes('BALCON'), {
        user: userDoc._id,
        type: 'PAYMENT_DEDUCTED',
        content: {
          refId: paymentId,
          morph: 'Payment',
        },
      });

      if (paymentNotificationMethods.includes('EMAIL')) {
        NotificationUtils.notifyWithEmail(
          userDoc,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_DEDUCTED_EMAIL_SUBJECT,
            userDoc.locale,
            {},
            userDoc.timeZone,
          ),
          TranslationUtils.format(
            TRANSLATION.PAYMENT_DEDUCTED_EMAIL_TEXT,
            userDoc.locale,
            {
              userFirstName: userDoc.profile.firstName,
              otherUserFullName: meetingDoc.host.profile.fullName,
            },
            userDoc.timeZone,
          ),
          false,
          'low',
        );
      }

      if (paymentNotificationMethods.includes('SMS')) {
        NotificationUtils.notifyWithSms(
          userDoc,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_DEDUCTED_SMS,
            userDoc.locale,
            {
              otherUserFullName: meetingDoc.host.profile.fullName,
            },
            userDoc.timeZone,
          ),
        );
      }

      UpdateController.updateBalance(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.USER.BALANCE.REFUNDED, async function userBalanceRefunded(
    userId,
    meetingId,
    paymentId,
  ) {
    try {
      const userDoc = await UserModel.findById(userId).exec();
      const { payment: paymentNotificationMethods } = userDoc.notifications;
      const meetingDoc = await MeetingModel.findById(meetingId).populate('host').exec();
      const meetingDate = moment.utc(meetingDoc.time.start).toDate();

      NotificationUtils.notifyWithNotification(paymentNotificationMethods.includes('BALCON'), {
        user: userDoc._id,
        type: 'PAYMENT_REFUNDED',
        content: {
          refId: paymentId,
          morph: 'Payment',
        },
      });

      if (paymentNotificationMethods.includes('EMAIL')) {
        NotificationUtils.notifyWithEmail(
          userDoc,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_REFUNDED_EMAIL_SUBJECT,
            userDoc.locale,
            {},
            userDoc.timeZone,
          ),
          TranslationUtils.format(
            TRANSLATION.PAYMENT_REFUNDED_EMAIL_TEXT,
            userDoc.locale,
            {
              userFirstName: userDoc.profile.firstName,
              otherUserFullName: meetingDoc.host.profile.fullName,
              meetingDateTime: meetingDate,
            },
            userDoc.timeZone,
          ),
          false,
          'low',
        );
      }

      if (paymentNotificationMethods.includes('SMS')) {
        NotificationUtils.notifyWithSms(
          userDoc,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_REFUNDED_SMS,
            userDoc.locale,
            {
              otherUserFullName: meetingDoc.host.profile.fullName,
              meetingDateTime: meetingDate,
            },
            userDoc.timeZone,
          ),
        );
      }

      UpdateController.updateBalance(userId);
    } catch (e) {
      EE.emit('error', e);
    }
  });
};
