const userSubscriber = require('./user-subscriber');
const meetingSubscriber = require('./meeting-subscriber');
const accountSubscriber = require('./account-subscriber');

/**
 *
 * @param {import('eventemitter3')} EE
 */
module.exports = (EE) => {
  EE.on('error', function error(e) {
    throw e;
  });

  userSubscriber(EE);
  meetingSubscriber(EE);
  accountSubscriber(EE);
};
