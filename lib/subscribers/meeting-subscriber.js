const _ = require('lodash');
const moment = require('moment');
const TranslationUtils = require('bin/utils/translation');
const TRANSLATION = require('lib/consts/translation');
const EVENT = require('lib/consts/event');
const NotificationUtils = require('bin/utils/notification');
const MeetingModel = require('lib/models/meeting-model');
const DisputeModel = require('lib/models/dispute-model');
const AppConfig = require('config/app');

/**
 *
 * @param {import('eventemitter3')} EE
 */
module.exports = (EE) => {
  EE.on(EVENT.MEETING.CANCELED, async function meetingCanceled(meetingId) {
    try {
      const canceledMeeting = await MeetingModel.findById(meetingId)
        .populate('host')
        .populate('invitee')
        .populate('canceled.user')
        .exec();

      [canceledMeeting.host, canceledMeeting.invitee].forEach((user) => {
        const { meeting: meetingNotificationMethods } = user.notifications;
        const isHost = user._id.equals(canceledMeeting.host._id);
        const otherParty = !isHost ? canceledMeeting.host : canceledMeeting.invitee;
        const formatedMeetingDate = moment.utc(canceledMeeting.time.start).toDate();

        // Send notifications

        NotificationUtils.notifyWithNotification(meetingNotificationMethods.includes('BALCON'), {
          user: user._id,
          type: 'MEETING_CANCELED',
          content: {
            refId: canceledMeeting._id,
            morph: 'Meeting',
          },
        });

        // Send emails
        if (meetingNotificationMethods.includes('EMAIL')) {
          NotificationUtils.notifyWithEmail(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_CANCELED_EMAIL_SUBJECT,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
              },
              user.timeZone,
            ),
            TranslationUtils.format(
              TRANSLATION.MEETING_CANCELED_EMAIL_TEXT,
              user.locale,
              {
                userFirstName: user.profile.firstName,
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
                cancelReason: canceledMeeting.canceled.reason,
              },
              user.timeZone,
            ),
            false,
            'low',
          );
        }

        // Send SMSs

        if (meetingNotificationMethods.includes('SMS')) {
          NotificationUtils.notifyWithSms(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_CANCELED_SMS,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
                cancelReason: canceledMeeting.canceled.reason,
              },
              user.timeZone,
            ),
          );
        }
      });
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.MEETING.DISPUTED, async function meetingDisputed(meetingId, disputeId) {
    try {
      const meeting = await MeetingModel.findById(meetingId)
        .populate('host')
        .populate('invitee')
        .exec();
      const dispute = await DisputeModel.findById(disputeId).populate('user').exec();

      [meeting.host, meeting.invitee].forEach((user) => {
        const { meeting: meetingNotificationMethods } = user.notifications;
        const isHost = user._id.equals(meeting.host._id);
        const otherParty = !isHost ? meeting.host : meeting.invitee;
        const formatedMeetingDate = moment.utc(meeting.time.start).toDate();

        // Send notifications
        NotificationUtils.notifyWithNotification(meetingNotificationMethods.includes('BALCON'), {
          user: user._id,
          type: 'MEETING_DISPUTED',
          content: {
            refId: meetingId,
            morph: 'Meeting',
          },
        });

        // Send emails
        if (meetingNotificationMethods.includes('EMAIL')) {
          NotificationUtils.notifyWithEmail(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_DISPUTED_EMAIL_SUBJECT,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
              },
              user.timeZone,
            ),
            TranslationUtils.format(
              TRANSLATION.MEETING_DISPUTED_EMAIL_TEXT,
              user.locale,
              {
                userFirstName: user.profile.firstName,
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
                disputeReason: dispute.reason,
              },
              user.timeZone,
            ),
            false,
            'low',
          );
        }

        // Send SMSs

        if (meetingNotificationMethods.includes('SMS')) {
          NotificationUtils.notifyWithSms(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_DISPUTED_SMS,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
                disputeReason: dispute.reason,
              },
              user.timeZone,
            ),
          );
        }
      });
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.MEETING.BOOKED, async function meetingBooked(meetingId) {
    try {
      const meeting = await MeetingModel.findById(meetingId).populate('host').populate('invitee');

      [meeting.host, meeting.invitee].forEach((user) => {
        const { meeting: meetingNotificationMethods } = user.notifications;
        const isHost = user._id.equals(meeting.host._id);
        const otherParty = !isHost ? meeting.host : meeting.invitee;
        const formatedMeetingDate = moment.utc(meeting.time.start).toDate();

        // Send notifications

        NotificationUtils.notifyWithNotification(meetingNotificationMethods.includes('BALCON'), {
          user: user._id,
          type: 'MEETING_CREATED',
          content: {
            refId: meeting._id,
            morph: 'Meeting',
          },
        });

        // Send emails
        if (meetingNotificationMethods.includes('EMAIL')) {
          NotificationUtils.notifyWithEmail(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_CREATED_EMAIL_SUBJECT,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
              },
              user.timeZone,
            ),
            TranslationUtils.format(
              TRANSLATION.MEETING_CREATED_EMAIL_TEXT,
              user.locale,
              {
                userFirstName: user.profile.firstName,
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
                disputeMeetingUrl: AppConfig.disputeMeetingUrl(meeting._id),
                joinMeetingUrl: AppConfig.joinMeetingUrl(meeting._id),
                rescheduleMeetingUrl: AppConfig.rescheduleMeetingUrl(
                  meeting.host.identifier,
                  meeting._id,
                ),
                cancelMeetingUrl: AppConfig.cancelMeetingUrl(meeting._id),
              },
              user.timeZone,
            ),
          );
        }

        // Send SMSs

        if (meetingNotificationMethods.includes('SMS')) {
          NotificationUtils.notifyWithSms(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_CREATED_SMS,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
              },
              user.timeZone,
            ),
          );
        }
      });
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.MEETING.RESCHEDULED, async function meetingRescheduled(meetingId) {
    try {
      const meeting = await MeetingModel.findById(meetingId)
        .populate('host')
        .populate('invitee')
        .populate('rescheduled.rescheduledTo')
        .exec();
      const rescheduledMeeting = meeting.rescheduled.rescheduledTo;

      [meeting.host, meeting.invitee].forEach((user) => {
        const { meeting: meetingNotificationMethods } = user.notifications;
        const isHost = user._id.equals(meeting.host._id);
        const otherParty = !isHost ? meeting.host : meeting.invitee;
        const formatedRescheduledMeetingDate = moment.utc(rescheduledMeeting.time.start).toDate();

        // Send notifications

        NotificationUtils.notifyWithNotification(meetingNotificationMethods.includes('BALCON'), {
          user: user._id,
          type: 'MEETING_RESCHEDULED',
          content: {
            refId: meeting._id,
            morph: 'Meeting',
          },
        });

        // Send emails
        if (meetingNotificationMethods.includes('EMAIL')) {
          NotificationUtils.notifyWithEmail(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_RESCHEDULED_EMAIL_SUBJECT,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedRescheduledMeetingDate,
              },
              user.timeZone,
            ),
            TranslationUtils.format(
              TRANSLATION.MEETING_RESCHEDULED_EMAIL_TEXT,
              user.locale,
              {
                userFirstName: user.profile.firstName,
                otherUserFullName: otherParty.profile.fullName,
                rescheduledDateTime: formatedRescheduledMeetingDate,
                rescheduleReason: meeting.rescheduled.reason,
                disputeRescheduledMeetingUrl: AppConfig.disputeMeetingUrl(rescheduledMeeting._id),
                joinRescheduledMeetingUrl: AppConfig.joinMeetingUrl(rescheduledMeeting._id),
                rescheduleRescheduledMeetingUrl: AppConfig.rescheduleMeetingUrl(
                  meeting.host.identifier,
                  rescheduledMeeting._id,
                ),
                cancelRescheduledMeetingUrl: AppConfig.cancelMeetingUrl(rescheduledMeeting._id),
              },
              user.timeZone,
            ),
          );
        }

        // Send SMSs

        if (meetingNotificationMethods.includes('SMS')) {
          NotificationUtils.notifyWithSms(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_RESCHEDULED_SMS,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                rescheduledDateTime: formatedRescheduledMeetingDate,
                rescheduleReason: meeting.rescheduled.reason,
              },
              user.timeZone,
            ),
          );
        }
      });
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.MEETING.REMIND, async function remindUpcomingMeeting(meetingId) {
    try {
      const meeting = await MeetingModel.findById(meetingId)
        .populate('host')
        .populate('invitee')
        .exec();

      [meeting.host, meeting.invitee].forEach((user) => {
        const { reminder: reminderNotificationMethods } = user.notifications;
        const isHost = user._id.equals(meeting.host._id);
        const otherParty = !isHost ? meeting.host : meeting.invitee;
        const formatedMeetingDate = moment.utc(meeting.time.start).toDate();

        // Send notifications

        NotificationUtils.notifyWithNotification(reminderNotificationMethods.includes('BALCON'), {
          user: user._id,
          type: 'MEETING_REMINDER',
          content: {
            refId: meeting._id,
            morph: 'Meeting',
          },
        });

        // Send emails
        if (reminderNotificationMethods.includes('EMAIL')) {
          NotificationUtils.notifyWithEmail(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_REMINDER_EMAIL_SUBJECT,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
              },
              user.timeZone,
            ),
            TranslationUtils.format(
              TRANSLATION.MEETING_REMINDER_EMAIL_TEXT,
              user.locale,
              {
                userFirstName: user.profile.firstName,
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
                disputeMeetingUrl: AppConfig.disputeMeetingUrl(meeting._id),
                joinMeetingUrl: AppConfig.joinMeetingUrl(meeting._id),
                rescheduleMeetingUrl: AppConfig.rescheduleMeetingUrl(
                  meeting.host.identifier,
                  meeting._id,
                ),
                cancelMeetingUrl: AppConfig.cancelMeetingUrl(meeting._id),
              },
              user.timeZone,
            ),
          );
        }

        // Send SMSs

        if (reminderNotificationMethods.includes('SMS')) {
          NotificationUtils.notifyWithSms(
            user,
            TranslationUtils.format(
              TRANSLATION.MEETING_REMINDER_SMS,
              user.locale,
              {
                otherUserFullName: otherParty.profile.fullName,
                meetingDateTime: formatedMeetingDate,
              },
              user.timeZone,
            ),
          );
        }
      });
    } catch (e) {
      EE.emit('error', e);
    }
  });

  EE.on(EVENT.MEETING.CONFIRMED, async function meetingConfirmed(meetingId) {
    try {
      const meeting = await MeetingModel.findById(meetingId)
        .populate('host')
        .populate('invitee')
        .populate('payments')
        .exec();

      const { payment: paymentNotificationMethods } = meeting.host.notifications;
      const formatedMeetingDate = moment.utc(meeting.time.start).toDate();

      // Send notifications

      NotificationUtils.notifyWithNotification(paymentNotificationMethods.includes('BALCON'), {
        user: meeting.host._id,
        type: 'PAYMENT_RECEIVED',
        content: {
          refId: _.last(meeting.payments)._id,
          morph: 'Payment',
        },
      });

      // Send emails
      if (paymentNotificationMethods.includes('EMAIL')) {
        NotificationUtils.notifyWithEmail(
          meeting.host,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_RECEIVED_EMAIL_SUBJECT,
            meeting.host.locale,
            {
              paymentAmount: meeting.cost.rate,
            },
            meeting.host.timeZone,
          ),
          TranslationUtils.format(
            TRANSLATION.PAYMENT_RECEIVED_EMAIL_TEXT,
            meeting.host.locale,
            {
              userFirstName: meeting.host.profile.firstName,
              paymentAmount: meeting.cost.rate,
              otherUserFullName: meeting.invitee.profile.fullName,
              meetingDateTime: formatedMeetingDate,
            },
            meeting.host.timeZone,
          ),
          false,
          'low',
        );
      }

      // Send SMSs

      if (paymentNotificationMethods.includes('SMS')) {
        NotificationUtils.notifyWithSms(
          meeting.host,
          TranslationUtils.format(
            TRANSLATION.PAYMENT_RECEIVED_SMS,
            meeting.host.locale,
            {
              paymentAmount: meeting.cost.rate,
            },
            meeting.host.timeZone,
          ),
        );
      }
    } catch (e) {
      EE.emit('error', e);
    }
  });
};
