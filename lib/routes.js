const express = require('express');
const { NotFound } = require('http-errors');
const ROUTE = require('./consts/route');

const router = express.Router();

router.use('/queues', require('./routes/queues-route'));
router.use(ROUTE.OAUTH.PATH, require('./routes/oauth'));
router.use(ROUTE.ACCOUNT.PATH, require('./routes/account'));
router.use(ROUTE.SCHEDULE.PATH, require('./routes/schedule'));
router.use(ROUTE.PAYMENT.PATH, require('./routes/payment'));
router.use(ROUTE.EVENT.PATH, require('./routes/event'));
router.use(ROUTE.MEETING.PATH, require('./routes/meeting'));

// catch 404 and forward to error handler
router.use((_req, _res, next) => {
  return next(new NotFound(`Not Found: ${_req.path}`));
});

module.exports = router;
