module.exports = {
  INTERVAL_TIME_PATTERN: /^(?<hours>[0-9]{2}):(?<minutes>[0-9]{2})$/,
  DASHBOARD_URL_PROD: 'https://dashboard.balcon.us',
  DASHBOARD_URL_STAGE: 'https://staging.dashboard.balcon.us',
  DASHBOARD_URL_DEV: 'https://dashboard.balcon.us:3000',
  WEBSITE_URL_PROD: 'https://www.balcon.us',
  WEBSITE_URL_STAGE: 'https://staging.balcon.us',
  WEBSITE_URL_DEV: 'https://www.balcon.us:3001',
  MEETING_URL_PROD: 'https://meeting.balcon.us',
  MEETING_URL_STAGE: 'https://staging.meeting.balcon.us',
  MEETING_URL_DEV: 'https://meeting.balcon.us:3002',
};
