module.exports = {
  MEETING: {
    PATH: '/meeting',
    SUB: {
      CANCEL: '/cancel',
      DISPUTE: '/dispute',
      DETAILS: '/details',
      JOIN: '/join',
    },
  },
  SCHEDULE: {
    PATH: '/schedule',
    SUB: {
      PROFILE: '/profile',
      SPOTS: '/spots',
      VALIDATE: '/validate',
      BOOK: '/book',
      RESCHEDULE: '/reschedule',
    },
  },
  EVENT: {
    PATH: '/event',
    SUB: {
      QUESTIONS: '/questions',
    },
  },
  PAYMENT: {
    PATH: '/payment',
    SUB: {
      PAY: '/pay',
      VERIFY: '/verify',
    },
  },
  ACCOUNT: {
    PATH: '/account',
    SUB: {
      SIGNIN: '/signin',
      SIGNUP: '/signup',
      BALANCE: '/balance',
      UPLOAD: {
        PATH: '/upload',
        SUB: {
          AVATAR: '/avatar',
        },
      },
      EMAIL: {
        PATH: '/email',
        SUB: {
          VERIFY: '/verify',
        },
      },
      PASSWORD: {
        PATH: '/password',
        SUB: {
          FORGOT: '/forgot',
          RESET: '/reset',
        },
      },
    },
  },
  OAUTH: {
    PATH: '/oauth',
    SUB: {
      FACEBOOK: '/facebook',
      GOOGLE: '/google',
    },
  },
};
