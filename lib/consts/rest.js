module.exports = {
  UNLINK_OAUTH: {
    GOOGLE: {
      URI: (token) => {
        return `https://accounts.google.com/o/oauth2/revoke?token=${token}`;
      },
      METHOD: 'POST',
    },
    FACEBOOK: {
      URI: (accountId, token) => {
        return `https://graph.facebook.com/v7.0/${accountId}/permissions?access_token=${token}`;
      },
      METHOD: 'DELETE',
    },
  },
};
