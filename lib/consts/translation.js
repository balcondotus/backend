exports.MEETING_CANCELED_EMAIL_SUBJECT = {
  en: `Meeting Canceled: {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}`,
  fa: `ملاقات کنسل شد: {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm}`,
};

exports.MEETING_CANCELED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because your meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm} has canceled.</div><br /><div>The meeting has canceled with a reason: "<strong>{cancelReason}</strong>"</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا ملاقات&zwnj;تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} لغو شد.</div><br /><div style="direction: rtl;">ملاقات با این دلیل لغو شده‌است: &laquo;<strong>{cancelReason}</strong>&raquo;</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.PAYMENT_REFUNDED_EMAIL_SUBJECT = {
  en: `Payment Refunded: A Meeting Canceled`,
  fa: `بازگشت هزینه ملاقات: یک ملاقات کنسل شد`,
};

exports.PAYMENT_REFUNDED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because you have been refunded the whole payment of your meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}.</div><br /><div>We will transfer your online balance to your bank account within 1-3 working days.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می‌کنید زیرا همه هزینه ملاقات&zwnj;تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} به موجودی حساب آنلاین‌تان بازگردانده شد.</div><br /><div style="direction: rtl;">ما موجودی حساب آنلاین‌تان را به حساب بانکی‌تان در مدت ۱ الی ۳ روز کاری واریز می&zwnj;کنیم.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.MEETING_CANCELED_SMS = {
  en: `Your meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm} has canceled.\n\nReason: "{cancelReason}"`,
  fa: `ملاقات‌تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} کنسل شد.\n\nدلیل: «{cancelReason}»`,
};

exports.PAYMENT_REFUNDED_SMS = {
  en: `The whole payment of the meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm} has refunded to you.`,
  fa: `همه هزینه ملاقات‌تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} بهت بازگشت.`,
};

exports.MEETING_CREATED_EMAIL_SUBJECT = {
  en: `Meeting Scheduled: {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}`,
  fa: `ملاقات رزرو شد: {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm}`,
};

exports.MEETING_CREATED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because a new meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm} has recently scheduled.</div><br /><ul><li>Dispute the meeting if you believe something is wrong: <a href="{disputeMeetingUrl}">{disputeMeetingUrl}</a></li><li>Join the meeting in time: <a href="{joinMeetingUrl}">{joinMeetingUrl}</a></li><li>Reschedule: <a href="{rescheduleMeetingUrl}">{rescheduleMeetingUrl}</a></li><li>Cancel: <a href="{cancelMeetingUrl}">{cancelMeetingUrl}</a></li></ul><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا یک ملاقات تازه با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} اخیراً رزرو شده&zwnj;است.</div><br /><ul style="direction: rtl;"><li>برای ملاقات اختلاف طرح کنید اگر باور دارید مشکلی است یا اتفاقی افتاده که نیازمند بررسی است: <a href="{disputeMeetingUrl}">{disputeMeetingUrl}</a></li><li>در صفحه ملاقات سر وقت حاضر شوید: <a href="{joinMeetingUrl}">{joinMeetingUrl}</a></li><li>یک زمان دیگر برای این ملاقات پیشنهاد دهید: <a href="{rescheduleMeetingUrl}">{rescheduleMeetingUrl}</a></li><li>ملاقات را لغو کنید: <a href="{cancelMeetingUrl}">{cancelMeetingUrl}</a></li></ul><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.PAYMENT_DEDUCTED_EMAIL_SUBJECT = {
  en: `Payment Deducted: A Meeting Booked`,
  fa: `کسر هزینه ملاقات: یک ملاقات رزرو کردید`,
};

exports.PAYMENT_DEDUCTED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because the cost of a recently booked meeting with {otherUserFullName} has deducted from your account balance.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا هزینه ملاقاتی که اخیراً با {otherUserFullName} رزرو کردید از موجودی حساب&zwnj;تان کسر شده&zwnj;است.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.MEETING_CREATED_SMS = {
  en: `You scheduled meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}.`,
  fa: `ملاقات شما با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} رزرو شد.`,
};

exports.PAYMENT_DEDUCTED_SMS = {
  en: `The cost of a recently booked meeting with {otherUserFullName} has deducted from your account balance.`,
  fa: `هزینه ملاقات اخیراً رزرو شده با {otherUserFullName} از موجودی حساب‌تان کسر شد.`,
};

exports.PAYMENT_INCREASED_EMAIL_SUBJECT = {
  en: `Balance Inreased: A Successful Payment`,
  fa: `موجودی افزایش یافت: تراکنش موفقیت‌آمیز بود`,
};

exports.PAYMENT_INCREASED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because you successfully increased your account balance.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا با موفقیت موجودی حساب آنلاین خود را افزایش دادید.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.PAYMENT_INCREASED_SMS = {
  en: `You successfully increased your account balance.`,
  fa: `با موفقیت موجودی حساب‌تان افزایش یافت.`,
};

exports.MEETING_RESCHEDULED_EMAIL_SUBJECT = {
  en: `Meeting Rescheduled: {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}`,
  fa: `وقت ملاقات تغییر کرد: {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm}`,
};

exports.MEETING_RESCHEDULED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because your meeting with {otherUserFullName} rescheduled to {rescheduledDateTime, date, ::yyyyMMddhhmm}.</div><br /><div>The meeting has rescheduled with a reason: "<strong>{rescheduleReason}</strong>"</div><div><ul><li>Dispute the meeting if you believe something is wrong: <a href="{disputeRescheduledMeetingUrl}">{disputeRescheduledMeetingUrl}</a></li><li>Join the meeting in time: <a href="{joinRescheduledMeetingUrl}">{joinRescheduledMeetingUrl}</a></li><li>Reschedule: <a href="{rescheduleRescheduledMeetingUrl}">{rescheduleRescheduledMeetingUrl}</a></li><li>Cancel: <a href="{cancelRescheduledMeetingUrl}">{cancelRescheduledMeetingUrl}</a></li></ul></div><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا وقت ملاقات&zwnj;تان با {otherUserFullName} به {rescheduledDateTime, date, ::yyyyMMddhhmm} تغییر کرد.</div><br /><div style="direction: rtl;">وقت ملاقات با این دلیل تغییر کرد: <strong>&laquo;{rescheduleReason}&raquo;</strong></div><div><ul style="direction: rtl;"><li>برای ملاقات اختلاف طرح کنید اگر باور دارید مشکلی است یا اتفاقی افتاده که نیازمند بررسی است: <a href="{disputeRescheduledMeetingUrl}">{disputeRescheduledMeetingUrl}</a></li><li>در صفحه ملاقات سر وقت حاضر شوید: <a href="{joinRescheduledMeetingUrl}">{joinRescheduledMeetingUrl}</a></li><li>یک زمان دیگر برای این ملاقات پیشنهاد دهید: <a href="{rescheduleRescheduledMeetingUrl}">{rescheduleRescheduledMeetingUrl}</a></li><li>ملاقات را لغو کنید: <a href="{cancelRescheduledMeetingUrl}">{cancelRescheduledMeetingUrl}</a></li></ul></div><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.MEETING_RESCHEDULED_SMS = {
  en: `Your meeting with {otherUserFullName} rescheduled to {rescheduledDateTime, date, ::yyyyMMddhhmm}.\n\nReason: "{rescheduleReason}"`,
  fa: `وقت ملاقات‌تان با {otherUserFullName} به {rescheduledDateTime, date, ::yyyyMMddhhmm} تغییر کرد.\n\nدلیل: «{rescheduleReason}»`,
};

exports.MEETING_DISPUTED_EMAIL_SUBJECT = {
  en: `Meeting Disputed: {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}`,
  fa: `برای ملاقات اختلاف مطرح شد: {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm}`,
};

exports.MEETING_DISPUTED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because your meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm} has disputed.</div><br /><div>The meeting has disputed with a reason: "<strong>{disputeReason}</strong>"</div><br /><div>We will check the recorded video of your meeting and contact you soon.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا برای ملاقات&zwnj;تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} اختلاف طرح شده&zwnj;است.</div><br /><div style="direction: rtl;">برای ملاقات با این دلیل اختلاف طرح شده‌است: <strong>&laquo;{disputeReason}&raquo;</strong></div><br /><div style="direction: rtl;">ما ویدئوی ضبط&zwnj;شده از ملاقات&zwnj;تان را بررسی می&zwnj;کنیم و به&zwnj;زودی با شما تماس می&zwnj;گیریم.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.MEETING_DISPUTED_SMS = {
  en: `Your meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm} has disputed.\n\nReason: "{disputeReason}"`,
  fa: `برای ملاقات‌تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} اختلاف طرح شد.\n\nدلیل: «{disputeReason}»`,
};

exports.MEETING_REMINDER_EMAIL_SUBJECT = {
  en: `Meeting Reminder: {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}`,
  fa: `یادآوری ملاقات: {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm}`,
};

exports.MEETING_REMINDER_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because you have a meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}.</div><br /><ul><li>Dispute the meeting if you believe something is wrong: <a href="{disputeMeetingUrl}">{disputeMeetingUrl}</a></li><li>Join the meeting in time: <a href="{joinMeetingUrl}">{joinMeetingUrl}</a></li><li>Reschedule: <a href="{rescheduleMeetingUrl}">{rescheduleMeetingUrl}</a></li><li>Cancel: <a href="{cancelMeetingUrl}">{cancelMeetingUrl}</a></li></ul><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا یک ملاقات با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} دارید.</div><br /><ul style="direction: rtl;"><li>برای ملاقات اختلاف طرح کنید اگر باور دارید مشکلی است یا اتفاقی افتاده که نیازمند بررسی است: <a href="{disputeMeetingUrl}">{disputeMeetingUrl}</a></li><li>در صفحه ملاقات سر وقت حاضر شوید: <a href="{joinMeetingUrl}">{joinMeetingUrl}</a></li><li>یک زمان دیگر برای این ملاقات پیشنهاد دهید: <a href="{rescheduleMeetingUrl}">{rescheduleMeetingUrl}</a></li><li>ملاقات را لغو کنید: <a href="{cancelMeetingUrl}">{cancelMeetingUrl}</a></li></ul><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.MEETING_REMINDER_SMS = {
  en: `You have a meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}.`,
  fa: `یک ملاقات با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddhhmm} دارید.`,
};

exports.PAYMENT_RECEIVED_EMAIL_SUBJECT = {
  en: `Payment Received: {paymentAmount, number, ,?} {paymentAmount, plural, =0 {Toman} many {Tomans} other {Tomans}}`,
  fa: `موجودی حساب‌تان افزایش یافت: {paymentAmount, number, ,?} {paymentAmount, plural, =0 {تومان} many {تومان} other {تومان}}`,
};

exports.PAYMENT_RECEIVED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because you received {paymentAmount, number, ,?} {paymentAmount, plural, =0 {Toman} many {Tomans} other {Tomans}} from the meeting with {otherUserFullName} at {meetingDateTime, date, ::yyyyMMddhhmm}.</div><br /><div>Thank you for choosing Balcon. We hope you had a great time at the meeting.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا {paymentAmount, number, ,?} {paymentAmount, plural, =0 {تومان} many {تومان} other {تومان}} بابت ملاقات&zwnj;تان با {otherUserFullName} در {meetingDateTime, date, ::yyyyMMddHHmm} به موجودی حساب&zwnj;تان اضافه شد.</div><br /><div style="direction: rtl;">ممنونیم از اینکه بالکن را انتخاب کردید. امیدواریم اوقات خوشی را در ملاقات داشتید.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.PAYMENT_RECEIVED_SMS = {
  en: `You received {paymentAmount, number, ,?} {paymentAmount, plural, =0 {Toman} many {Tomans} other {Tomans}} in your account.`,
  fa: `{paymentAmount, number, ,?} {paymentAmount, plural, =0 {تومان} many {تومان} other {تومان}} به موجودی حساب‌تان اضافه شد.`,
};

exports.PASSWORD_HAS_CHANGED_EMAIL_SUBJECT = {
  en: `Your Balcon account's password has changed`,
  fa: `پسورد اکانت بالکن شما تغییر کرد`,
};

exports.PASSWORD_HAS_CHANGED_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because your Balcon account's password has changed successfully.</div><br /><div>If this is not you, please contact us. We help you recover your account.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا پسورد اکانت بالکن شما باموفقیت تغییر کرد.</div><br /><div style="direction: rtl;">اگر شما این تغییر را ایجاد نکردید، لطفاً با ما تماس بگیرید. ما کمک&zwnj;تان می&zwnj;کنیم اکانت خود را بازیابی کنید.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.RESET_PASSWORD_EMAIL_SUBJECT = {
  en: `Reset your Balcon account's password`,
  fa: `پسورد اکانت بالکن خود را بازیابی کنید`,
};

exports.RESET_PASSWORD_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div>You receive this notification because you (or someone else) have requested the reset of the password for your account.</div><br /><div>Please click on the following link, or paste this into your browser to complete the process: <a href="{resetPasswordLink}" target="_blank">{resetPasswordLink}</a></div><br /><div>If you did not request this, please ignore this notification and your password will remain unchanged.</div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">شما این اطلاعیه را دریافت می&zwnj;کنید زیرا شما (یا شخص دیگر) تغییر پسورد اکانت بالکن شما را درخواست کرده&zwnj;است.</div><br /><div style="direction: rtl;">لطفاً روی لینک زیر کلیک کنید، یا آن را در مرورگر بنویسید تا فرایند ادامه یابد: <a href="{resetPasswordLink}" target="_blank">{resetPasswordLink}</a></div><br /><div style="direction: rtl;">اگر شما این را درخواست نکردید، لطفاً این اطلاعیه را نادیده بگیرید تا پسورد اکانت بالکن شما بدون تغییر باقی بماند.</div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};

exports.VERIFY_EMAIL_EMAIL_SUBJECT = {
  en: `Verify your email address on Balcon`,
  fa: `آدرس ایمیل‌تان را در بالکن تأیید کنید`,
};

exports.VERIFY_EMAIL_EMAIL_TEXT = {
  en: `<div>Hi {userFirstName},</div><br /><div><div><div>Thank you for registering with Balcon.</div></div></div><br /><div>Please click on the following link, or paste this into your browser to verify your email address: <a href="{verifyEmailLink}" target="_blank">{verifyEmailLink}</a></div><br /><br /><div>Sincerely,</div><div>Balcon</div>`,
  fa: `<div style="direction: rtl;">سلام {userFirstName}،</div><br /><div style="direction: rtl;">سپاسگزار و خوشحالیم در بالکن ثبت&zwnj;نام کردید.</div><br /><div style="direction: rtl;">لطفاً روی لینک زیر کلیک کنید، یا آن را در مرورگر بنویسید تا ایمیل&zwnj;تان را در بالکن تأیید کنید: <a href="{verifyEmailLink}" target="_blank">{verifyEmailLink}</a></div><br /><br /><div style="direction: rtl;">با احترام،</div><div style="direction: rtl;">بالکن</div>`,
};
