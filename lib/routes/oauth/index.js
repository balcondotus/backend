const passport = require('passport');
const express = require('express');
const ROUTE = require('lib/consts/route');
const { AuthMiddleware } = require('lib/middlewares');
const ResponseUtils = require('bin/utils/response');

const router = express.Router();

router.get(ROUTE.OAUTH.SUB.GOOGLE, AuthMiddleware.optional, (req, res, next) => {
  passport.authenticate('google-token', async (err, user, info) => {
    if (err) return next(err);

    res.status(info.isNew ? 201 : 200).send(ResponseUtils.respondSuccess(user));
  })(req, res, next);
});

router.get(ROUTE.OAUTH.SUB.FACEBOOK, AuthMiddleware.optional, (req, res, next) => {
  passport.authenticate('facebook-token', async (err, user, info) => {
    if (err) return next(err);

    res.status(info.isNew ? 201 : 200).send(ResponseUtils.respondSuccess(user));
  })(req, res, next);
});

module.exports = router;
