const express = require('express');
const ROUTE = require('lib/consts/route');
const { AuthMiddleware } = require('lib/middlewares');
const { PasswordController } = require('lib/controllers');

const router = express.Router();

router.post(ROUTE.ACCOUNT.SUB.PASSWORD.SUB.FORGOT, AuthMiddleware.guest, async (req, res) => {
  const { email } = req.body;

  const result = await PasswordController.forgotPassword(email);
  res.send(result);
});

router.post(ROUTE.ACCOUNT.SUB.PASSWORD.SUB.RESET, AuthMiddleware.optional, async (req, res) => {
  const { password, token } = req.body;

  const result = await PasswordController.resetPassword(password, token);
  res.send(result);
});

module.exports = router;
