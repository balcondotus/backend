const express = require('express');
const ROUTE = require('lib/consts/route');

const router = express.Router();

router.use(ROUTE.ACCOUNT.SUB.UPLOAD.SUB.AVATAR, require('./avatar-routes'));

module.exports = router;
