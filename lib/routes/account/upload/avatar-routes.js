const express = require('express');
const bodyParser = require('body-parser');
const { AuthMiddleware, UploadMiddleware } = require('lib/middlewares');
const AppConfig = require('config/app');
const { ProfileController } = require('lib/controllers');

const router = express.Router();

// Load given remote file.
router.get('/', AuthMiddleware.required, async (req, res, next) => {
  const { fetch: remoteFilePath } = req.query;
  const loadResponse = await ProfileController.loadRemoteFile(req.user._id, remoteFilePath);

  if (loadResponse.error) return res.status(loadResponse.error.status).send(loadResponse);

  res.writeHead(200, {
    'Access-Control-Expose-Headers': 'Content-Disposition, Content-Length, X-Content-Transfer-Id',
    'Content-Type': loadResponse.data.mime,
    'Content-Length': loadResponse.data.length,
    'Content-Disposition': `inline; filename="${loadResponse.data.transferId}"`,
    'X-Content-Transfer-Id': loadResponse.data.transferId,
  });

  loadResponse.data.readStream.pipe(res);
});

// Request a temp file.
router.post('/', AuthMiddleware.required, (req, res) => {
  const uploadLength = req.get('upload-length');

  const requestTempFileResult = ProfileController.requestTempFile(uploadLength);

  if (requestTempFileResult.error)
    return res.status(requestTempFileResult.error.status).send(requestTempFileResult);

  res.send(requestTempFileResult.data.name);
});

// Write file chunks in the previously created temp file.
router.patch(
  '/',
  AuthMiddleware.required,
  bodyParser.raw({
    type: 'application/offset+octet-stream',
    limit: AppConfig.maxRequestSize,
  }),
  UploadMiddleware.writeChunk({
    allowedMimeTypes: AppConfig.allowedAvatarMimeTypes,
  }),
  async (req, res, next) => {
    // FilePond expects an empty response.
    res.send();
  },
);

// Continue uploading.
router.head('/', AuthMiddleware.required, UploadMiddleware.nextRequestedOffset, (req, res) => {
  const { uploaded } = req;

  // FIXME: FilePond doesn't understand the offset passed in the header
  // https://github.com/pqina/filepond/issues/491
  // FilePond expects the next requested offset in this header.
  res.writeHead(200, {
    'Access-Control-Expose-Headers': 'Upload-Offset',
    'Upload-Offset': uploaded.offset,
  });

  res.send();
});

// Delete the avatar from local and database.
router.delete('/', AuthMiddleware.required, bodyParser.text(), async (req, res) => {
  const { user, body: transferId } = req;

  const result = await ProfileController.deleteAvatar(user._id, transferId);

  if (result.error) return res.status(result.error.status).send(result);

  // FilePond expects an empty response.
  return res.send();
});

module.exports = router;
