const express = require('express');
const ROUTE = require('lib/consts/route');
const { AuthMiddleware } = require('lib/middlewares');
const { EmailController } = require('lib/controllers');

const router = express.Router();

router.post(ROUTE.ACCOUNT.SUB.EMAIL.SUB.VERIFY, AuthMiddleware.optional, async (req, res) => {
  const { token } = req.body;

  const result = await EmailController.verifyEmail(token);
  res.send(result);
});

module.exports = router;
