const express = require('express');
const ROUTE = require('lib/consts/route');
const { AccountController, PaymentController } = require('lib/controllers');
const { AuthMiddleware } = require('lib/middlewares');

const router = express.Router();

router.post(ROUTE.ACCOUNT.SUB.SIGNIN, AuthMiddleware.guest, async (req, res, next) => {
  const { email, password } = req.body;

  try {
    const result = await AccountController.signin(email, password);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.post(ROUTE.ACCOUNT.SUB.SIGNUP, AuthMiddleware.guest, async (req, res, next) => {
  const { email, password, firstName, lastName } = req.body;

  try {
    const result = await AccountController.signup(email, password, firstName, lastName);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.get(ROUTE.ACCOUNT.SUB.BALANCE, AuthMiddleware.required, async (req, res, next) => {
  try {
    const result = await PaymentController.getBalance(req.user._id);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.use(ROUTE.ACCOUNT.SUB.PASSWORD.PATH, require('./password'));
router.use(ROUTE.ACCOUNT.SUB.EMAIL.PATH, require('./email-routes'));
router.use(ROUTE.ACCOUNT.SUB.UPLOAD.PATH, require('./upload'));

module.exports = router;
