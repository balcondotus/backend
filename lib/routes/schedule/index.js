const express = require('express');
const ROUTE = require('lib/consts/route');
const { ScheduleController } = require('lib/controllers');
const { AuthMiddleware } = require('lib/middlewares');

const router = express.Router();

router.get(ROUTE.SCHEDULE.SUB.PROFILE, async (req, res, next) => {
  const { userIdentifier } = req.query;

  try {
    const result = await ScheduleController.getHostProfile(userIdentifier);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.get(ROUTE.SCHEDULE.SUB.SPOTS, async (req, res, next) => {
  const { startRange, endRange, timeZone, userId } = req.query;

  try {
    const result = await ScheduleController.getSpots(userId, startRange, endRange, timeZone);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.get(ROUTE.SCHEDULE.SUB.VALIDATE, AuthMiddleware.required, async (req, res, next) => {
  const { spot, slot, timeZone, userIdentifier } = req.query;

  try {
    const result = await ScheduleController.validateMeetingTime(
      userIdentifier,
      spot,
      slot,
      timeZone,
    );

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.post(ROUTE.SCHEDULE.SUB.BOOK, AuthMiddleware.required, async (req, res, next) => {
  const { spot, slot, timeZone, userIdentifier, answers } = req.body;

  try {
    const result = await ScheduleController.book(
      req.user._id,
      userIdentifier,
      spot,
      slot,
      timeZone,
      answers,
    );

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.post(ROUTE.SCHEDULE.SUB.RESCHEDULE, AuthMiddleware.required, async (req, res, next) => {
  const { spot, slot, timeZone, rescheduleId, reason } = req.body;

  try {
    const result = await ScheduleController.reschedule(
      req.user,
      rescheduleId,
      spot,
      slot,
      timeZone,
      reason,
    );

    res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
