const express = require('express');
const ROUTE = require('lib/consts/route');
const { PaymentController } = require('lib/controllers');
const { AuthMiddleware } = require('lib/middlewares');

const router = express.Router();

router.post(ROUTE.PAYMENT.SUB.PAY, AuthMiddleware.required, async (req, res, next) => {
  const { amount } = req.body;

  try {
    const result = await PaymentController.pay(req.user._id, amount);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.post(ROUTE.PAYMENT.SUB.VERIFY, AuthMiddleware.required, async (req, res, next) => {
  const { payToken } = req.body;

  try {
    const result = await PaymentController.verify(req.user._id, payToken);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
