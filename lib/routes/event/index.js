const express = require('express');
const ROUTE = require('lib/consts/route');
const { EventController } = require('lib/controllers');
const { AuthMiddleware } = require('lib/middlewares');

const router = express.Router();

router.get(ROUTE.EVENT.SUB.QUESTIONS, AuthMiddleware.required, async (req, res, next) => {
  const { userIdentifier } = req.query;

  try {
    const result = await EventController.getDefaultEventQuestions(userIdentifier);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
