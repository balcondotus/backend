const express = require('express');
const ROUTE = require('lib/consts/route');
const { MeetingController } = require('lib/controllers');
const { AuthMiddleware } = require('lib/middlewares');

const router = express.Router();

router.post(ROUTE.MEETING.SUB.CANCEL, AuthMiddleware.required, async (req, res, next) => {
  const { meetingId, reason } = req.body;

  try {
    const result = await MeetingController.cancel(req.user._id, meetingId, reason);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.post(ROUTE.MEETING.SUB.DISPUTE, AuthMiddleware.required, async (req, res, next) => {
  const { meetingId, reason } = req.body;

  try {
    const result = await MeetingController.dispute(req.user, meetingId, reason);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.get(ROUTE.MEETING.SUB.DETAILS, AuthMiddleware.required, async (req, res, next) => {
  const { meetingId } = req.query;

  try {
    const result = await MeetingController.getMeeting(req.user, meetingId);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

router.get(ROUTE.MEETING.SUB.JOIN, AuthMiddleware.required, async (req, res, next) => {
  const { meetingId } = req.query;

  try {
    const result = await MeetingController.join(req.user, meetingId);

    res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
