const { Schema, model, SchemaTypes } = require('mongoose');
const moment = require('moment');
const _ = require('lodash');
const { NotificationContentModel } = require('./embedded');

const schema = new Schema({
  user: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
  type: { type: SchemaTypes.String, maxlength: 255, required: true, trim: true },
  content: { type: NotificationContentModel, required: true },
  createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
  readAt: { type: SchemaTypes.Date, default: null },
});

/**
 * Marks all unread notifications as read if any for the provided user ID.
 *
 * @param {ObjectId} userId
 *
 * @returns {Object} Plain JS object
 */
schema.statics.markAllAsRead = async function readAll(userId) {
  return this.updateMany({ readAt: null, user: userId }, { readAt: moment.utc().toDate() })
    .lean()
    .exec();
};

/**
 * Finds all unread notifications along with today received ones.
 *
 * @param {String} user
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getUnreadNotifications = async function getUnreadNotifications(userId) {
  return this.find(
    {
      user: userId,
      $or: [
        { readAt: null },
        {
          createdAt: {
            $lte: moment.utc().endOf('day').toDate(),
            $gte: moment.utc().startOf('day').toDate(),
          },
        },
      ],
    },
    { __v: false },
  )
    .populate('content.refId')
    .sort('-createdAt')
    .lean({ virtuals: true })
    .exec();
};

/**
 * Gets Notification and prepares for Client.
 *
 * @param {any} notificationId
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getOneByNotificationId = async function getOneByNotificationId(notificationId) {
  return this.findById(notificationId, { __v: false })
    .populate('content.refId')
    .lean({ virtuals: true })
    .exec();
};

schema.post(/find(\w+)?/i, async function find(result) {
  // Populate notifications properly.

  if (!_.isArray(result)) result = [result];

  return Promise.all(
    result.map(async (notification) => {
      switch (notification.content.morph) {
        case 'Dispute':
          notification.content = await model(notification.content.morph)
            .findById(notification.content.refId._id)
            .populate('user', { profile: true })
            .populate('meeting')
            .lean({ virtuals: true })
            .exec();
          break;
        case 'Meeting':
          notification.content = await model(notification.content.morph)
            .findById(notification.content.refId._id)
            .populate('created.user', { profile: true })
            .populate('rescheduled.user', { profile: true })
            .populate('canceled.user', { profile: true })
            .populate('rescheduled.rescheduledTo')
            .populate('disputed')
            .populate('disputed.user', { profile: true })
            .populate('event')
            .populate('host', { profile: true })
            .populate('invitee', { profile: true })
            .lean({ virtuals: true })
            .exec();
          break;
        case 'Payment':
          notification.content = await model(notification.content.morph)
            .findById(notification.content.refId._id)
            .populate('user', { profile: true })
            .lean({ virtuals: true })
            .exec();
          break;
        default:
          notification.content = await model(notification.content.morph)
            .lean({ virtuals: true })
            .exec();
      }

      return notification;
    }),
  );
});

module.exports = model('Notification', schema);
