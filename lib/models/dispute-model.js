const { Schema, model, SchemaTypes } = require('mongoose');

const schema = new Schema({
  meeting: { type: SchemaTypes.ObjectId, ref: 'Meeting', required: true },
  status: {
    type: SchemaTypes.String,
    enum: ['PENDING', 'APPROVED', 'REJECTED'],
    default: 'PENDING',
    required: true,
    trim: true,
    uppercase: true,
  },
  user: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
  reason: { type: SchemaTypes.String, maxlength: 255, required: true, trim: true },
  createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
});

/**
 * Inserts a new dispute.
 *
 * @param {any} meetingId
 * @param {String} status
 * @param {String} reason
 */
schema.statics.createAndInsertOne = async function createAndInsertOne(
  userId,
  meetingId,
  status,
  reason,
  session,
) {
  let dispute = {
    meeting: meetingId,
    user: userId,
    status,
    reason,
  };

  [dispute] = await this.create([dispute], { session });

  return dispute;
};

/**
 * Checks if given meeting has disputed.
 *
 * @param {any} meetingId
 *
 * @returns {Boolean} true if given meeting has disputed.
 */
schema.statics.isMeetingDisputed = async function isMeetingDisputed(meetingId) {
  const count = await this.countDocuments({ meeting: meetingId }).exec();
  return count !== 0;
};

/**
 * Finds all documents related to the given meeting.
 *
 * @param {any} meetingId
 */
schema.statics.findAllByMeetingId = async function findAllByMeetingId(meetingId) {
  return this.find({ meeting: meetingId }).exec();
};

/**
 * Finds the first created dispute for the given meeting.
 *
 * @param {any} meetingId
 */
schema.statics.findOneByMeetingId = async function findByMeetingId(meetingId) {
  return this.find({ meeting: meetingId, status: 'PENDING' }).exec();
};

module.exports = model('Dispute', schema);
