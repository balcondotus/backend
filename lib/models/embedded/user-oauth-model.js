const { Schema, SchemaTypes } = require('mongoose');
const AppConfig = require('config/app');

const schema = new Schema(
  {
    accountId: { type: SchemaTypes.String, maxlength: 255, trim: true },
    provider: {
      type: SchemaTypes.String,
      maxlength: 15,
      enum: AppConfig.supportedOauthProviders,
      lowercase: true,
      trim: true,
    },
    accessToken: { type: SchemaTypes.String, select: false, trim: true },
    accessTokenExpires: { type: SchemaTypes.Date, select: false },
    refreshToken: { type: SchemaTypes.String, default: null, select: false, trim: true },
  },
  { id: false, _id: false },
);

module.exports = schema;
