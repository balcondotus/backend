const { Schema, SchemaTypes } = require('mongoose');

const schema = new Schema(
  {
    refId: {
      type: SchemaTypes.ObjectId,
      required: true,
      refPath: 'content.morph', // `content` is the parent field in `NotificationModel`
    },
    morph: {
      type: SchemaTypes.String,
      required: true,
      trim: true,
      enum: ['Meeting', 'Event', 'Payment', 'Dispute'],
    },
  },
  { id: false, _id: false },
);

module.exports = schema;
