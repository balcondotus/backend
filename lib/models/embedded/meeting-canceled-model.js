const { Schema, SchemaTypes } = require('mongoose');

const schema = new Schema(
  {
    user: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
    canceledAt: { type: SchemaTypes.Date, default: Date.now, required: true },
    reason: { type: SchemaTypes.String, maxlength: 255, trim: true, required: true },
  },
  { id: false, _id: false },
);

module.exports = schema;
