const { Schema, SchemaTypes } = require('mongoose');
const AppConfig = require('config/app');

const schema = new Schema(
  {
    meeting: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.meeting,
      default: ['BALCON', 'EMAIL', 'SMS'],
      trim: true,
    },
    reminder: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.reminder,
      default: ['BALCON', 'EMAIL', 'SMS'],
      trim: true,
    },
    payment: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.payment,
      default: ['BALCON', 'EMAIL', 'SMS'],
      trim: true,
    },
    product: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.product,
      default: ['BALCON', 'EMAIL'],
      trim: true,
    },
    newsletter: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.newsletter,
      default: ['EMAIL'],
      trim: true,
    },
    survey: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.survey,
      default: ['EMAIL'],
      trim: true,
    },
    offer: {
      type: [SchemaTypes.String],
      enum: AppConfig.notification.offer,
      default: ['BALCON', 'EMAIL'],
      trim: true,
    },
    account: {
      // Security, Verifications, Account disabled, so on.
      type: [SchemaTypes.String],
      enum: AppConfig.notification.account,
      default: ['BALCON', 'EMAIL'],
      trim: true,
    },
  },
  { _id: false, id: false },
);

module.exports = schema;
