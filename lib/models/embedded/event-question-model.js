const { Schema, SchemaTypes } = require('mongoose');
const _ = require('lodash');
const { BadRequest } = require('http-errors');

const schema = new Schema({
  title: { type: SchemaTypes.String, maxlength: 255 },
  answerType: {
    type: SchemaTypes.String,
    enum: ['ONELINE', 'MULTIPLELINES', 'RADIOBUTTONS', 'CHECKBOXES'],
    default: 'ONELINE',
    trim: true,
  },
  isRequired: { type: SchemaTypes.Boolean, default: true },
  options: [{ title: { type: SchemaTypes.String, maxlength: 255, trim: true } }], // This field is only used with radio and check boxes.
  createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
  deletedAt: { type: SchemaTypes.Date, default: null, select: false },
});

schema.pre('save', function save(next) {
  if (this.isModified('answerType')) {
    return next(
      ['CHECKBOXES', 'RADIOBUTTONS'].includes(this.answerType) && _.isEmpty(this.options)
        ? new BadRequest('Options cannot be empty while using radio and check boxes.')
        : null,
    );
  }

  return next();
});

schema.pre('save', function save(next) {
  if (this.isModified('answerType')) {
    return next(
      ['ONELINE', 'MULTIPLELINES'].includes(this.answerType) && !_.isEmpty(this.options)
        ? new BadRequest('Options cannot be used with single and multi line questions.')
        : null,
    );
  }

  return next();
});

module.exports = schema;
