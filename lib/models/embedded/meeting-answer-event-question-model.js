const { Schema, SchemaTypes } = require('mongoose');

const schema = new Schema(
  {
    question: { type: SchemaTypes.ObjectId, ref: 'Event.questions' },
    answer: { type: SchemaTypes.String, maxlength: 65535, trim: true },
  },
  { _id: false, id: false },
);

module.exports = schema;
