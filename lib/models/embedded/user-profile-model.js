const { Schema, SchemaTypes } = require('mongoose');
const _ = require('lodash');
const AppConfig = require('config/app');

const schema = new Schema(
  {
    firstName: {
      type: SchemaTypes.String,
      trim: true,
      maxlength: 255,
      required: true,
    },
    lastName: {
      type: SchemaTypes.String,
      trim: true,
      maxlength: 255,
      required: true,
    },
    gender: {
      type: SchemaTypes.String,
      enum: AppConfig.allowedGenders,
      uppercase: true,
      trim: true,
    },
    avatar: String,
  },
  { _id: false, id: false, toJSON: { virtuals: true } },
);

/**
 * Set undefined if empty
 */
schema.pre('validate', function validate(next) {
  if (this.isModified('gender') && _.isEmpty(this.gender)) this.gender = undefined;

  next();
});

schema.virtual('fullName').get(function virtual() {
  return `${this.firstName} ${this.lastName}`;
});

module.exports = schema;
