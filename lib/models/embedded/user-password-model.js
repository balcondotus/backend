const { Schema, SchemaTypes } = require('mongoose');
const bcrypt = require('bcrypt');

const schema = new Schema(
  {
    password: { type: SchemaTypes.String, trim: true },
    resetToken: {
      type: SchemaTypes.String,
      minlength: 32,
      maxlength: 32,
      select: false,
      trim: true,
    },
    tokenExpiresAt: { type: SchemaTypes.Date, min: Date.now, select: false },
  },
  { _id: false, id: false, toJSON: { virtuals: true } },
);

schema.pre('save', function save(next) {
  if (this.isModified('password') && this.password !== undefined) {
    this.resetToken = undefined;
    this.tokenExpiresAt = undefined;
  }

  return next();
});

schema.virtual('hasPassword').get(function virtual() {
  return this.password !== undefined;
});

/**
 * Password hash middleware.
 */
schema.pre('save', function save(next) {
  if (!this.isModified('password')) {
    return next();
  }
  if (this.isModified('password') && this.password === undefined) {
    return next();
  }

  bcrypt.genSalt(10, (saltError, salt) => {
    if (saltError) {
      return next(saltError);
    }
    bcrypt.hash(this.password, salt, (hashError, hash) => {
      if (hashError) {
        return next(hashError);
      }
      this.password = hash;
      next();
    });
  });
});

/**
 * Compares user's password.
 *
 * @param {String} candidatePassword
 */
schema.methods.comparePassword = async function comparePassword(candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password);
};

module.exports = schema;
