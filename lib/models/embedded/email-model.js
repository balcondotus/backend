const { Schema, SchemaTypes } = require('mongoose');
const { normalizeEmail, isEmail } = require('validator').default;

const schema = new Schema(
  {
    email: {
      type: SchemaTypes.String,
      unique: true,
      trim: true,
      maxlength: 320,
      required: true,
      set: normalizeEmail,
      validate: {
        validator(v) {
          return isEmail(v);
        },
        message: (props) => `Invalid Email Address`,
      },
    },
    verificationToken: {
      type: SchemaTypes.String,
      minlength: 32,
      maxlength: 32,
      select: false,
      trim: true,
    },
    tokenExpiresAt: { type: SchemaTypes.Date, min: Date.now, select: false },
    verified: { type: SchemaTypes.Boolean, default: false },
  },
  { _id: false, id: false },
);

schema.pre('save', function save(next) {
  if (this.isModified('verified') && this.verified) {
    this.verificationToken = undefined;
    this.tokenExpiresAt = undefined;
  }

  return next();
});

module.exports = schema;
