const { Schema } = require('mongoose');
const EventAvailabilityWeekdayModel = require('./event-availability-weekday-model');

/**
 * Here is a sample:
 * {
 *  "sun":{
 *    "specifics":{
 *      "YYYY-MM-DD":[
 *        {
 *          "start":"HH:mm",
 *          "end":"HH:mm"
 *        }
 *      ]
 *    },
 *    "times":[
 *      {
 *        "start":"HH:mm",
 *        "end":"HH:mm"
 *      }
 *    ]
 *  }
 * }
 */
const schema = new Schema(
  {
    sun: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
    mon: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
    tue: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
    wed: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
    thu: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
    fri: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
    sat: {
      type: EventAvailabilityWeekdayModel,
      default: {},
    },
  },
  { id: false, _id: false, minimize: false },
);

module.exports = schema;
