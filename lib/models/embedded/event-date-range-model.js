const { Schema, SchemaTypes } = require('mongoose');
const _ = require('lodash');
const moment = require('moment');

const schema = new Schema(
  {
    type: {
      type: SchemaTypes.String,
      enum: ['ROLLING', 'RANGE', 'INDEFINITELY'],
      default: 'ROLLING',
      uppercase: true,
      trim: true,
      required: true,
    },
    // Rolling type expects a number value in days.
    // Range type expects an object value with two `start` and `end` field.
    // Indefinitely type expects a null value.
    value: {
      type: SchemaTypes.Mixed,
      default: 60,
      validate: [
        {
          validator(v) {
            if (this.type === 'ROLLING') {
              return _.isNumber(v) && v > 0 && v < 365;
            }

            return true;
          },
          message: () =>
            'Rolling type expects a value of type number greater than 0 and smaller than 365',
        },
        {
          validator(v) {
            if (this.type === 'RANGE') {
              return (
                _.isObject(v) &&
                !_.isUndefined(v.start) &&
                !_.isUndefined(v.end) &&
                moment.utc(v.start).isValid() &&
                moment.utc(v.end).isValid()
              );
            }

            return true;
          },
          message: () => 'Range type expects a value of type object with `start` and `end` fields',
        },
        {
          validator(v) {
            if (this.type === 'INDEFINITELY') {
              return _.isNull(v);
            }

            return true;
          },
          message: () => 'Indefinitely type expects a value of type null',
        },
      ],
    },
  },
  { id: false, _id: false },
);

/**
 * Changes range type values to dates before save.
 */
schema.pre('save', function save(next) {
  if (this.type === 'RANGE') {
    this.value.start = moment.utc(this.value.start).toDate();
    this.value.end = moment.utc(this.value.end).toDate();
  }

  next();
});

module.exports = schema;
