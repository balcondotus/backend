const { Schema, SchemaTypes } = require('mongoose');
const _ = require('lodash');

const validate = {
  validator(v) {
    return _.isNumber(v);
  },
  message: 'Prices must be numeric, no symbols allowed.',
};

// Prices are in Tomans.

const schema = new Schema(
  {
    rate: {
      type: SchemaTypes.Number,
      min: 0,
      required: true,
      validate,
    },
    transaction: {
      type: SchemaTypes.Number,
      required: true,
      validate,
    },
    platform: {
      type: SchemaTypes.Number,
      required: true,
      validate,
    },
    services: {
      type: SchemaTypes.Number,
      required: true,
      validate,
    },
  },
  { _id: false, id: false, toJSON: { virtuals: true } },
);

schema.virtual('sum').get(function virtual() {
  return this.rate + this.transaction + this.platform + this.services;
});

module.exports = schema;
