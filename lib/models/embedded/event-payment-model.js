const { Schema, SchemaTypes } = require('mongoose');
const _ = require('lodash');

const schema = new Schema(
  {
    rate: {
      type: SchemaTypes.Number,
      min: 0,
      max: 9999999,
      required: true,
      default: 75000, // in Tomans
      validate: {
        validator(v) {
          return _.isNumber(v);
        },
        message: () => 'Rate is not a number',
      },
    },
  },
  { id: false, _id: false },
);

module.exports = schema;
