const { Schema, SchemaTypes } = require('mongoose');

const schema = new Schema(
  {
    confirmedAt: { type: SchemaTypes.Date, default: Date.now, required: true },
  },
  { id: false, _id: false },
);

module.exports = schema;
