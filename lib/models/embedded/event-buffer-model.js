const { Schema, SchemaTypes } = require('mongoose');

const schema = new Schema(
  {
    before: { type: SchemaTypes.Number, min: 0, max: 180, require: true, default: 0 }, // in minutes
    after: { type: SchemaTypes.Number, min: 0, max: 180, require: true, default: 0 }, // in minutes
  },
  { _id: false, id: false },
);

module.exports = schema;
