const { Schema, SchemaTypes } = require('mongoose');
const IntervalUtils = require('bin/utils/interval');
const APP = require('lib/consts/app');

const validateIntervalTimeAgainstPattern = {
  validator(v) {
    return APP.INTERVAL_TIME_PATTERN.test(v);
  },
  message: ({ value }) =>
    `Time ${value} doesn't follow pattern ${APP.INTERVAL_TIME_PATTERN.source}`,
};

const schema = new Schema(
  {
    specifics: { type: SchemaTypes.Mixed, default: {}, validate: (v) => typeof v === 'object' },
    times: {
      type: [
        {
          start: {
            type: SchemaTypes.String,
            validate: [validateIntervalTimeAgainstPattern],
            trim: true,
          },
          end: {
            type: SchemaTypes.String,
            validate: [validateIntervalTimeAgainstPattern],
            trim: true,
          },
        },
      ],
      default: [],
    },
  },
  { id: false, _id: false, minimize: false },
);

/**
 * Sorts specifics ordered by asc.
 */
schema.pre('save', function save(next) {
  if (this.isModified('specifics')) {
    Object.entries(this.specifics).forEach(([date, intervals]) => {
      this.specifics[date] = IntervalUtils.sortIntervals(intervals);
    });
  }

  next();
});

/**
 * Sorts times ordered by asc.
 */
schema.pre('save', function save(next) {
  if (this.isModified('times')) {
    this.times = IntervalUtils.sortIntervals(this.times);
  }

  next();
});

module.exports = schema;
