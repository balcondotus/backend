const { Schema, SchemaTypes } = require('mongoose');
const { isValidNumber, parsePhoneNumber } = require('libphonenumber-js');

const schema = new Schema(
  {
    phone: {
      type: SchemaTypes.String,
      trim: true,
      unique: true,
      maxlength: 13,
      sparse: true,
      set: (v) => {
        this._previousPhone = this.phone;
        return parsePhoneNumber(v).number;
      },
      validate: {
        validator(v) {
          return isValidNumber(v);
        },
        message: (props) => `${props.value} isn't an international phone number`,
      },
    },
    verified: { type: SchemaTypes.Boolean, default: false },
  },
  { _id: false, id: false },
);

schema.pre('save', function save(next) {
  if (this.isModified('phone') && this.phone && this.phone !== this._previousPhone) {
    this.verified = false;
  }

  return next();
});

module.exports = schema;
