const { Schema, SchemaTypes } = require('mongoose');
const moment = require('moment');

const schema = new Schema(
  {
    start: {
      type: SchemaTypes.Date,
      required: true,
      min: Date.now,
      validate: [
        {
          validator(v) {
            return moment(v).isBefore(this.end);
          },
          message: () => 'Start date must be before the end date.',
        },
      ],
    },
    end: {
      type: SchemaTypes.Date,
      required: true,
      min: Date.now,
      validate: [
        {
          validator(v) {
            return moment(v).isAfter(this.start);
          },
          message: () => 'End date must be after the start date.',
        },
      ],
    },
  },
  { id: false, _id: false },
);

module.exports = schema;
