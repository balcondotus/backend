const { Schema, SchemaTypes } = require('mongoose');

const schema = new Schema(
  {
    user: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
    createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
  },
  { id: false, _id: false },
);

module.exports = schema;
