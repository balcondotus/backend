const { Schema, model, SchemaTypes, isValidObjectId } = require('mongoose');

const schema = new Schema({
  user: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
  amount: { type: SchemaTypes.Number, required: true },
  balance: { type: SchemaTypes.Number, required: true },
  description: { type: SchemaTypes.String, maxlength: 65535, default: null, trim: true },
  status: {
    type: SchemaTypes.String,
    enum: ['FAILED', 'SUCCEEDED', 'UNKNOWN'],
    required: true,
    trim: true,
    default: 'UNKNOWN',
  },
  transactionCode: { type: SchemaTypes.String, maxlength: 255, default: null, trim: true },
  createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
});

schema.statics.findByPaymentId = async function findByPaymentId(paymentId) {
  return this.findById(paymentId).populate('user').exec();
};

/**
 * Gets the current user balance.
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getUserBalance = async function getUserBalance(userId) {
  if (!isValidObjectId(userId)) throw new Error('Invalid User ID');

  const balance = await this.findOne({ user: userId, status: 'SUCCEEDED' }, { balance: true })
    .sort('-createdAt')
    .lean()
    .exec();

  // If not found, it means the user has no payments yet, so her balance is 0
  return balance || { balance: 0 };
};

/**
 * Creates a new payment.
 *
 * @param {ObjectId} user
 * @param {Number} amount
 * @param {String} status
 * @param {String} description
 * @param {ClientSession} session
 */
schema.statics.newPayment = async function newPayment(user, amount, status, description, session) {
  const userBalance = await this.getUserBalance(user);
  const payment = new this({
    user,
    amount,
    status,
    description,
    balance: userBalance.balance + amount,
  });

  const [savedPayments] = await this.create([payment], { session });

  return savedPayments;
};

/**
 * Gets User's payments list.
 *
 * @param {ObjectId} user
 */
schema.statics.getPaymentsForClient = async function getPaymentsForClient(user) {
  return this.find({ user }, { user: false }).lean().exec();
};

schema.statics.findByTransactionCode = async function findByTransactionCode(transCode) {
  return this.findOne({ transactionCode: transCode }).exec();
};

schema.statics.increaseUserBalance = async function increaseUserBalance(
  user,
  amount,
  description,
  transactionCode,
) {
  const userBalance = await this.getUserBalance(user);
  const payment = new this({
    status: 'SUCCEEDED',
    user,
    amount,
    description,
    transactionCode,
    balance: userBalance.balance + amount,
  });

  const [savedPayments] = await this.create([payment]);

  return savedPayments;
};

module.exports = model('Payment', schema);
