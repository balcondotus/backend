const { Schema, model, SchemaTypes } = require('mongoose');
const mongooseLeanGetters = require('mongoose-lean-getters');
const AppConfig = require('config/app');
const { stripAllHtmlTags } = require('bin/utils/string');
const { escape, unescape } = require('lodash');
const {
  EventBufferModel,
  EventDateRangeModel,
  EventPaymentModel,
  EventQuestionModel,
  EventAvailabilityModel,
} = require('./embedded');

const schema = new Schema(
  {
    // Set unique, because each user has only one event at this stage.
    user: { type: SchemaTypes.ObjectId, ref: 'User', required: true, unique: true },
    username: {
      type: SchemaTypes.String,
      trim: true,
      lowercase: true,
      sparse: true,
      minlength: AppConfig.minUsernameLength,
      maxlength: AppConfig.maxUsernameLength,
      validate: {
        validator(v) {
          return AppConfig.allowedUsernamePattern.test(v);
        },
        message:
          'Username can only contain alphanumeric characters (letters A-Z, numbers 0-9) with the exception of underscores',
      },
    },
    isPublic: { type: SchemaTypes.Boolean, default: true, required: true },
    description: {
      type: SchemaTypes.String,
      maxlength: 65535,
      trim: true,
      set: escape,
      get: unescape,
    },
    tagline: { type: SchemaTypes.String, maxlength: 255, trim: true, set: stripAllHtmlTags },
    title: { type: SchemaTypes.String, maxlength: 255, trim: true },
    duration: { type: SchemaTypes.Number, min: 1, max: 720, required: true, default: 30 }, // in minutes
    frequency: { type: SchemaTypes.Number, min: 5, max: 60, required: true, default: 30 }, // in minutes
    payment: { type: EventPaymentModel, default: {}, required: true },
    buffer: { type: EventBufferModel, default: {}, required: true },
    meetingsPerDay: { type: SchemaTypes.Number, min: 0, max: 1440, default: 0 }, // in minutes, 0 means unlimited
    noticeOfSchedule: { type: SchemaTypes.Number, default: 240, max: 525600, min: 0 }, // in minutes
    dateRange: { type: EventDateRangeModel, default: {}, required: true },
    createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
    disabledAt: { type: SchemaTypes.Date, default: null },
    deletedAt: { type: SchemaTypes.Date, default: null },
    questions: [{ type: EventQuestionModel }],
    availabilities: { type: EventAvailabilityModel, default: {} },
  },
  { minimize: false },
);

/**
 * Gets User's default event.
 * This is helpful since each user has a single event.
 *
 * @param {ObjectId} user
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getUserDefaultEvent = async function getUserDefaultEvent(user) {
  return this.findOne(
    { user },
    {
      user: false,
      isPublic: false,
      username: false,
      title: false,
      createdAt: false,
      deletedAt: false,
    },
  )
    .lean({ getters: true })
    .exec();
};

/**
 * Finds User's default event.
 *
 * @param {ObjectId} user
 */
schema.statics.findUserDefaultEvent = async function findUserDefaultEvent(user) {
  return this.findOne({ user }).exec();
};

schema.statics.findUserDefaultEventLean = async function findUserDefaultEventLean(user) {
  return this.findOne({ user }).lean({ getters: true }).exec();
};

schema.index({ username: 1, user: 1 }, { unique: true, sparse: true });

schema.plugin(mongooseLeanGetters);

module.exports = model('Event', schema);
