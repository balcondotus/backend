const { Schema, model, startSession, SchemaTypes, isValidObjectId } = require('mongoose');
const moment = require('moment');
const _ = require('lodash');
const AppConfig = require('config/app');
const {
  MeetingCostModel,
  MeetingAnswerEventQuestionModel,
  MeetingTimeModel,
  MeetingCreatedModel,
  MeetingRescheduledModel,
  MeetingCanceledModel,
  MeetingConfirmedModel,
} = require('./embedded');
const PaymentModel = require('./payment-model');
const DisputeModel = require('./dispute-model');

const schema = new Schema({
  event: { type: SchemaTypes.ObjectId, ref: 'Event', required: true },
  host: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
  invitee: { type: SchemaTypes.ObjectId, ref: 'User', required: true },
  cost: { type: MeetingCostModel, required: true },
  time: { type: MeetingTimeModel, required: true },
  status: {
    type: SchemaTypes.String,
    enum: ['CREATED', 'CANCELED', 'RESCHEDULED', 'DISPUTED', 'CONFIRMED'],
    default: 'CREATED',
    trim: true,
    required: true,
  },
  created: {
    type: MeetingCreatedModel,
    required: true,
  },
  rescheduled: {
    type: MeetingRescheduledModel,
    require() {
      return this.status === 'RESCHEDULED';
    },
  },
  disputed: {
    type: SchemaTypes.ObjectId,
    ref: 'Dispute',
    require() {
      return this.status === 'DISPUTED';
    },
  },
  canceled: {
    type: MeetingCanceledModel,
    require() {
      return this.status === 'CANCELED';
    },
  },
  confirmed: {
    type: MeetingConfirmedModel,
    require() {
      return this.status === 'CONFIRMED';
    },
  },
  payments: [{ type: SchemaTypes.ObjectId, ref: 'Payment', select: false }],
  answers: [{ type: MeetingAnswerEventQuestionModel }],
});

/**
 * Counts user's upcoming meetings.
 *
 * @param {ObjectId} userId
 */
schema.statics.countUpcomingMeetings = async function countUpcomingMeetings(userId) {
  return this.count({ $or: [{ host: userId }, { invitee: userId }] })
    .where('status', 'CREATED')
    .where('time.start')
    .gte(new Date())
    .exec();
};

/**
 * Finds all meetings need reminders.
 */
schema.statics.findRemindNeededMeetings = function findRemindNeededMeetings() {
  return this.find()
    .where('status', 'CREATED')
    .where(
      'time.start',
      moment.utc().startOf('minute').add(AppConfig.minutesBeforeMeetingToRemind, 'minutes'),
    )
    .exec();
};

/**
 * Gets all meetings where the provided user is the host or the invitee.
 *
 * @param {ObjectId} userId
 * @param {Date|null} startDate
 * @param {Date|null} endDate
 * @param {string|string[]} status
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getMeetings = async function getMeetings(userId, startDate, endDate, status) {
  /**
   * @type {import('mongoose').DocumentQuery<Document[],Document,{}>}
   */
  const query = this.findMeetings(userId, startDate, endDate, status)
    .populate('created.user')
    .populate('rescheduled.user')
    .populate('rescheduled.rescheduledTo')
    .populate('canceled.user')
    .populate('disputed')
    .populate('host')
    .populate('invitee');

  return query
    .lean({
      virtuals: true,
    })
    .exec();
};

schema.statics.findMeetings = function findMeetings(userId, startDate, endDate, status) {
  const query = this.find();

  if (isValidObjectId(userId)) {
    query.or([{ host: userId }, { invitee: userId }]);
  }

  if (startDate && !endDate) {
    // Upcoming
    query.where('time.end').gte(startDate).where('status', 'CREATED').sort('time.start');
  } else if (!startDate && endDate) {
    // Past
    query.where('time.end').lte(endDate).sort('-time.start');
  } else if (startDate && endDate) {
    // Range
    query.where('time.start').gte(startDate).where('time.start').lte(endDate).sort('time.start');
  }

  if (_.isString(status)) {
    query.where('status', _.toUpper(status));
  } else if (_.isArray(status)) {
    query.or(
      status.map((s) => {
        return { status: _.toUpper(s) };
      }),
    );
  }

  return query;
};

/**
 * Reschedule.
 *
 * @param {any} currentUserId
 * @param {any} meetingId
 * @param {String} reason
 * @param {Date} startDate
 * @param {Date} endDate
 */
schema.statics.reschedule = async function reschedule(
  currentUserId,
  meetingId,
  reason,
  startDate,
  endDate,
) {
  const session = await startSession();
  session.startTransaction();

  const meetingDoc = await this.findById(meetingId).session(session).exec();

  let newMeeting = _.cloneDeep(meetingDoc);
  newMeeting._id = undefined;
  newMeeting.isNew = true;
  newMeeting.time = { start: startDate, end: endDate };
  newMeeting.created = { user: currentUserId };

  [newMeeting] = await this.create([newMeeting], { session });

  meetingDoc.status = 'RESCHEDULED';
  meetingDoc.rescheduled = {
    user: currentUserId,
    rescheduledTo: newMeeting._id,
    reason,
  };

  await meetingDoc.save({ validateModifiedOnly: true });

  await session.commitTransaction();
  session.endSession();

  return newMeeting;
};

schema.statics.dispute = async function dispute(meetingId, userId, reason) {
  const session = await startSession();
  session.startTransaction();

  const meetingDoc = await this.findById(meetingId).session(session).exec();

  meetingDoc.status = 'DISPUTED';

  const disputeDoc = await DisputeModel.createAndInsertOne(
    userId,
    meetingDoc._id,
    'PENDING',
    reason,
    session,
  );

  meetingDoc.disputed = disputeDoc._id;

  await meetingDoc.save({ validateModifiedOnly: true });

  await session.commitTransaction();
  session.endSession();

  return disputeDoc;
};

/**
 * Creates a new meeting.
 */
schema.methods.createNewMeeting = async function createNewMeeting() {
  const session = await startSession();
  session.startTransaction();

  const [savedMeeting] = await model('Meeting').create([this], { session });

  const inviteeBalance = await PaymentModel.getUserBalance(this.invitee);
  const meetingTotalCost = this.cost.sum * -1;
  const payment = new PaymentModel({
    user: this.invitee,
    amount: meetingTotalCost,
    description: `The cost of the meeting ${savedMeeting._id}`,
    status: 'SUCCEEDED',
    balance: inviteeBalance.balance + meetingTotalCost,
  });

  const [savedPayment] = await PaymentModel.create([payment], { session });

  savedMeeting.payments = [savedPayment._id];
  await savedMeeting.save();

  await session.commitTransaction();
  session.endSession();

  return savedMeeting;
};

/**
 * Cancels the meeting
 *
 * @param {String} meetingId
 * @param {String} currentUserId
 * @param {String} reason
 */
schema.statics.cancel = async function cancel(meetingId, currentUserId, reason) {
  const session = await startSession();
  session.startTransaction();

  const meeting = await this.findById(meetingId).session(session);

  meeting.status = 'CANCELED';
  meeting.canceled = {
    user: currentUserId,
    reason,
  };

  const amount = meeting.cost.sum;
  const newPayment = await PaymentModel.newPayment(
    meeting.invitee,
    amount,
    'SUCCEEDED',
    `Meeting ${meeting._id} has canceled.`,
    session,
  );

  meeting.payments.push(newPayment);

  await meeting.save({ validateModifiedOnly: true });

  await session.commitTransaction();
  session.endSession();

  return meeting;
};

schema.statics.getCanceledMeeting = async function getCanceledMeeting(meetingId) {
  if (!isValidObjectId(meetingId)) throw new Error('Invalid Meeting ID');

  /**
   * @type {import('mongoose').DocumentQuery<Document[],Document,{}>}
   */
  const query = this.findById(meetingId)
    .populate('created.user')
    .populate('canceled.user')
    .populate('host')
    .populate('invitee');

  return query
    .lean({
      virtuals: true,
    })
    .exec();
};

schema.statics.getDisputedMeeting = async function getDisputedMeeting(meetingId) {
  if (!isValidObjectId(meetingId)) throw new Error('Invalid Meeting ID');

  /**
   * @type {import('mongoose').DocumentQuery<Document[],Document,{}>}
   */
  const query = this.findById(meetingId)
    .populate('created.user')
    .populate('disputed')
    .populate('host')
    .populate('invitee');

  return query
    .lean({
      virtuals: true,
    })
    .exec();
};

/**
 * Gets meeting.
 *
 * @param {any} meetingId
 */
schema.statics.getMeeting = async function getMeeting(meetingId) {
  return this.findById(meetingId, { __v: false })
    .populate('event')
    .populate('host')
    .populate('invitee')
    .lean({ virtuals: true })
    .exec();
};

schema.statics.findSuccessfulMeetings = function findSuccessfulMeetings() {
  return this.find()
    .where('status', 'CREATED')
    .where('time.start')
    .lte(moment.utc().subtract(AppConfig.allowedDisputeHours, 'hour'))
    .exec();
};

schema.methods.confirm = async function confirm() {
  const session = await startSession();
  session.startTransaction();

  this.$session(session);

  this.status = 'CONFIRMED';
  this.confirmed = { confirmedAt: Date.now() };

  const amount = this.cost.rate;
  const newPayment = await PaymentModel.newPayment(
    this.host._id || this.host,
    amount,
    'SUCCEEDED',
    `Meeting ${this._id} has successfully confirmed.`,
    session,
  );

  this.payments.push(newPayment);

  await this.save({ validateModifiedOnly: true });

  await session.commitTransaction();
  session.endSession();

  return this;
};

module.exports = model('Meeting', schema);
