const { Schema, model, startSession, SchemaTypes, isValidObjectId } = require('mongoose');
const mongooseLeanVirtuals = require('mongoose-lean-virtuals');
const moment = require('moment');
const _ = require('lodash');
const validator = require('validator');
const TokenUtils = require('bin/utils/token');
const AppConfig = require('config/app');
const { stripSpaces } = require('bin/utils/string');
const EventModel = require('./event-model');
const {
  UserProfileModel,
  UserNotificationModel,
  PhoneModel,
  EmailModel,
  UserPasswordModel,
  UserOauthModel,
  UserOptionsModel,
} = require('./embedded');

const schema = new Schema(
  {
    username: {
      type: SchemaTypes.String,
      trim: true,
      lowercase: true,
      sparse: true,
      unique: true,
      minlength: AppConfig.minUsernameLength,
      maxlength: AppConfig.maxUsernameLength,
      validate: {
        validator(v) {
          return AppConfig.allowedUsernamePattern.test(v);
        },
        message:
          'Username can only contain alphanumeric characters (letters A-Z, numbers 0-9) with the exception of underscores',
      },
    },
    iban: {
      type: SchemaTypes.String,
      trim: true,
      unique: true,
      sparse: true,
      set: stripSpaces,
      validate: [
        {
          validator(v) {
            if (!_.isEmpty(v)) {
              return _.isString(v) && _.startsWith(v, 'IR') && validator.isIBAN(v);
            }

            return true;
          },
          message: () => 'Invalid IR IBAN',
        },
      ],
    },

    phone: { type: PhoneModel },

    email: { type: EmailModel, required: true },

    password: { type: UserPasswordModel },

    oauths: [{ type: UserOauthModel }],

    token: { type: SchemaTypes.String, required: true, unique: true, trim: true },

    profile: { type: UserProfileModel, required: true },
    notifications: { type: UserNotificationModel, default: {}, required: true },

    timeZone: {
      type: SchemaTypes.String,
      enum: moment.tz.names(),
      default: 'Asia/Tehran',
      trim: true,
      required: true,
    },
    locale: {
      type: SchemaTypes.String,
      enum: AppConfig.supportedLocales,
      trim: true,
      lowercase: true,
      default: 'fa',
      maxlength: 5,
      required: true,
    },

    options: { type: UserOptionsModel, default: {} },

    createdAt: { type: SchemaTypes.Date, default: Date.now, required: true },
    deletedAt: SchemaTypes.Date,
  },
  { toJSON: { virtuals: true } },
);

schema.virtual('identifier').get(function virtual() {
  return this.username || this._id;
});

/**
 * Creates a user along with an event because we need an event as well.
 */
schema.methods.createUserAndEvent = async function createUserAndEvent() {
  const session = await startSession();
  session.startTransaction();

  const [savedUsers] = await model('User').create([this], { session });

  const event = new EventModel({
    user: this._id,
    questions: [
      {
        title: 'راجع به چی می‌خواهید با من صحبت کنید؟',
      },
    ],
  });

  await EventModel.create([event], { session });

  await session.commitTransaction();
  session.endSession();

  return savedUsers;
};

/**
 * Updates User and her default event.
 *
 * @param {Function} cb Update necessary fields here
 */
schema.methods.updateUserAndEvent = async function updateUserAndEvent(cb) {
  const session = await startSession();
  session.startTransaction();

  const userEvent = await EventModel.findUserDefaultEvent(this._id);

  cb(this, userEvent);

  await this.save({ session });
  await userEvent.save({ session });

  await session.commitTransaction();
  session.endSession();

  return true;
};

/**
 * Assigns a new token if the user is not authorized.
 * Note: It only assigns, you have to save the model explicitly.
 */
schema.methods.assignToken = async function assignToken() {
  // Declaring this line above causes circular dependency.
  // eslint-disable-next-line global-require
  const { AuthMiddleware } = require('lib/middlewares');

  try {
    await AuthMiddleware.authorize(this.token);

    // User exists, and token not expired, so no need to generate a new one.
    return true;
  } catch (e) {
    // User doesn't exist, or she exists but her token has been expired, so generate a new one.
    if (e.status === 401) {
      this.token = TokenUtils.generateToken(this._id);
      return true;
    }

    // Something wrong happened
    return false;
  }
};

schema.statics.getLeanUser = function getLeanUser(userId) {
  return this.findById(userId, {
    __v: false,
  })
    .lean({
      virtuals: true,
    })
    .exec();
};

/**
 * Gets User by ID.
 *
 * @param {String} userId User's ID
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getUserById = async function getUserById(userId) {
  const queryOrConditions = [];

  // Search in IDs
  if (isValidObjectId(userId)) queryOrConditions.push({ _id: userId });
  // Search in usernames
  queryOrConditions.push({ username: userId });

  const user = await this.findOne(
    { $and: [{ _id: userId }, { deletedAt: null }] },
    {
      _id: true,
      profile: true,
    },
  )
    .lean({
      virtuals: true,
    })
    .exec();

  if (!user) throw new Error('User not found');

  return user;
};

/**
 * Gets User by identifier.
 *
 * @param {String} userIdentifier User's username or ID
 *
 * @returns {Object} Plain JS object
 */
schema.statics.getUserByIdentifier = async function getUserByIdentifier(userIdentifier) {
  const queryOrConditions = [];

  // Search in IDs
  if (isValidObjectId(userIdentifier)) queryOrConditions.push({ _id: userIdentifier });
  // Search in usernames
  queryOrConditions.push({ username: userIdentifier });

  const user = await this.findOne(
    { $or: queryOrConditions, $and: [{ deletedAt: null }] },
    {
      _id: true,
      profile: true,
    },
  )
    .lean({
      virtuals: true,
    })
    .exec();

  if (!user) throw new Error('User not found');

  return user;
};

schema.statics.findByIdentifier = async function findByIdentifier(
  identifier,
  includeDeleted = true,
) {
  const queryOrConditions = [];

  // Search in IDs
  if (isValidObjectId(identifier)) queryOrConditions.push({ _id: identifier });
  // Search in usernames
  queryOrConditions.push({ username: identifier });

  const user = this.findOne({ $or: queryOrConditions });

  if (!includeDeleted) {
    user.and([{ deletedAt: null }]);
  }

  return user.exec();
};

/**
 * Find a document by auth provider ID
 *
 * @param {String} provider
 * @param {Number} providerId
 */
schema.statics.findByProviderId = async function findByProviderId(provider, providerId) {
  return this.findOne({ 'oauths.provider': provider, 'oauths.accountId': providerId }).exec();
};

/**
 * Find a document by email
 *
 * @param {String} email
 */
schema.statics.findByEmail = async function findByEmail(email) {
  return this.findOne({ 'email.email': email }).exec();
};

/**
 * Find a document by password reset token
 *
 * @param {String} token
 */
schema.statics.findByPasswordResetToken = async function findByPasswordResetToken(token) {
  return this.findOne({ 'password.resetToken': token })
    .where('password.tokenExpiresAt')
    .gt(moment.utc().toDate())
    .exec();
};

/**
 * Find a document by ID and email
 *
 * @param {String} id
 * @param {String} email
 *
 * @returns {Object} Plain JS object
 */
schema.statics.findByIdEmail = async function findByIdEmail(id, email) {
  return this.findOne({ _id: id, 'email.email': email }).lean().exec();
};

/**
 * Finds User by username.
 *
 * @param {String} username
 */
schema.statics.findByUsername = async function findByUsername(username) {
  return this.findOne({ username }).exec();
};

/**
 * Deletes the user
 *
 * @param {String} id User ID
 */
schema.statics.deleteUser = async function deleteUser(id) {
  return this.findOneAndUpdate({ _id: id }, { deletedAt: Date.now() }).exec();
};

/**
 * Finds the user with a email verification token.
 *
 * @param {String} token
 */
schema.statics.findByEmailVerificationToken = async function findByEmailVerificationToken(token) {
  return this.findOne({ 'email.verificationToken': token })
    .where('email.tokenExpiresAt')
    .gt(moment.utc().toDate())
    .exec();
};

/**
 * Adds the method into the notification, or removes it.
 *
 * @param {String} id
 * @param {String} notification
 * @param {String} method
 */
schema.statics.saveNotification = async function saveNotification(id, notification, method) {
  const user = await this.findById(id, { notifications: true }).exec();

  if (user.notifications[notification].includes(method))
    user.notifications[notification] = user.notifications[notification].filter((item) => {
      return item !== method;
    });
  else user.notifications[notification].push(method);

  return user.save({ validateModifiedOnly: true });
};

schema.statics.disableNotification = async function disableNotification(id, notification, method) {
  const user = await this.findById(id, { notifications: true }).exec();

  if (user.notifications[notification].includes(method))
    user.notifications[notification] = user.notifications[notification].filter((item) => {
      return item !== method;
    });

  return user.save({ validateModifiedOnly: true });
};

/**
 * Changes IBAN empty string with null.
 */
schema.pre('save', function save(next) {
  if (this.isModified('iban') && _.isEmpty(this.iban)) {
    this.iban = undefined;
  }

  next();
});

// Plugins
schema.plugin(mongooseLeanVirtuals);

module.exports = model('User', schema);
