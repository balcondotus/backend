const fs = require('fs-extra');
const { BadRequest, InternalServerError, Forbidden } = require('http-errors');
const _ = require('lodash');
const FileType = require('file-type');

/**
 * Writes given chunk into the given path in `req`.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
function writeChunk(options) {
  if (!options || !options.allowedMimeTypes)
    throw new InternalServerError('Options parameter is required');

  const closure =
    writeChunk[options] ||
    (async (req, res, next) => {
      try {
        const {
          body,
          query: { patch: tempFilePath },
        } = req;
        // const uploadName = req.get('upload-name');
        let uploadLength = req.get('upload-length');
        let uploadOffset = req.get('upload-offset');

        if (
          !_.isString(tempFilePath) ||
          _.isEmpty(tempFilePath) ||
          !fs.existsSync(tempFilePath) ||
          _.isEmpty(body) ||
          _.isNaN(uploadLength) ||
          _.isNaN(uploadOffset)
        )
          throw new BadRequest('Upload cannot be started');

        // Make sure of types
        uploadLength = _.toNumber(uploadLength);
        uploadOffset = _.toNumber(uploadOffset);

        if (uploadLength === 0 || uploadOffset < 0 || uploadOffset >= uploadLength)
          throw new BadRequest('Upload cannot be proceeded');

        // Determine mime type from the first chunk
        if (uploadOffset === 0) {
          const chunkMimeType = await FileType.fromBuffer(body);

          if (
            !_.isUndefined(chunkMimeType) &&
            !options.allowedMimeTypes.includes(chunkMimeType.mime)
          ) {
            fs.removeSync(tempFilePath);
            throw new Forbidden(`Mime type (${chunkMimeType.mime}) is not supported`);
          }
        }

        fs.appendFileSync(tempFilePath, body);

        next();
      } catch (e) {
        next(e);
      }
    });

  return closure;
}

/**
 * Determines next requested offset in the case of failure.
 * Find the offset through `req.uploaded.offset`.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
function nextRequestedOffset(req, res, next) {
  try {
    const {
      query: { patch: tempFilePath },
    } = req;

    if (!_.isString(tempFilePath) || _.isEmpty(tempFilePath) || !fs.existsSync(tempFilePath))
      throw new BadRequest("Temp file is invalid or doesn't exist");

    const buffer = fs.readFileSync(tempFilePath);

    req.uploaded = { offset: buffer.length };

    next();
  } catch (e) {
    next(e);
  }
}

module.exports = {
  nextRequestedOffset,
  writeChunk,
};
