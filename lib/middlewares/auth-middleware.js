const passport = require('passport');
const jwt = require('jsonwebtoken');
const { Unauthorized, Forbidden } = require('http-errors');
const _ = require('lodash');
const { UserModel } = require('lib/models');
const TokenUtils = require('bin/utils/token');

/**
 * Authentication is required.
 */
exports.required = (req, res, next) =>
  passport.authenticate('jwt', { session: false }, (_err, user) => {
    if (user) {
      req.user = user;
      return next();
    }

    return next(new Unauthorized('Not Authorized'));
  })(req, res, next);

/**
 * Authentication is required to be empty.
 */
exports.guest = (req, res, next) =>
  passport.authenticate('jwt', { session: false }, (_err, user) => {
    if (!user) return next();

    return next(new Forbidden('You are already authorized.'));
  })(req, res, next);

/**
 * Authentication is optional.
 */
exports.optional = (req, res, next) =>
  passport.authenticate(['jwt', 'anonymous'], { session: false }, (_err, user) => {
    if (user) req.user = user;
    next();
  })(req, res, next);

/**
 * Authorizes the token.
 *
 * @param {String} token
 */
exports.authorize = async (token) => {
  try {
    if (TokenUtils.isTokenEmpty(token)) throw new Unauthorized('Token is malformed');

    const { _id } = jwt.verify(token, process.env.JWT_SECRET, {
      audience: process.env.JWT_AUDIENCE,
      issuer: process.env.JWT_ISSUER,
    });

    const foundUser = await UserModel.findById(_id);

    if (!foundUser) throw new Unauthorized('User not found');

    if (_.isDate(foundUser.deletedAt)) throw new Unauthorized('User has been deleted');

    return foundUser;
  } catch (e) {
    if (e.name === 'TokenExpiredError') throw new Unauthorized('Token has been expired');

    throw e;
  }
};
