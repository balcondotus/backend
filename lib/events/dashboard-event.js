/**
 * This file contains only dashboard events.
 */

const {
  AccountController,
  NotificationController,
  PaymentController,
  CalendarController,
  MeetingController,
  EmailController,
  ProfileController,
  EventController,
} = require('lib/controllers');
const SOCKET = require('lib/consts/socket');

/**
 * Handles read notification event.
 *
 * @param {SocketIO.Server} socket
 */
exports.markNotificationsRead = (socket) => {
  socket.on(SOCKET.EVENT.READ_NOTIFICATIONS, async (fn) => {
    const result = await NotificationController.markNotificationsRead(socket.user._id);
    fn(result);
  });
};

/**
 * Handles get notifications event.
 *
 * @param {SocketIO.Server} socket
 */
exports.getUnreadNotifications = (socket) => {
  socket.on(SOCKET.EVENT.NOTIFICATIONS, async (fn) => {
    const result = await NotificationController.getUnreadNotifications(socket.user._id);
    fn(result);
  });
};

/**
 * Handles get balance event.
 *
 * @param {SocketIO.Server} socket
 */
exports.getBalance = (socket) => {
  socket.on(SOCKET.EVENT.BALANCE, async (fn) => {
    const balance = await PaymentController.getBalance(socket.user._id);
    fn(balance);
  });
};

/**
 * Handles get meetings event.
 *
 * @param {SocketIO.Server} socket
 */
exports.getMeetings = (socket) => {
  socket.on(SOCKET.EVENT.MEETINGS, async (startDate, endDate, fn) => {
    const result = await MeetingController.getMeetings(socket.user._id, startDate, endDate);
    fn(result);
  });
};

/**
 * Handles cancel meetings event.
 *
 * @param {SocketIO.Server} socket
 */
exports.cancelMeetings = (socket) => {
  socket.on(SOCKET.EVENT.CANCEL_MEETING, async (meetingId, reason, fn) => {
    const result = await MeetingController.cancel(socket.user._id, meetingId, reason);
    fn(result);
  });
};

exports.disableNotificationsByMethod = (socket) => {
  socket.on(SOCKET.EVENT.DISABLE_NOTIFICATIONS, async (method, fn) => {
    const result = await NotificationController.disableNotificationsByMethod(
      socket.user._id,
      method,
    );
    fn(result);
  });
};

/**
 * Handles save notification changes.
 *
 * @param {SocketIO.Server} socket
 */
exports.saveNotificationChange = (socket) => {
  socket.on(SOCKET.EVENT.SAVE_NOTIFICATION, async (notification, method, fn) => {
    const result = await NotificationController.saveSettings(socket.user._id, notification, method);
    fn(result);
  });
};

/**
 * Handles unlinking oauths.
 *
 * @param {SocketIO.Server} socket
 */
exports.unlinkOauth = (socket) => {
  socket.on(SOCKET.EVENT.UNLINK_OAUTH, async (provider, fn) => {
    const result = await AccountController.unlinkOauth(socket.user._id, provider);
    fn(result);
  });
};

/**
 * Handles Saving account changes.
 *
 * @param {SocketIO.Server} socket
 */
exports.saveAccountChanges = (socket) => {
  socket.on(SOCKET.EVENT.SAVE_ACCOUNT, async (phone, email, timeZone, locale, fn) => {
    const result = await AccountController.saveAccountChanges(
      socket.user._id,
      phone,
      email,
      timeZone,
      locale,
    );
    fn(result);
  });
};

/**
 * Handles saving password.
 *
 * @param {SocketIO.Server} socket
 */
exports.savePassword = (socket) => {
  socket.on(SOCKET.EVENT.SAVE_PASSWORD, async (currentPassword, newPassword, fn) => {
    const result = await AccountController.savePassword(
      socket.user._id,
      currentPassword,
      newPassword,
    );
    fn(result);
  });
};

/**
 * Handles deleting User's account
 *
 * @param {SocketIO.Server} socket
 */
exports.deleteAccount = (socket) => {
  socket.on(SOCKET.EVENT.DELETE_ACCOUNT, async (fn) => {
    const result = await AccountController.deleteAccount(socket.user);
    fn(result);
  });
};

/**
 * Handles sending a verification to User's email address.
 *
 * @param {SocketIO.Server} socket
 */
exports.sendEmailVerification = (socket) => {
  socket.on(SOCKET.EVENT.SEND_EMAIL_VERIFICATION, async (fn) => {
    const result = await EmailController.sendEmailVerification(socket.user._id);
    fn(result);
  });
};

/**
 * Handles sending a verification to User's phone number.
 *
 * @param {SocketIO.Server} socket
 */
exports.sendPhoneVerification = (socket) => {
  socket.on(SOCKET.EVENT.SEND_PHONE_VERIFICATION, async (fn) => {
    const result = await AccountController.sendPhoneVerificationCode(socket.user._id);
    fn(result);
  });
};

/**
 * Handles verifying a verification code User entered.
 *
 * @param {SocketIO.Server} socket
 */
exports.verifyPhoneVerificationCode = (socket) => {
  socket.on(SOCKET.EVENT.VERIFY_PHONE_VERIFICATION_CODE, async (code, fn) => {
    const result = await AccountController.verifyPhoneVerificationCode(socket.user._id, code);
    fn(result);
  });
};

/**
 * Handles saving finance settings.
 *
 * @param {SocketIO.Server} socket
 */
exports.saveFinanceSettings = (socket) => {
  socket.on(SOCKET.EVENT.SAVE_FINANCE_SETTINGS, async (iban, fn) => {
    const result = await PaymentController.saveSettings(socket.user._id, iban);
    fn(result);
  });
};

/**
 * Handles saving calendar settings.
 *
 * @param {SocketIO.Server} socket
 */
exports.saveCalendarSettings = (socket) => {
  socket.on(
    SOCKET.EVENT.SAVE_CALENDAR_SETTINGS,
    async (
      availabilities,
      duration,
      frequency,
      buffer,
      meetingsPerDay,
      noticeOfSchedule,
      dateRange,
      fn,
    ) => {
      const result = await CalendarController.saveSettings(
        socket.user._id,
        availabilities,
        duration,
        frequency,
        buffer,
        meetingsPerDay,
        noticeOfSchedule,
        dateRange,
      );
      fn(result);
    },
  );
};

/**
 * Handles saving profile settings.
 *
 * @param {SocketIO.Server} socket
 */
exports.saveProfileSettings = (socket) => {
  socket.on(
    SOCKET.EVENT.SAVE_PROFILE_SETTINGS,
    async (
      username,
      firstName,
      lastName,
      gender,
      rate,
      tagline,
      description,
      newAvatar,
      fn,
    ) => {
      const result = await ProfileController.saveSettings(
        socket.user._id,
        username,
        firstName,
        lastName,
        gender,
        rate,
        tagline,
        description,
        newAvatar,
      );
      fn(result);
    },
  );
};

/**
 * Handles getting User's payments list.
 *
 * @param {SocketIO.Server} socket
 */
exports.getPayments = (socket) => {
  socket.on(SOCKET.EVENT.PAYMENTS, async (fn) => {
    const result = await PaymentController.getPayments(socket.user._id);
    fn(result);
  });
};

/**
 * Handles updating User options.
 *
 * @param {SocketIO.Server} socket
 */
exports.updateUserOptions = (socket) => {
  socket.on(SOCKET.EVENT.UPDATE_USER_OPTION, async (optionKey, optionValue, fn) => {
    const result = await AccountController.updateUserOption(
      socket.user._id,
      optionKey,
      optionValue,
    );
    fn(result);
  });
};

/**
 * Handles getting User's default event.
 *
 * @param {SocketIO.Server} socket
 */
exports.getUserDefaultEvent = (socket) => {
  socket.on(SOCKET.EVENT.USER_DEFAULT_EVENT, async (fn) => {
    const result = await EventController.getUserDefaultEvent(socket.user._id);
    fn(result);
  });
};
