const moment = require('moment');
const IntervalUtils = require('../../bin/utils/interval');
require('../../lib/loaders/moment-loader')();

beforeAll(() => {
  jest.spyOn(Date, 'now').mockImplementation(() => new Date('2020-10-05T00:00:00.000Z'));
});

describe('interval utils', () => {
  describe('make intervals', () => {
    describe('should be within the range with 15 minutes duration', () => {
      it('same date', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').startOf('date'),
          moment.utc('2020-10-05').set('hour', 6),
          60,
          15,
        );
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T00:00', end: '2020-10-05T01:00' },
            { start: '2020-10-05T01:00', end: '2020-10-05T02:00' },
            { start: '2020-10-05T02:00', end: '2020-10-05T03:00' },
            { start: '2020-10-05T03:00', end: '2020-10-05T04:00' },
            { start: '2020-10-05T04:00', end: '2020-10-05T05:00' },
            { start: '2020-10-05T05:00', end: '2020-10-05T06:00' },
          ],
        };

        expect(intervals).toStrictEqual(expected);
      });

      it('two days in a row', () => {
        const result = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 20),
          moment.utc('2020-10-06').set('hour', 6),
          60,
          15,
        );
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T20:00', end: '2020-10-05T21:00' },
            { start: '2020-10-05T21:00', end: '2020-10-05T22:00' },
            { start: '2020-10-05T22:00', end: '2020-10-05T23:00' },
            { start: '2020-10-05T23:00', end: '2020-10-06T00:00' },
          ],
          '2020-10-06': [
            { start: '2020-10-06T00:00', end: '2020-10-06T01:00' },
            { start: '2020-10-06T01:00', end: '2020-10-06T02:00' },
            { start: '2020-10-06T02:00', end: '2020-10-06T03:00' },
            { start: '2020-10-06T03:00', end: '2020-10-06T04:00' },
            { start: '2020-10-06T04:00', end: '2020-10-06T05:00' },
            { start: '2020-10-06T05:00', end: '2020-10-06T06:00' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });
    });

    describe('should be within the range with 1 hour duration', () => {
      it('same date', () => {
        const result = IntervalUtils.make(
          moment.utc('2020-10-05').startOf('date'),
          moment.utc('2020-10-05').set('hour', 6),
          60,
          60,
        );
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T00:00', end: '2020-10-05T01:00' },
            { start: '2020-10-05T01:00', end: '2020-10-05T02:00' },
            { start: '2020-10-05T02:00', end: '2020-10-05T03:00' },
            { start: '2020-10-05T03:00', end: '2020-10-05T04:00' },
            { start: '2020-10-05T04:00', end: '2020-10-05T05:00' },
            { start: '2020-10-05T05:00', end: '2020-10-05T06:00' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });

      it('two days in a row', () => {
        const result = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 20),
          moment.utc('2020-10-06').set('hour', 6),
          60,
          60,
        );
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T20:00', end: '2020-10-05T21:00' },
            { start: '2020-10-05T21:00', end: '2020-10-05T22:00' },
            { start: '2020-10-05T22:00', end: '2020-10-05T23:00' },
            { start: '2020-10-05T23:00', end: '2020-10-06T00:00' },
          ],
          '2020-10-06': [
            { start: '2020-10-06T00:00', end: '2020-10-06T01:00' },
            { start: '2020-10-06T01:00', end: '2020-10-06T02:00' },
            { start: '2020-10-06T02:00', end: '2020-10-06T03:00' },
            { start: '2020-10-06T03:00', end: '2020-10-06T04:00' },
            { start: '2020-10-06T04:00', end: '2020-10-06T05:00' },
            { start: '2020-10-06T05:00', end: '2020-10-06T06:00' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });
    });
  });

  describe('time zones', () => {
    describe('same date', () => {
      it('asia/Tehran > UTC', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 1),
          moment.utc('2020-10-05').set('hour', 6),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'Asia/Tehran', 'UTC');
        const expected = {
          '2020-10-04': [
            { start: '2020-10-04T21:30', end: '2020-10-04T22:30' },
            { start: '2020-10-04T22:30', end: '2020-10-04T23:30' },
            { start: '2020-10-04T23:30', end: '2020-10-05T00:30' },
          ],
          '2020-10-05': [
            { start: '2020-10-05T00:30', end: '2020-10-05T01:30' },
            { start: '2020-10-05T01:30', end: '2020-10-05T02:30' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });

      it('asia/Tehran > Asia/Tehran', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 1),
          moment.utc('2020-10-05').set('hour', 6),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'Asia/Tehran', 'Asia/Tehran');
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T01:00', end: '2020-10-05T02:00' },
            { start: '2020-10-05T02:00', end: '2020-10-05T03:00' },
            { start: '2020-10-05T03:00', end: '2020-10-05T04:00' },
            { start: '2020-10-05T04:00', end: '2020-10-05T05:00' },
            { start: '2020-10-05T05:00', end: '2020-10-05T06:00' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });

      it('uTC > Asia/Tehran', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 1),
          moment.utc('2020-10-05').set('hour', 6),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T04:30', end: '2020-10-05T05:30' },
            { start: '2020-10-05T05:30', end: '2020-10-05T06:30' },
            { start: '2020-10-05T06:30', end: '2020-10-05T07:30' },
            { start: '2020-10-05T07:30', end: '2020-10-05T08:30' },
            { start: '2020-10-05T08:30', end: '2020-10-05T09:30' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });

      it('Asia/Tehran > Pacific/Honolulu (-10:00)', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 1),
          moment.utc('2020-10-05').set('hour', 6),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'Asia/Tehran', 'Pacific/Honolulu');
        const expected = {
          '2020-10-04': [
            { start: '2020-10-04T11:30', end: '2020-10-04T12:30' },
            { start: '2020-10-04T12:30', end: '2020-10-04T13:30' },
            { start: '2020-10-04T13:30', end: '2020-10-04T14:30' },
            { start: '2020-10-04T14:30', end: '2020-10-04T15:30' },
            { start: '2020-10-04T15:30', end: '2020-10-04T16:30' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });
    });

    describe('two days in a row', () => {
      it('asia/Tehran > UTC', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 20),
          moment.utc('2020-10-06').set('hour', 5),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'Asia/Tehran', 'UTC');
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T16:30', end: '2020-10-05T17:30' },
            { start: '2020-10-05T17:30', end: '2020-10-05T18:30' },
            { start: '2020-10-05T18:30', end: '2020-10-05T19:30' },
            { start: '2020-10-05T19:30', end: '2020-10-05T20:30' },
            { start: '2020-10-05T20:30', end: '2020-10-05T21:30' },
            { start: '2020-10-05T21:30', end: '2020-10-05T22:30' },
            { start: '2020-10-05T22:30', end: '2020-10-05T23:30' },
            { start: '2020-10-05T23:30', end: '2020-10-06T00:30' },
          ],
          '2020-10-06': [{ start: '2020-10-06T00:30', end: '2020-10-06T01:30' }],
        };

        expect(result).toStrictEqual(expected);
      });

      it('asia/Tehran > Asia/Tehran', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 20),
          moment.utc('2020-10-06').set('hour', 5),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'Asia/Tehran', 'Asia/Tehran');
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T20:00', end: '2020-10-05T21:00' },
            { start: '2020-10-05T21:00', end: '2020-10-05T22:00' },
            { start: '2020-10-05T22:00', end: '2020-10-05T23:00' },
            { start: '2020-10-05T23:00', end: '2020-10-06T00:00' },
          ],
          '2020-10-06': [
            { start: '2020-10-06T00:00', end: '2020-10-06T01:00' },
            { start: '2020-10-06T01:00', end: '2020-10-06T02:00' },
            { start: '2020-10-06T02:00', end: '2020-10-06T03:00' },
            { start: '2020-10-06T03:00', end: '2020-10-06T04:00' },
            { start: '2020-10-06T04:00', end: '2020-10-06T05:00' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });

      it('uTC > Asia/Tehran', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 20),
          moment.utc('2020-10-06').set('hour', 5),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
        const expected = {
          '2020-10-05': [{ start: '2020-10-05T23:30', end: '2020-10-06T00:30' }],
          '2020-10-06': [
            { start: '2020-10-06T00:30', end: '2020-10-06T01:30' },
            { start: '2020-10-06T01:30', end: '2020-10-06T02:30' },
            { start: '2020-10-06T02:30', end: '2020-10-06T03:30' },
            { start: '2020-10-06T03:30', end: '2020-10-06T04:30' },
            { start: '2020-10-06T04:30', end: '2020-10-06T05:30' },
            { start: '2020-10-06T05:30', end: '2020-10-06T06:30' },
            { start: '2020-10-06T06:30', end: '2020-10-06T07:30' },
            { start: '2020-10-06T07:30', end: '2020-10-06T08:30' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });

      it('asia/Tehran > Pacific/Honolulu (-10:00)', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-06').set('hour', 1),
          60,
          15,
        );
        const result = IntervalUtils.tz(intervals, 'Asia/Tehran', 'Pacific/Honolulu');
        const expected = {
          '2020-10-04': [
            { start: '2020-10-04T22:30', end: '2020-10-04T23:30' },
            { start: '2020-10-04T23:30', end: '2020-10-05T00:30' },
          ],
          '2020-10-05': [
            { start: '2020-10-05T00:30', end: '2020-10-05T01:30' },
            { start: '2020-10-05T01:30', end: '2020-10-05T02:30' },
            { start: '2020-10-05T02:30', end: '2020-10-05T03:30' },
            { start: '2020-10-05T03:30', end: '2020-10-05T04:30' },
            { start: '2020-10-05T04:30', end: '2020-10-05T05:30' },
            { start: '2020-10-05T05:30', end: '2020-10-05T06:30' },
            { start: '2020-10-05T06:30', end: '2020-10-05T07:30' },
            { start: '2020-10-05T07:30', end: '2020-10-05T08:30' },
            { start: '2020-10-05T08:30', end: '2020-10-05T09:30' },
            { start: '2020-10-05T09:30', end: '2020-10-05T10:30' },
            { start: '2020-10-05T10:30', end: '2020-10-05T11:30' },
          ],
        };

        expect(result).toStrictEqual(expected);
      });
    });
  });

  describe('sort', () => {
    it('sort by asc', () => {
      const intervals = {
        '2020-10-06': [
          { start: '2020-10-06T03:00', end: '2020-10-06T04:00' },
          { start: '2020-10-06T01:00', end: '2020-10-06T02:00' },
        ],
        '2020-10-03': [
          { start: '2020-10-03T03:00', end: '2020-10-03T04:00' },
          { start: '2020-10-03T01:00', end: '2020-10-03T02:00' },
        ],
        '2020-10-07': [
          { start: '2020-10-07T01:00', end: '2020-10-07T02:00' },
          { start: '2020-10-07T03:00', end: '2020-10-07T04:00' },
        ],
      };
      const expected = {
        '2020-10-03': [
          { start: '2020-10-03T01:00', end: '2020-10-03T02:00' },
          { start: '2020-10-03T03:00', end: '2020-10-03T04:00' },
        ],
        '2020-10-06': [
          { start: '2020-10-06T01:00', end: '2020-10-06T02:00' },
          { start: '2020-10-06T03:00', end: '2020-10-06T04:00' },
        ],
        '2020-10-07': [
          { start: '2020-10-07T01:00', end: '2020-10-07T02:00' },
          { start: '2020-10-07T03:00', end: '2020-10-07T04:00' },
        ],
      };

      expect(IntervalUtils.sort(intervals)).toStrictEqual(expected);
    });
  });

  describe('apply notice of schedule', () => {
    describe('apply 0', () => {
      it('past intervals', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-09-01'),
          moment.utc('2020-10-05'),
          60,
          15,
        );
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 0)).toStrictEqual(expected);
      });

      it('today intervals', () => {
        const now = moment.utc();
        const intervals = IntervalUtils.make(now, now.clone().add(1, 'hour'), 60, 15);
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 0)).toStrictEqual(expected);
      });

      it('future intervals', () => {
        const future = moment.utc().add(100, 'year');
        const intervals = IntervalUtils.make(future, future.clone().add(1, 'hour'), 60, 15);
        const expected = {};
        expected[future.format('YYYY-MM-DD')] = [
          {
            start: future.format('YYYY-MM-DDTHH:mm'),
            end: future.clone().add(1, 'hour').format('YYYY-MM-DDTHH:mm'),
          },
        ];

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 0)).toStrictEqual(expected);
      });
    });

    describe('apply 262800', () => {
      it('past intervals', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-09-01'),
          moment.utc('2020-10-05'),
          60,
          15,
        );
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 262800)).toStrictEqual(expected);
      });

      it('today intervals', () => {
        const now = moment.utc();
        const intervals = IntervalUtils.make(now, now.clone().add(1, 'hour'), 60, 15);
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 262800)).toStrictEqual(expected);
      });

      it('next year intervals', () => {
        const nextYear = moment.utc().add(1, 'year');
        const intervals = IntervalUtils.make(nextYear, nextYear.clone().add(1, 'hour'), 60, 15);
        const expected = {};
        expected[nextYear.format('YYYY-MM-DD')] = [
          {
            start: nextYear.format('YYYY-MM-DDTHH:mm'),
            end: nextYear.clone().add(1, 'hour').format('YYYY-MM-DDTHH:mm'),
          },
        ];

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 262800)).toStrictEqual(expected);
      });

      it('future intervals', () => {
        const future = moment.utc().add(100, 'year');
        const intervals = IntervalUtils.make(future, future.clone().add(1, 'hour'), 60, 15);
        const expected = {};
        expected[future.format('YYYY-MM-DD')] = [
          {
            start: future.format('YYYY-MM-DDTHH:mm'),
            end: future.clone().add(1, 'hour').format('YYYY-MM-DDTHH:mm'),
          },
        ];

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 262800)).toStrictEqual(expected);
      });
    });

    describe('apply 525600', () => {
      it('past intervals', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-09-01'),
          moment.utc('2020-10-05'),
          60,
          15,
        );
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 525600)).toStrictEqual(expected);
      });

      it('today intervals', () => {
        const now = moment.utc();
        const intervals = IntervalUtils.make(now, now.clone().add(1, 'hour'), 60, 15);
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 525600)).toStrictEqual(expected);
      });

      it('next year intervals', () => {
        const nextYear = moment.utc().add(1, 'year');
        const intervals = IntervalUtils.make(nextYear, nextYear.clone().add(1, 'hour'), 60, 15);
        const expected = {};

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 525600)).toStrictEqual(expected);
      });

      it('future intervals', () => {
        const future = moment.utc().add(100, 'year');
        const intervals = IntervalUtils.make(future, future.clone().add(1, 'hour'), 60, 15);
        const expected = {};
        expected[future.format('YYYY-MM-DD')] = [
          {
            start: future.format('YYYY-MM-DDTHH:mm'),
            end: future.clone().add(1, 'hour').format('YYYY-MM-DDTHH:mm'),
          },
        ];

        expect(IntervalUtils.applyNoticeOfSchedule(intervals, 525600)).toStrictEqual(expected);
      });
    });
  });

  describe('apply buffers', () => {
    describe('with meetings', () => {
      it('apply before', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-05').set('hour', 15),
          60,
          15,
        );
        const meetings = { '2020-10-05': [{ start: '2020-10-05T12:00', end: '2020-10-05T12:30' }] };
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
            { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
          ],
        };

        expect(IntervalUtils.applyBuffers(intervals, meetings, 10, 0)).toStrictEqual(expected);
      });
      it('apply after', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-05').set('hour', 15),
          60,
          15,
        );
        const meetings = { '2020-10-05': [{ start: '2020-10-05T12:00', end: '2020-10-05T12:30' }] };
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
            { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
          ],
        };

        expect(IntervalUtils.applyBuffers(intervals, meetings, 0, 10)).toStrictEqual(expected);
      });
      it('apply both', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-05').set('hour', 15),
          60,
          15,
        );
        const meetings = {
          '2020-10-05': [{ start: '2020-10-05T12:00', end: '2020-10-05T12:30' }],
        };
        const expected = {
          '2020-10-05': [{ start: '2020-10-05T14:00', end: '2020-10-05T15:00' }],
        };

        expect(IntervalUtils.applyBuffers(intervals, meetings, 10, 60)).toStrictEqual(expected);
      });
    });

    describe('without meetings', () => {
      it('apply before', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-05').set('hour', 15),
          60,
          15,
        );
        const meetings = {};
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T12:00', end: '2020-10-05T13:00' },
            { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
            { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
          ],
        };

        expect(IntervalUtils.applyBuffers(intervals, meetings, 10, 0)).toStrictEqual(expected);
      });
      it('apply after', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-05').set('hour', 15),
          60,
          15,
        );
        const meetings = {};
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T12:00', end: '2020-10-05T13:00' },
            { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
            { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
          ],
        };

        expect(IntervalUtils.applyBuffers(intervals, meetings, 0, 10)).toStrictEqual(expected);
      });
      it('apply both', () => {
        const intervals = IntervalUtils.make(
          moment.utc('2020-10-05').set('hour', 12),
          moment.utc('2020-10-05').set('hour', 15),
          60,
          15,
        );
        const meetings = {};
        const expected = {
          '2020-10-05': [
            { start: '2020-10-05T12:00', end: '2020-10-05T13:00' },
            { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
            { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
          ],
        };

        expect(IntervalUtils.applyBuffers(intervals, meetings, 10, 60)).toStrictEqual(expected);
      });
    });
  });

  describe('apply date range', () => {
    it('rolling', () => {
      const now = moment.utc();
      const intervals = IntervalUtils.make(now, now.clone().add(10, 'day'), 60, 15);
      const expected = {
        '2020-10-05': [
          { start: '2020-10-05T00:00', end: '2020-10-05T01:00' },
          { start: '2020-10-05T01:00', end: '2020-10-05T02:00' },
          { start: '2020-10-05T02:00', end: '2020-10-05T03:00' },
          { start: '2020-10-05T03:00', end: '2020-10-05T04:00' },
          { start: '2020-10-05T04:00', end: '2020-10-05T05:00' },
          { start: '2020-10-05T05:00', end: '2020-10-05T06:00' },
          { start: '2020-10-05T06:00', end: '2020-10-05T07:00' },
          { start: '2020-10-05T07:00', end: '2020-10-05T08:00' },
          { start: '2020-10-05T08:00', end: '2020-10-05T09:00' },
          { start: '2020-10-05T09:00', end: '2020-10-05T10:00' },
          { start: '2020-10-05T10:00', end: '2020-10-05T11:00' },
          { start: '2020-10-05T11:00', end: '2020-10-05T12:00' },
          { start: '2020-10-05T12:00', end: '2020-10-05T13:00' },
          { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
          { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
          { start: '2020-10-05T15:00', end: '2020-10-05T16:00' },
          { start: '2020-10-05T16:00', end: '2020-10-05T17:00' },
          { start: '2020-10-05T17:00', end: '2020-10-05T18:00' },
          { start: '2020-10-05T18:00', end: '2020-10-05T19:00' },
          { start: '2020-10-05T19:00', end: '2020-10-05T20:00' },
          { start: '2020-10-05T20:00', end: '2020-10-05T21:00' },
          { start: '2020-10-05T21:00', end: '2020-10-05T22:00' },
          { start: '2020-10-05T22:00', end: '2020-10-05T23:00' },
          { start: '2020-10-05T23:00', end: '2020-10-06T00:00' },
        ],
      };

      expect(IntervalUtils.applyDateRange(intervals, 'rolling', 1)).toStrictEqual(expected);
    });

    describe('range', () => {
      it('range in the past', () => {
        const now = moment.utc();
        const intervals = IntervalUtils.make(now, now.clone().add(30, 'day'), 60, 15);
        const expected = {};

        expect(
          IntervalUtils.applyDateRange(intervals, 'range', {
            start: moment.utc('2020-09-01'),
            end: moment.utc('2020-10-01'),
          }),
        ).toStrictEqual(expected);
      });

      it('range in the future', () => {
        const now = moment.utc();
        const intervals = IntervalUtils.make(now, now.clone().add(30, 'day'), 60, 15);
        const expected = {};

        expect(
          IntervalUtils.applyDateRange(intervals, 'range', {
            start: moment.utc('2120-09-01'),
            end: moment.utc('2120-10-01'),
          }),
        ).toStrictEqual(expected);
      });

      it('range in the current week', () => {
        const now = moment.utc();
        const intervals = IntervalUtils.make(now, now.clone().add(1, 'hour'), 60, 15);
        const expected = {};
        expected[now.format('YYYY-MM-DD')] = [
          {
            start: now.format('YYYY-MM-DDTHH:mm'),
            end: now.clone().add(60, 'minute').format('YYYY-MM-DDTHH:mm'),
          },
        ];

        expect(
          IntervalUtils.applyDateRange(intervals, 'range', {
            start: now.clone().startOf('week'),
            end: now.clone().endOf('week'),
          }),
        ).toStrictEqual(expected);
      });
    });
    it('indefinitely', () => {
      const intervals = IntervalUtils.make(
        moment.utc('2020-10-05').set('hour', 12),
        moment.utc('2020-10-05').set('hour', 15),
        60,
        15,
      );
      const expected = {
        '2020-10-05': [
          { start: '2020-10-05T12:00', end: '2020-10-05T13:00' },
          { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
          { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
        ],
      };

      expect(IntervalUtils.applyDateRange(intervals, 'indefinitely')).toStrictEqual(expected);
    });
  });

  describe('apply max meetings per day', () => {
    it('unlimited', () => {
      const intervals = IntervalUtils.make(
        moment.utc('2020-10-05').set('hour', 12),
        moment.utc('2020-10-05').set('hour', 15),
        60,
        15,
      );
      const meetings = { '2020-10-05': [{ start: '2020-10-05T13:00', end: '2020-10-05T13:30' }] };
      const expected = {
        '2020-10-05': [
          { start: '2020-10-05T12:00', end: '2020-10-05T13:00' },
          { start: '2020-10-05T13:00', end: '2020-10-05T14:00' },
          { start: '2020-10-05T14:00', end: '2020-10-05T15:00' },
        ],
      };

      expect(IntervalUtils.applyMaxPerDay(intervals, meetings, 0)).toStrictEqual(expected);
    });

    it('limited', () => {
      const intervals = IntervalUtils.make(
        moment.utc('2020-10-05').set('hour', 12),
        moment.utc('2020-10-05').set('hour', 15),
        60,
        15,
      );
      const meetings = { '2020-10-05': [{ start: '2020-10-05T13:00', end: '2020-10-05T13:30' }] };
      const expected = {};

      expect(IntervalUtils.applyMaxPerDay(intervals, meetings, 1)).toStrictEqual(expected);
    });
  });

  it('make slots', () => {
    const intervals = IntervalUtils.make(
      moment.utc('2020-10-05').set('hour', 12),
      moment.utc('2020-10-05').set('hour', 15),
      60,
      15,
    );
    const expected = {
      '2020-10-05': ['12:00', '13:00', '14:00'],
    };

    expect(IntervalUtils.slots(intervals)).toStrictEqual(expected);
  });
});
