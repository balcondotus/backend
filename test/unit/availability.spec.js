const moment = require('moment');
require('../../lib/loaders/moment-loader')();
const AvailabilityUtils = require('../../bin/utils/availability');
const IntervalUtils = require('../../bin/utils/interval');

beforeEach(() => {
  jest.spyOn(Date, 'now').mockImplementation(() => new Date('2020-10-05T00:00:00.000Z'));
});

describe('availability utils', () => {
  describe('make intervals from availabilities', () => {
    it('should not have duplicate events while traversing Monday (with specifics) multiple times', () => {
      const availabilities = {
        sat: {
          times: [],
          specifics: {},
        },
        sun: { times: [], specifics: {} },
        mon: {
          times: [],
          specifics: {},
        },
        tue: {
          times: [],
          specifics: {},
        },
        wed: { times: [], specifics: { '2020-10-07': [{ start: '01:00', end: '05:00' }] } },
        thu: {
          times: [],
          specifics: {},
        },
        fri: { times: [], specifics: {} },
      };
      const now = moment.utc();
      const intervals = AvailabilityUtils.intervals(
        availabilities,
        now,
        now.clone().add(20, 'day'),
        60,
        15,
        'UTC',
      );

      const expected = {
        '2020-10-07': [
          { start: '2020-10-07T01:00', end: '2020-10-07T02:00' },
          { start: '2020-10-07T02:00', end: '2020-10-07T03:00' },
          { start: '2020-10-07T03:00', end: '2020-10-07T04:00' },
          { start: '2020-10-07T04:00', end: '2020-10-07T05:00' },
        ],
      };

      expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
    });

    it('should be within the range with 15 minutes duration', () => {
      const availabilities = {
        sat: {
          times: [
            { start: '11:00', end: '23:59' },
            { start: '01:00', end: '10:59' },
          ],
          specifics: {},
        },
        sun: { times: [{ start: '18:00', end: '22:00' }], specifics: {} },
        mon: {
          times: [],
          specifics: {
            '2020-10-12': [
              { start: '10:00', end: '15:00' },
              { start: '16:00', end: '21:00' },
            ],
          },
        },
        tue: {
          times: [],
          specifics: {},
        },
        wed: { times: [], specifics: {} },
        thu: { times: [], specifics: {} },
        fri: { times: [], specifics: {} },
      };
      const now = moment.utc();
      let intervals = AvailabilityUtils.intervals(
        availabilities,
        now,
        now.clone().add(7, 'day'),
        60,
        15,
        'Asia/Tehran',
      );

      intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');

      const expected = {
        '2020-10-10': [
          { start: '2020-10-10T11:00', end: '2020-10-10T12:00' },
          { start: '2020-10-10T12:00', end: '2020-10-10T13:00' },
          { start: '2020-10-10T13:00', end: '2020-10-10T14:00' },
          { start: '2020-10-10T14:00', end: '2020-10-10T15:00' },
          { start: '2020-10-10T15:00', end: '2020-10-10T16:00' },
          { start: '2020-10-10T16:00', end: '2020-10-10T17:00' },
          { start: '2020-10-10T17:00', end: '2020-10-10T18:00' },
          { start: '2020-10-10T18:00', end: '2020-10-10T19:00' },
          { start: '2020-10-10T19:00', end: '2020-10-10T20:00' },
          { start: '2020-10-10T20:00', end: '2020-10-10T21:00' },
          { start: '2020-10-10T21:00', end: '2020-10-10T22:00' },
          { start: '2020-10-10T22:00', end: '2020-10-10T23:00' },
          { start: '2020-10-10T23:00', end: '2020-10-11T00:00' },
          { start: '2020-10-10T01:00', end: '2020-10-10T02:00' },
          { start: '2020-10-10T02:00', end: '2020-10-10T03:00' },
          { start: '2020-10-10T03:00', end: '2020-10-10T04:00' },
          { start: '2020-10-10T04:00', end: '2020-10-10T05:00' },
          { start: '2020-10-10T05:00', end: '2020-10-10T06:00' },
          { start: '2020-10-10T06:00', end: '2020-10-10T07:00' },
          { start: '2020-10-10T07:00', end: '2020-10-10T08:00' },
          { start: '2020-10-10T08:00', end: '2020-10-10T09:00' },
          { start: '2020-10-10T09:00', end: '2020-10-10T10:00' },
          { start: '2020-10-10T10:00', end: '2020-10-10T11:00' },
        ],
        '2020-10-11': [
          { start: '2020-10-11T18:00', end: '2020-10-11T19:00' },
          { start: '2020-10-11T19:00', end: '2020-10-11T20:00' },
          { start: '2020-10-11T20:00', end: '2020-10-11T21:00' },
          { start: '2020-10-11T21:00', end: '2020-10-11T22:00' },
        ],
        '2020-10-12': [
          { start: '2020-10-12T10:00', end: '2020-10-12T11:00' },
          { start: '2020-10-12T11:00', end: '2020-10-12T12:00' },
          { start: '2020-10-12T12:00', end: '2020-10-12T13:00' },
          { start: '2020-10-12T13:00', end: '2020-10-12T14:00' },
          { start: '2020-10-12T14:00', end: '2020-10-12T15:00' },
          { start: '2020-10-12T16:00', end: '2020-10-12T17:00' },
          { start: '2020-10-12T17:00', end: '2020-10-12T18:00' },
          { start: '2020-10-12T18:00', end: '2020-10-12T19:00' },
          { start: '2020-10-12T19:00', end: '2020-10-12T20:00' },
          { start: '2020-10-12T20:00', end: '2020-10-12T21:00' },
        ],
      };

      expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
    });

    it('should be within the range with 1 hour duration', () => {
      const availabilities = {
        sat: {
          times: [
            { start: '11:00', end: '23:59' },
            { start: '01:00', end: '10:59' },
          ],
          specifics: {},
        },
        sun: { times: [{ start: '18:00', end: '22:00' }], specifics: {} },
        mon: {
          times: [],
          specifics: {
            '2020-10-12': [
              { start: '10:00', end: '15:00' },
              { start: '16:00', end: '21:00' },
            ],
          },
        },
        tue: {
          times: [],
          specifics: {},
        },
        wed: { times: [], specifics: {} },
        thu: { times: [], specifics: {} },
        fri: { times: [], specifics: {} },
      };
      const now = moment.utc();
      let intervals = AvailabilityUtils.intervals(
        availabilities,
        now,
        now.clone().add(7, 'day'),
        60,
        60,
        'Asia/Tehran',
      );
      intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
      const expected = {
        '2020-10-10': [
          { start: '2020-10-10T11:00', end: '2020-10-10T12:00' },
          { start: '2020-10-10T12:00', end: '2020-10-10T13:00' },
          { start: '2020-10-10T13:00', end: '2020-10-10T14:00' },
          { start: '2020-10-10T14:00', end: '2020-10-10T15:00' },
          { start: '2020-10-10T15:00', end: '2020-10-10T16:00' },
          { start: '2020-10-10T16:00', end: '2020-10-10T17:00' },
          { start: '2020-10-10T17:00', end: '2020-10-10T18:00' },
          { start: '2020-10-10T18:00', end: '2020-10-10T19:00' },
          { start: '2020-10-10T19:00', end: '2020-10-10T20:00' },
          { start: '2020-10-10T20:00', end: '2020-10-10T21:00' },
          { start: '2020-10-10T21:00', end: '2020-10-10T22:00' },
          { start: '2020-10-10T22:00', end: '2020-10-10T23:00' },
          { start: '2020-10-10T01:00', end: '2020-10-10T02:00' },
          { start: '2020-10-10T02:00', end: '2020-10-10T03:00' },
          { start: '2020-10-10T03:00', end: '2020-10-10T04:00' },
          { start: '2020-10-10T04:00', end: '2020-10-10T05:00' },
          { start: '2020-10-10T05:00', end: '2020-10-10T06:00' },
          { start: '2020-10-10T06:00', end: '2020-10-10T07:00' },
          { start: '2020-10-10T07:00', end: '2020-10-10T08:00' },
          { start: '2020-10-10T08:00', end: '2020-10-10T09:00' },
          { start: '2020-10-10T09:00', end: '2020-10-10T10:00' },
        ],
        '2020-10-11': [
          { start: '2020-10-11T18:00', end: '2020-10-11T19:00' },
          { start: '2020-10-11T19:00', end: '2020-10-11T20:00' },
          { start: '2020-10-11T20:00', end: '2020-10-11T21:00' },
          { start: '2020-10-11T21:00', end: '2020-10-11T22:00' },
        ],
        '2020-10-12': [
          { start: '2020-10-12T10:00', end: '2020-10-12T11:00' },
          { start: '2020-10-12T11:00', end: '2020-10-12T12:00' },
          { start: '2020-10-12T12:00', end: '2020-10-12T13:00' },
          { start: '2020-10-12T13:00', end: '2020-10-12T14:00' },
          { start: '2020-10-12T14:00', end: '2020-10-12T15:00' },
          { start: '2020-10-12T16:00', end: '2020-10-12T17:00' },
          { start: '2020-10-12T17:00', end: '2020-10-12T18:00' },
          { start: '2020-10-12T18:00', end: '2020-10-12T19:00' },
          { start: '2020-10-12T19:00', end: '2020-10-12T20:00' },
          { start: '2020-10-12T20:00', end: '2020-10-12T21:00' },
        ],
      };

      expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
    });
  });

  describe('real world examples', () => {
    describe('no settings applied', () => {
      describe('Asia/Tehran time zone', () => {
        describe('15 minutes duration', () => {
          it('no meetings', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {
                  '2020-10-06': [{ start: '11:00', end: '23:59' }],
                },
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: {} },
              fri: { times: [], specifics: {} },
            };
            const meetings = {};

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              15,
              'Asia/Tehran',
            );
            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-06': [
                '11:00',
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '22:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });

          it('should fuck', () => {
            jest.spyOn(Date, 'now').mockImplementation(() => new Date('2020-10-07T16:34:00.000Z'));

            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {},
              },
              wed: { times: [], specifics: { '2020-10-07': [{ start: '01:00', end: '23:59' }] } },
              thu: { times: [], specifics: { '2020-10-08': [{ start: '01:00', end: '23:59' }] } },
              fri: { times: [], specifics: {} },
            };
            const now = moment.utc().startOf('month');
            const meetings = {};
            const intervals = AvailabilityUtils.slots(
              availabilities,
              meetings,
              now,
              now.clone().endOf('month'),
              'Asia/Tehran',
              60,
              15,
              'Asia/Tehran',
              0,
              0,
              0,
              'indefinitely',
              null,
              0,
            ).local;

            const expected = {
              '2020-10-07': ['21:00', '22:00', '23:00'],
              '2020-10-08': [
                '01:00',
                '02:00',
                '03:00',
                '04:00',
                '05:00',
                '06:00',
                '07:00',
                '08:00',
                '09:00',
                '10:00',
                '11:00',
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '22:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });

          it('with 1 meeting', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {
                  '2020-10-06': [{ start: '11:00', end: '23:59' }],
                },
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: {} },
              fri: { times: [], specifics: {} },
            };
            const meetings = IntervalUtils.tz(
              {
                '2020-10-06': [{ start: '2020-10-06T12:00', end: '2020-10-06T12:15' }],
              },
              'Asia/Tehran',
              'UTC',
            );

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              15,
              'Asia/Tehran',
            );
            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-06': [
                '11:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '22:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });

          it('with 2 meeting', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {
                  '2020-10-06': [{ start: '11:00', end: '23:59' }],
                },
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: {} },
              fri: { times: [], specifics: {} },
            };
            const meetings = IntervalUtils.tz(
              {
                '2020-10-06': [
                  { start: '2020-10-06T12:00', end: '2020-10-06T12:15' },
                  { start: '2020-10-06T22:00', end: '2020-10-06T22:15' },
                ],
              },
              'Asia/Tehran',
              'UTC',
            );

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              15,
              'Asia/Tehran',
            );
            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-06': [
                '11:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });
        });

        describe('45 minutes duration', () => {
          it('no meetings', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {
                  '2020-10-06': [{ start: '11:00', end: '23:59' }],
                },
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: {} },
              fri: { times: [], specifics: {} },
            };
            const meetings = {};

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              45,
              'Asia/Tehran',
            );
            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-06': [
                '11:00',
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '22:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });

          it('with 1 meeting', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {
                  '2020-10-06': [{ start: '11:00', end: '23:59' }],
                },
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: {} },
              fri: { times: [], specifics: {} },
            };
            const meetings = IntervalUtils.tz(
              {
                '2020-10-06': [{ start: '2020-10-06T20:00', end: '2020-10-06T20:45' }],
              },
              'Asia/Tehran',
              'UTC',
            );

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              45,
              'Asia/Tehran',
            );
            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-06': [
                '11:00',
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '21:00',
                '22:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });

          it('with 2 meeting', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {
                  '2020-10-06': [{ start: '11:00', end: '23:59' }],
                },
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: {} },
              fri: { times: [], specifics: {} },
            };
            const meetings = IntervalUtils.tz(
              {
                '2020-10-06': [
                  { start: '2020-10-06T20:00', end: '2020-10-06T20:45' },
                  { start: '2020-10-06T11:00', end: '2020-10-06T11:45' },
                ],
              },
              'Asia/Tehran',
              'UTC',
            );

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              15,
              'Asia/Tehran',
            );
            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Asia/Tehran');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-06': [
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '21:00',
                '22:00',
                '23:00',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });
        });
      });

      describe('Pacific/Honolulu (-10:00) time zone', () => {
        describe('15 minutes duration', () => {
          it('no meetings', () => {
            const availabilities = {
              sat: {
                times: [],
                specifics: {},
              },
              sun: { times: [], specifics: {} },
              mon: {
                times: [],
                specifics: {},
              },
              tue: {
                times: [],
                specifics: {},
              },
              wed: { times: [], specifics: {} },
              thu: { times: [], specifics: { '2020-10-08': [{ start: '11:00', end: '23:59' }] } },
              fri: { times: [], specifics: {} },
            };
            const meetings = {};

            const now = moment.utc();

            let intervals = AvailabilityUtils.intervals(
              availabilities,
              now,
              now.clone().add(6, 'day'),
              60,
              15,
              'Asia/Tehran',
            );

            intervals = IntervalUtils.applyNoticeOfSchedule(intervals, 0);
            intervals = IntervalUtils.applyBuffers(intervals, meetings, 0, 0);
            intervals = IntervalUtils.applyDateRange(intervals, 'indefinitely', null);
            intervals = IntervalUtils.applyMaxPerDay(intervals, meetings, 0);
            intervals = IntervalUtils.tz(intervals, 'UTC', 'Pacific/Honolulu');
            intervals = IntervalUtils.slots(intervals);

            const expected = {
              '2020-10-07': ['21:30', '22:30', '23:30'],
              '2020-10-08': [
                '00:30',
                '01:30',
                '02:30',
                '03:30',
                '04:30',
                '05:30',
                '06:30',
                '07:30',
                '08:30',
                '09:30',
              ],
            };

            expect(IntervalUtils.sort(intervals)).toStrictEqual(IntervalUtils.sort(expected));
          });
        });
      });
    });
  });
});
