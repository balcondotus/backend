ARG NODE_ENV

FROM node:14.5.0-alpine

ARG NODE_ENV

ENV NODE_ENV ${NODE_ENV}

# Set working directory
WORKDIR /var/www/

# Install app dependencies
COPY package.json package-lock.json ./
RUN npm ci --silent --only=production

COPY . ./

# Run migrations
RUN npm run migrate:up

# Make wait-for-it executable
RUN chmod +x ./wait-for-it.sh

# A perfect companion to get the most out of Node.js in production environment
RUN npm install pm2 -g

# Install bash to execute wait-for-it
# bash is not installed in alpine images
RUN apk add --no-cache bash