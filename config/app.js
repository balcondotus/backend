const APP = require('lib/consts/app');
const { InternalServerError } = require('http-errors');

module.exports = {
  clientOriginProd: [
    'https://www.balcon.us',
    'https://dashboard.balcon.us',
    'https://meeting.balcon.us',
  ],
  clientOriginStage: [
    'https://staging.balcon.us',
    'https://staging.dashboard.balcon.us',
    'https://staging.meeting.balcon.us',
  ],
  clientOriginDev: [
    'https://dashboard.balcon.us:3000',
    'https://www.balcon.us:3001',
    'https://meeting.balcon.us:3002',
  ],
  notificationSender: {
    email: 'Balcon.us <notification@balcon.us>',
    number: '+12565591553', // TODO: Purchase a number later.
  },
  maxRequestSize: '500KB',
  allowedDisputeHours: 48,
  minPasswordLength: 6,
  phoneVerificationCodeLength: 6,
  minUsernameLength: 3,
  maxUsernameLength: 15,
  allowedUsernamePattern: /^[a-z0-9_]+$/i,
  allowedAvatarMimeTypes: ['image/jpeg', 'image/png', 'image/webp'],
  maxAvatarFileSize: 5000000, // 5MB
  allowedGenders: ['MALE', 'FEMALE', 'OTHER'],
  notification: {
    meeting: ['BALCON', 'EMAIL', 'SMS', 'PUSH'],
    reminder: ['BALCON', 'EMAIL', 'SMS', 'PUSH'],
    payment: ['BALCON', 'EMAIL', 'SMS', 'PUSH'],
    product: ['BALCON', 'EMAIL', 'PUSH'],
    newsletter: ['EMAIL'],
    survey: ['EMAIL'],
    offer: ['BALCON', 'EMAIL', 'PUSH'],
    account: ['BALCON', 'EMAIL', 'SMS'],
  },
  dollarExchangeRate: 18852, // in Tomans
  fee: {
    transaction: {
      percent: 0.015, // 1.5%
      max: 4000,
    },
    platform: {
      percent: 0.3,
    },
    service: {
      videoCall: 0.004 * 2, // base rate * 2 participants
      sms: 0.0822 * 2, // base rate * 2 participants
      recording: 0.01,
    },
  },
  dashboardUrl() {
    if (process.env.NODE_ENV === 'production') return APP.DASHBOARD_URL_PROD;
    if (process.env.NODE_ENV === 'staging') return APP.DASHBOARD_URL_STAGE;
    if (process.env.NODE_ENV === 'development') return APP.DASHBOARD_URL_DEV;

    throw new InternalServerError(`${process.env.NODE_ENV} is not implemented here`);
  },
  websiteUrl() {
    if (process.env.NODE_ENV === 'production') return APP.WEBSITE_URL_PROD;
    if (process.env.NODE_ENV === 'staging') return APP.WEBSITE_URL_STAGE;
    if (process.env.NODE_ENV === 'development') return APP.WEBSITE_URL_DEV;

    throw new InternalServerError(`${process.env.NODE_ENV} is not implemented here`);
  },
  meetingUrl() {
    if (process.env.NODE_ENV === 'production') return APP.MEETING_URL_PROD;
    if (process.env.NODE_ENV === 'staging') return APP.MEETING_URL_STAGE;
    if (process.env.NODE_ENV === 'development') return APP.MEETING_URL_DEV;

    throw new InternalServerError(`${process.env.NODE_ENV} is not implemented here`);
  },
  paymentGateway(trackId) {
    return `https://gateway.zibal.ir/start/${trackId}`;
  },
  resetPasswordUrl(token) {
    return `${this.dashboardUrl()}/password/reset?token=${token}`;
  },
  verifyEmailUrl(token) {
    return `${this.dashboardUrl()}/email/verify?token=${token}`;
  },
  payRedirectUrl() {
    return `${this.websiteUrl()}/payment/redirect`;
  },
  disputeMeetingUrl(meetingId) {
    return `${this.meetingUrl()}/${meetingId}/dispute`;
  },
  rescheduleMeetingUrl(hostIdentifier, meetingId) {
    return `${this.websiteUrl()}/${hostIdentifier}?reschedule=${meetingId}`;
  },
  cancelMeetingUrl(meetingId) {
    return `${this.meetingUrl()}/${meetingId}/cancel`;
  },
  joinMeetingUrl(meetingId) {
    return `${this.meetingUrl()}/${meetingId}`;
  },
  supportedOauthProviders: ['facebook', 'google'],
  aws: {
    s3: {
      bucket: {
        avatar: 'balcon-avatars',
      },
      endpoint: 'https://s3.ir-thr-at1.arvanstorage.com',
    },
  },
  transport: {
    sendgrid: {
      host: 'smtp.sendgrid.net',
      port: 465,
      auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASSWORD,
      },
    },
    mailtrap: {
      host: 'smtp.mailtrap.io',
      port: 587,
      auth: {
        user: process.env.MAILTRAP_USER,
        pass: process.env.MAILTRAP_PASSWORD,
      },
    },
  },
  minutesBeforeMeetingToRemind: 30,
  supportedLocales: ['fa', 'en'],
  jwt: {
    expiration: 1,
    expirationType: 'week',
  },
  sendgrid: {
    list: {
      productListId: '9cd1d109-2fb4-4682-987f-67fd2fd36ad0',
      surveyListId: '7b232a81-50d6-44ea-b159-58cfefbc7252',
      offerListId: '1ec9259f-3055-4141-b0a6-b56b0cfebae0',
      newsletterListId: '73e9ace6-1c43-4147-990a-e91a38066a40',
    },
  },
};
