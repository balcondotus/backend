const requireDirectory = require('require-directory');
const _ = require('lodash');

module.exports = requireDirectory(module, {
  rename(name) {
    return _.flow(_.camelCase, _.upperFirst)(name);
  },
});
